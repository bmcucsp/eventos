<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class {controller_name} extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('session');

        $this->load->model(array('mcb_data/mdl_mcb_data'));
        
        $this->load->language('mcb', $this->mdl_mcb_data->setting('default_language'));

        $this->load->helper(array('uri', 'mcb_icon'));

        $this->load->library(array('form_validation', 'redir'));

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');


        $this->_post_handler();

        $this->load->model('{model_name}');
    }

    public function index() {

        $this->load->model('{model_name_table}');

        $params = array(
            'paginate' => TRUE,
            'limit' => $this->config->item('results_per_page'),
            'page' => uri_assoc('page')
        );

        $order_by = uri_assoc('order_by');

        $order = uri_assoc('order');

        {switch_order}

        $data = array(
            '{entity}s' => $this->{model_name}->get($params),
            'table_headers' => $this->{model_name_table}->get_table_headers()
        );

        /*
		 * assets
		 */
		/*
        $this->template->add_css('nombre_archivo.css', 'link', false);
		$this->template->add_js('nombre_archivo.js', 'import', false);
		$javascript_inline = '
	    	$(".clase").accion({
	    	  //operaciones
	    	})
		';
		$this->template->add_js($javascript_inline, 'embed', false);
		*/
		
		/*
		 * template
		 */
		$this->template->write('header_title', 'Listado de {controller_name}');
		$this->template->write('title', 'Listado de {controller_name}');
		$this->template->write_view('content', 'index', $data);
		
		//$this->template->write_view('system_messages', 'dashboard/system_messages');
		//$this->template->write_view('sidebar', 'sidebar_view');
		
		$this->template->render();
    }

    public function form() {

        ${identity} = uri_assoc('{identity}');
        
        if ($this->{model_name}->validate()) {
            
            $this->{model_name}->save();

            ${identity} = (${identity}) ? ${identity} : $this->db->insert_id();

            redirect('{entity}/form/{identity}/' . ${identity});
            
        } else {
            
            if (!$_POST && ${identity}) {

                $this->{model_name}->prep_validation(${identity});
            }
            
            /*
             * template
            */
            $this->template->write('header_title', 'Administrar {controller_name}');
            $this->template->write('title', 'Administrar {controller_name}');
            $this->template->write_view('content', 'form', array('custom_fields' => $this->{model_name}->custom_fields));
            
            $this->template->write_view('system_messages', 'dashboard/system_messages');
            //$this->template->write_view('sidebar', 'sidebar_view');
            
            $this->template->render();
        }
    }

    public function _post_handler() {
        
        if ($this->input->post('btn_add'))
            redirect('{entity}/form');
        if ($this->input->post('btn_cancel'))
            redirect($this->session->userdata('last_index'));
    }

    public function delete() {

        ${identity} = uri_assoc('{identity}');

        if (${identity}) {
            $this->{model_name}->delete(array('{identity}' => ${identity}));
        }
        
        redirect('{entity}');
    }

}

?>