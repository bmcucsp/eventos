<table>
	<tr>
		{headers}
	</tr>
	
	<?php foreach (${entity}s as ${entity}) { ?>
	<tr>
		<?php foreach (${entity} as $value) { ?>
		<td>
			<?php echo $value; ?>
		</td>
		<?php } ?>
	
		<td>
			<a href="<?php echo site_url('{entity}/form/{identity}/' . ${entity}->{identity}); ?>" title="<?php echo $this->lang->line('edit'); ?>">
			<?php echo icon('edit'); ?>
			</a>
			<a href="<?php echo site_url('{entity}/delete/{identity}/' . ${entity}->{identity}); ?>" title="<?php echo $this->lang->line('delete'); ?>" onclick="javascript:if(!confirm('<?php echo $this->lang->line('confirm_delete'); ?>')) return false">
			<?php echo icon('delete'); ?>
			</a>
		</td>
	</tr>
	<?php } ?>
</table>

<?php if ($this->mdl_{entity}->page_links) { ?>
    <div id="pagination">
        <?php echo $this->mdl_{entity}->page_links; ?>
    </div>
<?php } ?>