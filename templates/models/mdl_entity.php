<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class {model_name} extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = '{entity}';

        $this->primary_key = '{entity}.{identity}';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		{entity}.*";

        $this->order_by = '{identity}';

        $this->custom_fields = $this->mdl_fields->get_object_fields(3);
    }

    public function validate() {
        {form_validations}
        foreach ($this->custom_fields as $custom_field) {

            $this->form_validation->set_rules($custom_field->column_name, $custom_field->field_name);
        }

        return parent::validate();
    }

    public function save() {

        parent::save(parent::db_array(), uri_assoc('{identity}'));
    }

}
?>