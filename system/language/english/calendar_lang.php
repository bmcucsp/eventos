<?php

$lang['cal_su']			= "Su";
$lang['cal_mo']			= "Mo";
$lang['cal_tu']			= "Tu";
$lang['cal_we']			= "We";
$lang['cal_th']			= "Th";
$lang['cal_fr']			= "Fr";
$lang['cal_sa']			= "Sa";
$lang['cal_sun']		= "Dom";
$lang['cal_mon']		= "Lun";
$lang['cal_tue']		= "Mar";
$lang['cal_wed']		= "Mie";
$lang['cal_thu']		= "Jue";
$lang['cal_fri']		= "Vie";
$lang['cal_sat']		= "Sab";
$lang['cal_sunday']		= "Domingo";
$lang['cal_monday']		= "Lunes";
$lang['cal_tuesday']	= "Martes";
$lang['cal_wednesday']	= "Wednesday";
$lang['cal_thursday']	= "Thursday";
$lang['cal_friday']		= "Friday";
$lang['cal_saturday']	= "Saturday";
$lang['cal_jan']		= "Jan";
$lang['cal_feb']		= "Feb";
$lang['cal_mar']		= "Mar";
$lang['cal_apr']		= "Apr";
$lang['cal_may']		= "May";
$lang['cal_jun']		= "Jun";
$lang['cal_jul']		= "Jul";
$lang['cal_aug']		= "Aug";
$lang['cal_sep']		= "Sep";
$lang['cal_oct']		= "Oct";
$lang['cal_nov']		= "Nov";
$lang['cal_dec']		= "Dec";
$lang['cal_january']	= "Enero";
$lang['cal_february']	= "Febrero";
$lang['cal_march']		= "Marzo";
$lang['cal_april']		= "Abril";
$lang['cal_mayl']		= "Mayo";
$lang['cal_june']		= "Junio";
$lang['cal_july']		= "Julio";
$lang['cal_august']		= "Agosto";
$lang['cal_september']	= "Septiembre";
$lang['cal_october']	= "Octubre";
$lang['cal_november']	= "Noviembre";
$lang['cal_december']	= "Diciembre";

/* End of file calendar_lang.php */
/* Location: ./system/language/english/calendar_lang.php */