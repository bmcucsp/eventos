function cancelar_asistencia(evento_id, large) {
    if (large) large = "/large/true";
    else large = "";
    $.ajax({
        type: 'post',
        url: base_url() + "evento/evento_async/cancelar_asistencia/evento_id/" + evento_id + large,
        data : 'last_index=' + document.URL,
        dataType: 'json',
        success: function(msg) {
            if (msg.redir !== undefined) {
                window.location.href = base_url() + 'user/login';
            }
            else {
                $('.boton-participar-' + evento_id).html(msg.content);
                $('button.participar').on('click', function() {
                    var evento_id = $(this).attr('data-evento-id');
                    participar(evento_id, msg.large);
                });
            }
        },
        failure: function(msg) {
            $('.boton-participar-' + evento_id).append("Error, int&eacute;ntelo de nuevo.");
        }
    });

}
function participar(evento_id, large) {
    if (large) large = "/large/true";
    else large = "";
    $.ajax({
        type: 'post',
        url: base_url() + "evento/evento_async/participar/evento_id/" + evento_id + large,
        data: 'last_index=' + document.URL,
        dataType: 'json',
        success: function(msg) {
            if (msg.redir !== undefined) {
                window.location.href = base_url() + 'user/login';
            }
            else {
                $('.boton-participar-' + evento_id).html(msg.content);
                $('.cancelar_asistencia').on('click', function() {
                    var evento_id = $(this).attr('data-evento-id');
                    cancelar_asistencia(evento_id, msg.large);
                });
            }
        },
        failure: function(msg) {
            $('.boton-participar-' + evento_id).append("Error, int&eacute;ntelo de nuevo.");
        }
    });
}
$(document).ready(function(){
    $('button.participar').on('click', function() {
        var evento_id = $(this).attr('data-evento-id');
        participar(evento_id, $(this).hasClass('btn-large'));
    });
    $('.cancelar_asistencia').on('click', function() {
        var evento_id = $(this).attr('data-evento-id');
        cancelar_asistencia(evento_id, $(this).hasClass('largo'));
    });
    $("#buscar").on("click", function() {
        setTimeout('$("#searchmenui_search").focus()', 100);
    });
    $("#focus_login").on("click", function() {
        setTimeout('$("#inputEmail").focus()', 100);
    });
});