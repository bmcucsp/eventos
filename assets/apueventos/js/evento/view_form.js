//para enviar el formulario
$('.btn_submit').click(function() {
	$('#evento').submit();
});

//cuando cambia el valor del combo de pais
$('#pais_id').change(function() {
	$('#ciudad_id > option').remove();
	var pais_id = $('#pais_id').val();
	$.ajax({
		type: 'post',
		url: base_url() + 'evento/get_ciudades/' + pais_id,
		dataType: 'json',
		success: function(cities) {
			$.each(cities, function(id, ciudad) {
				var opt = $('<option />');
				opt.val(ciudad.ciudad_id);
				opt.text(ciudad.nombre);
				$('#ciudad_id').append(opt);
			});
		}
	})
});
$('ul.frec-repeat li a').on('click', function() {
	$this = $(this).attr('data-value');
	if ($this == 'no-repite') {
		$("#select_repite").val("evento_unico").change();
	}
	else {
		$('#modal_' + $this).modal();
	}
});

if ($('#select_repite').val() == 'evento_unico') {
	$('#evento_repite_content').hide();
}
else {
	$('#evento_unico_content').hide();
}
$('#select_repite').change(function() {
	if ($('#select_repite').val() == 'evento_unico') {
		$('#evento_repite_content').hide();
		$('#evento_unico_content').show();
	}
	else {
		$('#evento_repite_content').show();
		$('#evento_unico_content').hide();
	}
});
$('#guardar_modal_diariamente').click(function() {
	$('#repeticiones').empty();
	//para normalizar con las fechas de evento

	$('#fecha_inicio').val($('#id_daily-repeat_start').val());
	$('#fecha_fin').val($('#id_daily-repeat_end').val());

	$('#hour_start').val($('#hour_start_daily').val());
	$('#minute_start').val($('#minute_start_daily').val());
	$('#meridian_start').val($('#meridian_start_daily').val());

	$('#hour_end').val($('#hour_end_daily').val());
	$('#minute_end').val($('#minute_end_daily').val());
	$('#meridian_end').val($('#meridian_end_daily').val());


	$('#repeticiones').append('<label>Repite cada ' +  $('#id_daily-repeat_interval').val() + ' dia(s) hasta el ' + $('#id_daily-repeat_end').val() + '</label> <a class="btn btn-mini" style="cursor:pointer" onclick="$(\'#modal_diariamente\').modal()">Editar</a>');
	$('#repeticiones').append('<input type="hidden" name="repeat_type" value="diariamente" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#id_daily-repeat_interval').val() + '" name = "arreglo_repeticiones[num_dias]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#id_daily-repeat_start').val() + '" name = "arreglo_repeticiones[repeat_start]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#id_daily-repeat_end').val() + '" name = "arreglo_repeticiones[repeat_end]" />');

	$('#repeticiones').append('<input type="hidden" value="' + $('#hour_start_daily').val() + '" name = "arreglo_repeticiones[hour_start_daily]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#minute_start_daily').val() + '" name = "arreglo_repeticiones[minute_start_daily]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#meridian_start_daily').val() + '" name = "arreglo_repeticiones[meridian_start_daily]" />');

	$('#repeticiones').append('<input type="hidden" value="' + $('#hour_end_daily').val() + '" name = "arreglo_repeticiones[hour_end_daily]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#minute_end_daily').val() + '" name = "arreglo_repeticiones[minute_end_daily]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#meridian_end_daily').val() + '" name = "arreglo_repeticiones[meridian_end_daily]" />');
});
$('#guardar_modal_semanalmente').click(function() {

	//para normalizar con las fechas de evento
	$('#fecha_inicio').val($('#id_weekly-repeat_start').val());
	$('#fecha_fin').val($('#id_weekly-repeat_end').val());

	$('#hour_start').val($('#hour_start_weekly').val());
	$('#minute_start').val($('#minute_start_weekly').val());
	$('#meridian_start').val($('#meridian_start_weekly').val());

	$('#hour_end').val($('#hour_end_weekly').val());
	$('#minute_end').val($('#minute_end_weekly').val());
	$('#meridian_end').val($('#meridian_end_weekly').val());

	$('#repeticiones').empty();
	$('input[name="days[]"]:checked').each(function() {
		$('#repeticiones').append('<input type="hidden" name="arreglo_repeticiones[dias][]" value="' + this.value + '" />');
	});
	$('#repeticiones').append('<label>Repite cada ' +  $('#id_weekly-repeat_interval').val() + ' semana(s) hasta el ' + $('#id_weekly-repeat_end').val() + '</label> <a class="btn btn-mini" style="cursor:pointer" onclick="$(\'#modal_semanalmente\').modal()">Editar</a>');
	$('#repeticiones').append('<input type="hidden" name="repeat_type" value="semanalmente" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#id_weekly-repeat_interval').val() + '" name = "arreglo_repeticiones[num_semanas]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#id_weekly-repeat_start').val() + '" name = "arreglo_repeticiones[repeat_start]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#id_weekly-repeat_end').val() + '" name = "arreglo_repeticiones[repeat_end]" />');

	$('#repeticiones').append('<input type="hidden" value="' + $('#hour_start_weekly').val() + '" name = "arreglo_repeticiones[hour_start_weekly]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#minute_start_weekly').val() + '" name = "arreglo_repeticiones[minute_start_weekly]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#meridian_start_weekly').val() + '" name = "arreglo_repeticiones[meridian_start_weekly]" />');

	$('#repeticiones').append('<input type="hidden" value="' + $('#hour_end_weekly').val() + '" name = "arreglo_repeticiones[hour_end_weekly]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#minute_end_weekly').val() + '" name = "arreglo_repeticiones[minute_end_weekly]" />');
	$('#repeticiones').append('<input type="hidden" value="' + $('#meridian_end_weekly').val() + '" name = "arreglo_repeticiones[meridian_end_weekly]" />');
});
$(document).ready(function() {
	$("#costo").hide();
	$("#combo_condicion").change(function() {
		if ($("#combo_condicion option:selected").val() == "gratuito")
			$("#costo").hide("slow");
		else
			$("#costo").show("slow");
	})

	if ($("#combo_condicion option:selected").val() == "gratuito") $("#costo").hide();
	else $("#costo").show();

	/* para los detalles de organizador */
	$("#has_detail_organizer").click(function() {
		if ($(this).attr("checked")) {
			$("#organizer_detail").show();
		}
		else {
			$("#organizer_detail").hide();
		}
	});

	if ($("#has_detail_organizer:checked").val() !== undefined) {
		$("#organizer_detail").show();
	}
	else {
		$("#organizer_detail").hide();
	}

	/* para los detalles de ubicacion referencial*/
	$("#agregar_ubicacion_referencial").click(function(){
		if ($(this).attr("checked")) {
			$(".ubicacion_referencial").show();
			$("#map_canvas").show();
			if ($('#latitud').val() == '' || $('#longitud').val() == '') {
				localizar();
			}
			google.maps.event.trigger(window, "resize");
			// $("#map_canvas").show();
			// google.maps.event.trigger(window, "resize");
	}
	else {
		$(".ubicacion_referencial").hide();
		$("#map_canvas").hide();
	}
	});
if ($("#agregar_ubicacion_referencial:checked").val() !== undefined) {
	$(".ubicacion_referencial").show();
	$("#map_canvas").show();
}
else {
	$(".ubicacion_referencial").hide();
	$("#map_canvas").hide();
}
});
