function suscribirse(categoria_id) {
    $.ajax({
        type: 'post',
        url: base_url() + "user/suscribir/categoria/" + categoria_id,
        data : 'last_index=' + document.URL,
        success: function(msg) {
            //console.log(msg); return;
            if (msg == 'redir') {
                window.location.href = base_url() + 'user/login';
            }
            else {
                $('#categoria-id-' + categoria_id).html(msg);
            }
            $('button.desuscribirse').on('click', function() {
                var categoria_id = $(this).attr('id');
                desuscribirse(categoria_id);
                return false;
            });
        },
        failure: function(msg) {
            console.log("failure");
            $('#categoria-id-' + categoria_id).append("Error, int&eacute;ntelo de nuevo.");
        }
    });
    return false;
}

function desuscribirse(categoria_id) {
    $.ajax({
        type: 'post',
        url: base_url() + "user/desuscribir/categoria/" + categoria_id,
        data : 'last_index=' + document.URL,
        success: function(msg) {
            //console.log(msg); return;
            if (msg == 'redir') {
                window.location.href = base_url() + 'user/login';
            }
            else {
                $('#categoria-id-' + categoria_id).html(msg);
            }
            $('button.suscribirse').on('click', function() {
                var categoria_id = $(this).attr('id');
                suscribirse(categoria_id);
                return false;
            });
        },
        failure: function(msg) {
            console.log("failure");
            $('#categoria-id-' + categoria_id).append("Error, int&eacute;ntelo de nuevo.");
        }
    });
    return false;
}

$(document).ready(function(){
    $('.suscribirse').on('click', function() {
        var categoria_id = $(this).attr("id");
        suscribirse(categoria_id);
        return false;
    });

    $('.desuscribirse').on('click', function() {
        var categoria_id = $(this).attr("id");
        desuscribirse(categoria_id);
        return false;
    });
});