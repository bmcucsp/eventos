$(function() {
    applyNextCalendar();
    function applyNextCalendar() {
        $("#calendario_mes th a, #calendario_mes_barra_lateral th a").click(function() {
            var url = $(this).attr("href");
            $.ajax({
                type: "post",
                data: "ajax=1",
                url: url,
                beforeSend: function() {
                    $(".loading_calendar").html('<div style="text-align: center"><img src="' + base_img() + 'loading.gif"/></div>');
                },
                success: function(msg) {
                    $("#calendario_mes, #calendario_mes_barra_lateral").html(msg);
                    applyNextCalendar();
                }
            });
            return false;
        });
    }
})

$(function(){
    //cuando cambia el valor del combo de pais
    $("#pais_id").change(function() {
        $("#ciudad_id > option").remove();
        var pais_id = $("#pais_id").val();
        if (pais_id == "") pais_id = "0";
        $.ajax({
            type: "post",
            url: base_url() + "evento/get_ciudades/" + pais_id,
            dataType: "json",
            success: function(cities) {
                var opt = $("<option />");
                opt.val("");
                opt.text("-- Seleccione una ciudad");
                $("#ciudad_id").append(opt);
                $.each(cities, function(id, ciudad){
                    opt = $("<option />");
                    opt.val(ciudad.ciudad_id);
                    opt.text(ciudad.nombre);
                    $("#ciudad_id").append(opt);
                });
            }
        })
    });

    $("#search-option-field .search-option-header-wrap").click( function() {
    	$("#bloque-categorias").toggle("fast");
    	$("#bloque-calendar").hide();
    	$("#bloque-location").hide();
    });

    $("#search-option-location .search-option-header-wrap").click( function() {
    	$("#bloque-categorias").hide();
    	$("#bloque-calendar").hide();
    	$("#bloque-location").toggle("fast");
    });

    $("#search-option-time .search-option-header-wrap").click( function() {
    	$("#bloque-categorias").hide();
    	$("#bloque-calendar").toggle("fast");
    	$("#bloque-location").hide();
    });
    $('body').on('touchstart.dropdown', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
    $('.dropdown-menu').click(function(event){
     event.stopPropagation();
    });
    $("#buscar_pais").click(function() {
        var pais_id = $("#pais_id").val();
        var ciudad_id = $("#ciudad_id").val();
        if (pais_id != "")
            pais_id = "p_" + pais_id;
        if (ciudad_id != "")
            ciudad_id = "i_" + ciudad_id;

        var url = document.createElement("a");
        url.href = window.location.href;

        //console.log(url.protocol);
        //console.log(url.host);

        var arr_url = url.pathname.split("/");
        var prefix = ["c", "f", "t", "p", "i"];
        //buscamos desde el indice 2 porque empieza desde el 2do segmento
        //vamos a agregar los nuevos (pais_id y ciudad_id) o reemplazarlos
        //si ya existen

        //variables que usaremos para verificar que nuestros parametros son
        //nuevos o ya existen y deben ser actualizados
        var found_p = false;
        var found_i = false;
        //arreglo para los segmentos a agregar
        var segments = [];
        for (i = 2; i < arr_url.length; i++) {
            if (arr_url[i].length > 3 && arr_url[i][1] == "_" && prefix.indexOf(arr_url[i].substr(0, 1)) != -1) {
                var pushed = false;
                if (pais_id.length > 0 && arr_url[i].substr(0, 1) == pais_id.substr(0, 1)) {
                    found_p = true;
                    segments.push(pais_id);
                    pushed = true;
                }

                if (ciudad_id.length > 0 && arr_url[i].substr(0, 1) == ciudad_id.substr(0, 1)) {
                    found_i = true;
                    segments.push(ciudad_id);
                    pushed = true;
                }
                if (!pushed)
                    segments.push(arr_url[i]);
            }
        }
        if (!found_p && pais_id) segments.push(pais_id);
        if (!found_i && ciudad_id) segments.push(ciudad_id);
        var new_location = base_url() + "eventos";
        for (var i = 0; i < segments.length; i++)
            new_location += "/" + segments[i];
        window.location.href = new_location;
    });
})