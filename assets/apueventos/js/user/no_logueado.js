$(".registrarse").on('click', function() {
    var action = $(".dropdown-login").attr("action");
    var indice = action.lastIndexOf('/');

    if ($(this).text() != "Iniciar sesión") {
    	$(".email").before('<div class="control-group username"><div class="controls"><input type="text" id="inputUsername" placeholder="Nombre de usuario" name="username"></div></div>');
        $(".registrarse").text("Iniciar sesión");
        $(".entrar").text("Crear cuenta");
        $(".dropdown-login").attr("action",  action.substring(0, indice) + "/registro");
        $(".titulo_log").text("Registro de nueva cuenta");
        $("#tituloh1").html("Registro de nueva cuenta");
    }
    else {
        $(".registrarse").text("Registrarse");
        $(".username").remove();
        $(".entrar").text("Entrar");
        $(".dropdown-login").attr("action", action.substring(0, indice) +  "/login");
        $(".titulo_log").text("Ingreso a cuenta de eventos");
        $("#tituloh1").html("Ingrese a su cuenta");
    }
    return false;
});