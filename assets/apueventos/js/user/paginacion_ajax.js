/* para la paginacion */
$(function() {
    applyPagination();
    function applyPagination() {
        $("#pagination a").click(function() {
            var url = $(this).attr("href");
            if (url == "#" || url == undefined) return false;
            $.ajax({
                type: "post",
                data: "ajax=1",
                url: url,
                beforeSend: function() {
                    $("#loading").html('<div style="text-align: center; position: absolute; bottom: 0px;left: 50%; margin-left: -80px;"><img src="' + base_img() + 'loading.gif"/></div>');
                },
                success: function(msg) {
                    $("#lista_eventos").html(msg);
                    applyPagination();
                }
            });
            return false;
        });
    }
})