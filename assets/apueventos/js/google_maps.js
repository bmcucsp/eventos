var markersArray = [];
google.maps.Map.prototype.clearOverlays = function() {
  for (var i = 0; i < markersArray.length; i++ ) {
    markersArray[i].setMap(null);
  }
}
function crear_mapa(latitud, longitud) {
	$("#latitud").val(latitud);
	$("#longitud").val(longitud);
	var mapOptions = {
		center: new google.maps.LatLng(latitud, longitud),
		zoom: 18,
		scrollwheel: false,
		mapTypeControl: false,
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
		},
		panControl: false,
		streetViewControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById('map_canvas'),
		mapOptions);

	var input = document.getElementById('searchTextField');
	var autocomplete = new google.maps.places.Autocomplete(input);

	autocomplete.bindTo('bounds', map);

	var infowindow = new google.maps.InfoWindow();
	var marker = new google.maps.Marker({
		map: map
	});
	/* seteamos esto para que marque por defecto al inicio de la carga */
	marker.setPosition(mapOptions.center);
	var service =new google.maps.places.PlacesService(map);

	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		infowindow.close();
		var place = autocomplete.getPlace();
//        console.log(place);
//        console.log(place.geometry.location);
		$("#latitud").val(place.geometry.location.lat());
		$("#longitud").val(place.geometry.location.lng());
//        $("#referencia").val(place.name);

		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17);  // Why 17? Because it looks good.
		}

		var image = new google.maps.MarkerImage(
			place.icon,
			new google.maps.Size(71, 71),
			new google.maps.Point(0, 0),
			new google.maps.Point(17, 34),
			new google.maps.Size(35, 35));
		marker.setIcon(image);
		marker.setPosition(place.geometry.location);

		var address = "";
		if (place.address_components) {
			address = [
			(place.address_components[0] && place.address_components[0].short_name || ""),
			(place.address_components[1] && place.address_components[1].short_name || ""),
			(place.address_components[2] && place.address_components[2].short_name || "")
			].join(' ');
		}

		infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
		infowindow.open(map, marker);
	});
	google.maps.event.addListener(map, "click", function(event)
	{
		map.clearOverlays();
		marker.setPosition(event.latLng);
		$("#latitud").val(event.latLng.lat());
		$("#longitud").val(event.latLng.lng());
		curPlace = event.latLng;
		var request ={
			location : event.latLng,
			radius: 50,
		};
		service.nearbySearch(request, callback);
		function callback(results, status) {
			// console.log(results);
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				for (var i = 0; i < results.length; i++) {
					if (i == 0) {
						var nome = results[i].name;
						var place = results[i];
						google.maps.event.addListener(marker, 'click', function() {
							infowindow.setContent('<div><strong>' + nome + '</strong></div>');
							infowindow.open(map, marker);
							$("#searchTextField").val(nome);
							$("#latitud").val(curPlace.lat());
							$("#longitud").val(curPlace.lng());
						});
						infowindow.setContent('<div><strong>' + nome + '</strong></div>');
						infowindow.open(map, marker);
						$("#searchTextField").val(nome);
						$("#latitud").val(curPlace.lat());
						$("#longitud").val(curPlace.lng());
						// createMarker(results[i]);
						break;
					}
				};
			}
		}

		function createMarker (place) {
			var placeLoc =place.geometry.location;
			var marker =new google.maps.Marker({
				map: map,
				position: place.geometry.location
			});
			markersArray.push(marker);
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent('<div><strong>' + place.name + '</strong></div>');
				/* this es en este caso el marker actual */
				infowindow.open(map, this);
				$("#searchTextField").val(place.name);
				$("#latitud").val(place.geometry.location.lat());
				$("#longitud").val(place.geometry.location.lng());
			});
		}
//        console.log(event);
	});
}
if ($('#latitud').val() != '' && $('#longitud').val() != '') {
	var latitud = $('#latitud').val();
	var longitud = $('#longitud').val();
	crear_mapa(latitud, longitud);
}
function localizar() {
	// console.log("inicializo con exito");
	mi_localizacion({
		success: function(position) {
			crear_mapa(position.coords.latitude, position.coords.longitude);
		},
		error: function(error) {
			alert('la Geolocalizaci&oacute;n fall&oacute;: '+error.message);
		},
		not_supported: function() {
			alert('tu navegador no soporta geolocalizaci&oacute;n,\npero puedes buscar el lugar escribiendo en el campo de texto');
		},
		always: function() {
			// alert('hecho!');
			// console.log("listo!")
		}
	});

}
mi_localizacion = function(options) {
  var complete_callback = options.always || options.complete;

  if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(function(position) {
	  options.success(position);
	  if (complete_callback) {
		complete_callback();
	  }
	}, function(error) {
	  options.error(error);

	  if (complete_callback) {
		complete_callback();
	  }
	}, options.options);
  }
  else {
	options.not_supported();

	if (complete_callback) {
	  complete_callback();
	}
  }
};