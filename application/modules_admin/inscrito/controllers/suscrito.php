<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Inscrito extends MX_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('user/mdl_usuario');
        $this->load->model('mdl_inscrito');
        $this->load->model('evento/mdl_evento');
        $this->load->model('evento/mdl_evento_repeticion');
    }

    public function index() {
    	redirect('/');
    }
    
	function guardar_envios_suscripcion($id_evento) {
		
		$usuarios = array();
		$emails = array();
		
		//obteniendo la categoria del evento recientemente agregado
		$params = array();
	    $params['select'] = 'nombre, url, categoria_id, fecha_inicio, ciudad_id';
	    $params['where']['evento_id'] = $id_evento;
	    $params['return_row'] = true;
	    $query = $this->mdl_evento->get($params);

		//verificando si el evento tiene repeticiones
		/*$params = array();
	    $params['select'] = 'repeat_start, repeat_interval';
	    $params['where']['evento_id'] = $id_evento;
	    $query_repetido = $this->mdl_evento_repeticion->get($params);*/
	    //if()	    

	    //obteniendo los ids de usuario segun la categoria a la que estan suscritos
	    $params = array();
    	$params['select'] = 'usuario_id';
    	$params['where']['categoria_id'] = $query->categoria_id;
        $query_suscrito = $this->mdl_suscrito->get($params);
		
        //si tenemos usuarios suscritos en la categoria del evento agregado recientemente
        if (count($query_suscrito) > 0){
        	
	        foreach ($query_suscrito as $value) {
	        	$usuarios[] = $value->usuario_id;
	        }
	        
	        // obteniendo los emails, nombres de los usuarios que estan suscritos a cierta categoria
	        $params = array();
	    	$params['select'] = 'usuario_id, nombres, email';
	    	$params['where_in']['usuario_id'] = $usuarios; 
	        $query_usuarios = $this->mdl_usuario->get($params);
	        
	        foreach ($query_usuarios as $value) {
	        	$emails[] = $value->email;
	        }
	                
			$this->load->helper('enviar_mail');
	        $mensaje = "Hay un nuevo evento en Acercate-eventos: " . anchor('evento/' . $query->url, $query->nombre) . " asdsd";

	        $this->load->model('envio/mdl_envio');
	        
	        foreach ($emails as $value) {
          		$params = array('titulo_evento' => $query->nombre, 'url_evento' => $query->url, 'fecha_evento' => $query->fecha_inicio, 'nombre_user' => 'Usuario', 'email_user' => $value, 'created_at' => date("Y-m-d"), 'tipo' => 'suscrito');
            	$this->mdl_envio->save($params, '', false);
	        	//enviar_mails($this->config->item('site_email'), $this->config->item('site_name'), $value, '', '', 'Nuevo evento ', mensaje_alerta_correo('Alerta de Eventos', $mensaje, $value));
	        }       	
        }
        else
        	echo 'no hay a quien enviar';

	}

	function test() {
		echo obtener_gmt_calendar('1354233600', 5);
	}

}

?>