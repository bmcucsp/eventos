<div class="padded">
<form method="post" action="<?php echo site_url($this->uri->uri_string()); ?>">

	<dl>
                <input type="hidden" name="ponente_id" value="<?php echo $this->mdl_ponente->form_value('ponente_id'); ?>" />
                </dl>
                <dl>
                	<dt><label>* nombres </label></dt>
                <dd>
<input type="text" name="nombres" value="<?php echo $this->mdl_ponente->form_value('nombres'); ?>" />
                </dd>
                	</dl>
                <dl>
                	<dt><label>* area_especialidad </label></dt>
                <dd>
<input type="text" name="area_especialidad" value="<?php echo $this->mdl_ponente->form_value('area_especialidad'); ?>" />
                </dd>
                	</dl>
                <dl>
                	<dt><label>* grado_estudio </label></dt>
                <dd>
<input type="text" name="grado_estudio" value="<?php echo $this->mdl_ponente->form_value('grado_estudio'); ?>" />
                </dd>
                	</dl>
                <dl>
                	<dt><label>* profesion </label></dt>
                <dd>
<input type="text" name="profesion" value="<?php echo $this->mdl_ponente->form_value('profesion'); ?>" />
                </dd>
                	</dl>
                <dl>
                	<dt><label>* email </label></dt>
                <dd>
<input type="text" name="email" value="<?php echo $this->mdl_ponente->form_value('email'); ?>" />
                </dd>
                	</dl>
                <dl>
                	<dt><label>* nacionalidad </label></dt>
                <dd>
<input type="text" name="nacionalidad" value="<?php echo $this->mdl_ponente->form_value('nacionalidad'); ?>" />
                </dd>
                	</dl>
                <dl>
                	<dt><label>* foto </label></dt>
                <dd>
<input type="text" name="foto" value="<?php echo $this->mdl_ponente->form_value('foto'); ?>" />
                </dd>
                	</dl>


	<?php foreach ($custom_fields as $custom_field) { ?>
	<dl>
		<dt>
			<label><?php echo $custom_field->field_name; ?>: </label>
		</dt>
		<dd>
			<input type="text" name="<?php echo $custom_field->column_name; ?>"
				id="<?php echo $custom_field->column_name; ?>"
				value="<?php echo $this->mdl_empleado->form_value($custom_field->column_name); ?>" />
		</dd>
	</dl>
	<?php } ?>

	<input type="submit" id="btn_cancel" class="btn btn-danger" name="btn_cancel" value="<?php echo $this->lang->line('cancel'); ?>" />
	<input type="submit" id="btn_submit" class="btn btn-success" name="btn_submit" value="<?php echo $this->lang->line('submit'); ?>" />

</form>
</div><!-- padded -->

<div class="controles">
	<ul class="nav nav-list">
		<li><?php echo anchor('ponente/index', '<i class=icon-list></i> Listado de ponentes');?></li>
	</ul>
</div>