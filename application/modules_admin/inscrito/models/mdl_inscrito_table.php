<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_ponente_table extends CI_Model {

    public function get_table_headers() {

        $order = (uri_assoc('order')) == 'asc' ? 'desc' : 'asc';

        $headers = array(
        'ponente_id' => anchor('ponente/index/order_by/ponente_id/order/'.$order, 'ponente_id'),
        'nombres' => anchor('ponente/index/order_by/nombres/order/'.$order, 'nombres'),
        'area_especialidad' => anchor('ponente/index/order_by/area_especialidad/order/'.$order, 'area_especialidad'),
        'grado_estudio' => anchor('ponente/index/order_by/grado_estudio/order/'.$order, 'grado_estudio'),
        'profesion' => anchor('ponente/index/order_by/profesion/order/'.$order, 'profesion'),
        'email' => anchor('ponente/index/order_by/email/order/'.$order, 'email'),
        'nacionalidad' => anchor('ponente/index/order_by/nacionalidad/order/'.$order, 'nacionalidad'),
        'foto' => anchor('ponente/index/order_by/foto/order/'.$order, 'foto'),
        );


        return $headers;
    }

}

?>
