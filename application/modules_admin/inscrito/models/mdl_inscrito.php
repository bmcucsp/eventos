<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_inscrito extends MY_Model {

	public function __construct() {

		parent::__construct();

		$this->table_name = 'inscrito';
		$this->primary_key = 'inscrito.usuario_id';
		$this->select_fields = "SQL_CALC_FOUND_ROWS inscrito.*";
		$this->order_by = 'fecha_inscripcion';
	}
	
	public function listar_inscritos($evento_id)
	{
		$params['select'] = 'usuario_id';
		$params['where']['evento_id'] = $evento_id;
		$inscritos = $this->mdl_inscrito->get($params);
		$this->load->model('user/mdl_usuario');
		$params = array();
		$params['select'] = "nombres, apellidos, email";
		$usuario_ids = array();
		for ($i=0; $i < count($inscritos); $i++) { 
			$usuario_ids[] = $inscritos[$i]->usuario_id;
		}
		$params['where_in']['usuario_id'] = $usuario_ids;
		return $this->mdl_usuario->get($params);
	}
	
}

?>