<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_publicidad extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'categoria';

        $this->primary_key = 'categoria.categoria_id';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		categoria.*";

        $this->order_by = 'categoria_id';
        
    }

    public function validate() {
        
        $this->form_validation->set_rules('nombre','nombre', 'required|trim|xss_clean');
        $this->form_validation->set_rules('descripcion','descripcion', 'trim|xss_clean');
        $this->form_validation->set_rules('categoria_padre','categoria_padre', 'trim|xss_clean');
        $this->form_validation->set_rules('img','img', 'trim|xss_clean');
        $this->form_validation->set_rules('url','url', 'required|trim|xss_clean');
        $this->form_validation->set_rules('created_at','created_at', 'trim|xss_clean');
        $this->form_validation->set_rules('status','status', 'trim|xss_clean');

        return parent::validate();
    }

    public function save() {

        parent::save(parent::db_array(), uri_assoc('categoria_id'));
    }

}
?>