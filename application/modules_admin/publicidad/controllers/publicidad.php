<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Publicidad extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('session');

        $this->load->model(array('mcb_data/mdl_mcb_data'));

        $this->load->language('mcb', $this->mdl_mcb_data->setting('default_language'));

        $this->load->helper(array('uri', 'mcb_icon'));

        $this->load->library(array('form_validation', 'redir'));

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');


        $this->_post_handler();

        $this->load->model('mdl_publicidad');
    }

    public function index() {

        $this->load->model('mdl_publicidad_table');

        $params = array(
            'paginate' => TRUE,
            'limit' => $this->config->item('results_per_page'),
            'page' => uri_assoc('page', 2)
        );

        $order_by = uri_assoc('order_by');

        $order = uri_assoc('order');

        switch ($order_by) {
          case 'nombre':
              $params['order_by'] = 'categoria.nombre ' .$order;
              break;
          case 'descripcion':
              $params['order_by'] = 'categoria.descripcion ' .$order;
              break;
          case 'categoria_padre':
              $params['order_by'] = 'categoria.categoria_padre ' .$order;
              break;
          case 'img':
              $params['order_by'] = 'categoria.img ' .$order;
              break;
          case 'url':
              $params['order_by'] = 'categoria.url ' .$order;
              break;
          case 'created_at':
              $params['order_by'] = 'categoria.created_at ' .$order;
              break;
          case 'status':
              $params['order_by'] = 'categoria.status ' .$order;
              break;
          default:
              $params['order_by'] = 'categoria.categoria_id ' .$order;
          }


        $data = array(
            'categorias' => $this->mdl_categoria->get($params),
            'table_headers' => $this->mdl_categoria_table->get_table_headers()
        );

        /*
		 * assets
		 */
		/*
        $this->template->add_css('nombre_archivo.css', 'link', false);
		$this->template->add_js('nombre_archivo.js', 'import', false);
		$javascript_inline = '
	    	$(".clase").accion({
	    	  //operaciones
	    	})
		';
		$this->template->add_js($javascript_inline, 'embed', false);
		*/

		/*
		 * template
		 */
		$this->template->write('header_title', 'Listado de Categoria');
		$this->template->write('title', 'Listado de Categoria');
		$this->template->write_view('content', 'index', $data);

		//$this->template->write_view('system_messages', 'dashboard/system_messages');
		//$this->template->write_view('sidebar', 'sidebar_view');

		$this->template->render();
    }

    public function form() {

        $categoria_id = uri_assoc('categoria_id');

        if ($this->mdl_categoria->validate()) {

            $this->mdl_categoria->save();

            $categoria_id = ($categoria_id) ? $categoria_id : $this->db->insert_id();

            redirect('categoria/form/categoria_id/' . $categoria_id);

        } else {

            if (!$_POST && $categoria_id) {

                $this->mdl_categoria->prep_validation($categoria_id);
            }

            /*
             * template
            */
            $this->template->write('header_title', 'Administrar Categoria');
            $this->template->write('title', 'Administrar Categoria');
            $this->template->write_view('content', 'form');

            $this->template->write_view('system_messages', 'dashboard/system_messages');
            //$this->template->write_view('sidebar', 'sidebar_view');

            $this->template->render();
        }
    }

    public function _post_handler() {

        if ($this->input->post('btn_add'))
            redirect('categoria/form');
        if ($this->input->post('btn_cancel'))
            redirect($this->session->userdata('last_index'));
    }

    public function delete() {

        $categoria_id = uri_assoc('categoria_id');

        if ($categoria_id) {
            $this->mdl_categoria->delete(array('categoria_id' => $categoria_id));
        }

        redirect('categoria');
    }

}

?>