<?php if (!defined('BASEPATH'))	exit('No direct script access allowed');

class User extends MX_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('mcb_data/mdl_mcb_data');
		$this->mdl_mcb_data->set_session_data();
		$this->load->language('mcb', $this->mdl_mcb_data->setting('default_language'));
		$this->load->helper(array('uri', 'mcb_icon', 'mcb_date'));
		$this->load->library(array('form_validation', 'redir'));
		$this->form_validation->set_error_delimiters('<li class="errorli">', '</li>');
		$this->_post_handler();
		$this->load->model('mdl_usuario');

		$this->load->model('evento/mdl_evento');
		$this->mdl_evento->add_index = true;
		$this->load->model('suscrito/mdl_suscrito');
		$this->load->model('user/mdl_inscrito');
		$this->load->model("ciudad/mdl_ciudad");
		$this->load->model("pais/mdl_pais");
		$this->load->model("categoria/mdl_categoria");
	}

	public function _post_handler() {
		if ($this->input->post('btn_add'))
			redirect('evento/agregar');
		if ($this->input->post('btn_cancel'))
			redirect($this->session->userdata('last_index'));
	}
	public function eventos_creados($usuario_id = false) {
		if ($usuario_id) {
			//eventos del usuario
			$params = array();
			$params['where']['usuario_id'] = $usuario_id;
			$params['order_by'] = 'created_at desc';
			$params['paginate'] = true;
			$params['limit'] = $this->config->item('results_per_page');
			$params['page'] = uri_page();
			$data['eventos'] = $this->mdl_evento->get($params);
			$data['page_links'] = $this->mdl_evento->page_links;

			$i=0;
			foreach ($data['eventos'] as $evento) {
				//recuperamos nro de inscritos x evento
				$params_inscritos = array();
				$params_inscritos['select'] = 'evento_id, usuario_id';
				$params_inscritos['where']['evento_id'] = $evento->evento_id;
				$inscritos = $this->mdl_inscrito->get($params_inscritos);
				$data['eventos'][$i]->nro_inscritos = count($inscritos);

				//recuperamos paises y ciudades
				$params_ciudad = array();
				$params_ciudad['select'] = 'ciudad_id, pais_id, nombre';
				$params_ciudad['where']['ciudad_id'] = $evento->ciudad_id;
				$params_ciudad['return_row'] = true;
				$ciudad = $this->mdl_ciudad->get($params_ciudad);
				$data['eventos'][$i]->ciudad = $ciudad->nombre;

				$params_pais = array();
				$params_pais['select'] = "nombre";
				$params_pais['where']['pais_id'] = $ciudad->pais_id;
				$params_pais['return_row'] = true;
				$pais = $this->mdl_pais->get($params_pais);
				$data['eventos'][$i]->pais = $pais->nombre;

				//recuperamos categorias
				$params_categoria = array();
				$params_categoria['select'] = "nombre, url, img";
				$params_categoria['where']['categoria_id'] = $data['eventos'][$i]->categoria_id;
				$params_categoria['return_row'] = true;
				$categoria = $this->mdl_categoria->get($params_categoria);
				$data['eventos'][$i]->categoria = $categoria->nombre;
				$data['eventos'][$i]->categoria_url = $categoria->url;
				$data['eventos'][$i]->categoria_img = $categoria->img;
				$i++;
			}
			return $data;
		}
	}

	public function suscripciones_de_usuario($usuario_id = false) {
		if ($usuario_id) {
			//categorias a las que se suscribio
			$params = array();
			$params['select'] = 'categoria_id';
			$params['where']['usuario_id'] = $usuario_id;
			return $this->mdl_suscrito->get($params);
		}
	}

	public function eventos_inscritos($usuario_id = false){
		if ($usuario_id) {
			//eventos a los que se inscribio
			$params_inscrito['select'] = 'evento_id';
			$params_inscrito['where']['usuario_id'] = $usuario_id;
			$inscrito = $this->mdl_inscrito->get($params_inscrito);

			//eventos a los que se inscribio
			$eventos_inscritos = array();
			$params = array();
			$data['eventos_inscritos'] = array();
			$params['return_row'] = true;
			for ($i = 0; $i < count($inscrito); $i++) {
				$params['where']['evento_id'] = $inscrito[$i]->evento_id;
				//$evento_inscrito = $this->mdl_evento->get($params);
				$evento_inscrito = $this->mdl_evento->proximos_eventos($params);
				if ($evento_inscrito)
					array_push($data['eventos_inscritos'], $evento_inscrito[0]);
			}

			$i=0;
			foreach ($data['eventos_inscritos'] as $evento) {
				if ($evento->usuario_id == $this->session->userdata('sesion_id_user')) {
					unset($data['eventos_inscritos'][$i]);
				} else {
					//recuperamos paises y ciudades
					$params_ciudad = array();
					$params_ciudad['select'] = 'ciudad_id, pais_id, nombre';
					$params_ciudad['where']['ciudad_id'] = $evento->ciudad_id;
					$params_ciudad['return_row'] = true;
					$ciudad = $this->mdl_ciudad->get($params_ciudad);
					$data['eventos_inscritos'][$i]->ciudad = $ciudad->nombre;

					$params_pais = array();
					$params_pais['select'] = "nombre";
					$params_pais['where']['pais_id'] = $ciudad->pais_id;
					$params_pais['return_row'] = true;
					$pais = $this->mdl_pais->get($params_pais);
					$data['eventos_inscritos'][$i]->pais = $pais->nombre;

					//recuperamos categorias
					$params_categoria = array();
					$params_categoria['select'] = "nombre, url, img";
					$params_categoria['where']['categoria_id'] = $data['eventos_inscritos'][$i]->categoria_id;
					$params_categoria['return_row'] = true;
					$categoria = $this->mdl_categoria->get($params_categoria);
					$data['eventos_inscritos'][$i]->categoria = $categoria->nombre;
					$data['eventos_inscritos'][$i]->categoria_url = $categoria->url;
					$data['eventos_inscritos'][$i]->categoria_img = $categoria->img;
				}
				$i++;
			}
			return $data['eventos_inscritos'];
		}
	}

	public function eventos_online($usuario_id = false) {
		if ($usuario_id) {
			$params = array();
			$params['return_row'] = true;
			$params['where']['presencial'] = 0;
			$data['eventos_online'] = $this->mdl_evento->proximos_eventos($params);

			if ($data['eventos_online']) {
				$i=0;
				foreach ($data['eventos_online'] as $evento) {
					if ($evento->usuario_id == $this->session->userdata('sesion_id_user')) {
						unset($data['eventos_online'][$i]);
					} else {
						//recuperamos paises y ciudades
						$params_ciudad = array();
						$params_ciudad['select'] = 'ciudad_id, pais_id, nombre';
						$params_ciudad['where']['ciudad_id'] = $evento->ciudad_id;
						$params_ciudad['return_row'] = true;
						$ciudad = $this->mdl_ciudad->get($params_ciudad);
						$data['eventos_online'][$i]->ciudad = $ciudad->nombre;

						$params_pais = array();
						$params_pais['select'] = "nombre";
						$params_pais['where']['pais_id'] = $ciudad->pais_id;
						$params_pais['return_row'] = true;
						$pais = $this->mdl_pais->get($params_pais);
						$data['eventos_online'][$i]->pais = $pais->nombre;

						//recuperamos categorias
						$params_categoria = array();
						$params_categoria['select'] = "nombre, url, img";
						$params_categoria['where']['categoria_id'] = $data['eventos_online'][$i]->categoria_id;
						$params_categoria['return_row'] = true;
						$categoria = $this->mdl_categoria->get($params_categoria);
						$data['eventos_online'][$i]->categoria = $categoria->nombre;
						$data['eventos_online'][$i]->categoria_url = $categoria->url;
						$data['eventos_online'][$i]->categoria_img = $categoria->img;
					}
					$i++;
				}
			}
			return $data['eventos_online'];
		}
	}

	public function index() {
		if ($this->milib->logueado()) {
			$data['totalEvents'] = $this->milib->totalEventsCreated();
			$todo_eventos = $this->eventos_creados($this->session->userdata('sesion_id_user'));
			$data['eventos'] = $todo_eventos['eventos'];
			$data['page_links'] = $todo_eventos['page_links'];

			$data['suscripciones'] = $this->suscripciones_de_usuario($this->session->userdata('sesion_id_user'));

			$data['eventos_inscritos'] = $this->eventos_inscritos($this->session->userdata('sesion_id_user'));

			$data['eventos_online'] = $this->eventos_online($this->session->userdata('sesion_id_user'));

			$data['categorias'] = $this->milib->categorias();
			$data['logged_in'] = $this->session->userdata('logged_in');
			$data['usuario_id'] = $this->session->userdata('sesion_id_user') ? $this->session->userdata('sesion_id_user') : 0;
			//$data['mis_eventos'] = $this->recuperar_mis_id_eventos();

			$this->template->set_template('template-sidebar');
			if (count($data['eventos_inscritos']) >= count($data['eventos'])) {
				$activar_tab = "eventos_inscritos";
			}
			else {
				$activar_tab = "eventos_creados";
			}
			$javascript_inline = "
			$(document).ready(function() {
				$('#myTab a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				});
				$('#myTab a[href=\"#$activar_tab\"]').tab('show');
				$('.ajaxify').ajaxify();
       		});";
			$this->template->add_js(base_js() . "ajaxify.js");
			$this->template->add_js($javascript_inline, "embed");
			$this->template->write('title', 'Panel de cuenta personal' );
			$this->template->write_view('system_messages', 'dashboard/system_messages');
			$this->template->write_view('content', 'index', $data);
			$this->template->render();
		}
		else
			redirect('user/login');
	}

	public function grocery() {
		$crud = new grocery_CRUD();

		$crud->set_table('evento');
		$crud->set_subject('Evento');
		$crud->unset_columns(array('url',
			'pais_id',
			'ciudad_id',
			'ubicacion',
			'descripcion',
			'programa',
			'organizador',
			'detalle_costo',
			'created_at',
			'referencia',
			'dirigido_a',
			'categoria_id',
			'usuario_id',
			'img',
			'latitud',
			'longitud',
			'detalle_organizador'));
//        $this->load->view("example", $crud->render());
		$content = $crud->render();
		$js_files = $content->js_files;
		$css_files = $content->css_files;
//        var_dump($content);
		foreach ($js_files as $key => $js_file) {
			$this->template->add_js($js_file);
		}
		foreach ($css_files as $key => $css_file) {
			$this->template->add_css($css_file);
		}
		unset($content->js_files);
		unset($content->css_files);
//        var_dump($content);exit;
		$this->template->write('title', 'titulo de mi contenido');
		$this->template->write('content', $content->output);
		$this->template->render();
	}

	public function delete() {

		$usuario_id = uri_assoc('usuario_id');
		if ($usuario_id) {
			$this->mdl_usuario->delete(array('usuario_id' => $usuario_id));
		}
		redirect('user');
	}

	public function validate() {

		$this->form_validation->set_rules('nombres', 'Nombres', 'required|trim|xss_clean');
		$this->form_validation->set_rules('apellidos', 'Apellidos', 'required|trim|xss_clean');
		$this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|xss_clean|callback_verificar_email_registrado');
		$this->form_validation->set_rules('doc_identidad', 'doc_identidad', 'trim|xss_clean|max_length[8]|min_length[8]|numeric');
		$this->form_validation->set_rules('sexo', 'sexo', 'trim|xss_clean');
		$this->form_validation->set_rules('fecha_nac', 'fecha_nac', 'trim|xss_clean');
		$this->form_validation->set_rules('ciudad_id', 'Ciudad', 'required|trim|xss_clean');
		$this->form_validation->set_rules('img', 'img', 'trim|xss_clean');
		return $this->mdl_usuario->validate();
	}

	public function validate_email_and_pass($campo = '') {
		if ($campo == 'email') {
			$this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|valid_email');
		} else if ($campo == '') {
			$this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|valid_email|callback__verificar_email_registrado');
			$this->form_validation->set_rules('pass', 'Contrase&ntilde;a', 'required|trim|xss_clean|min_length[5]');
		} else { /* es registro */
			$this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|valid_email');
			$this->form_validation->set_rules('pass', 'Contrase&ntilde;a', 'required|trim|xss_clean|min_length[5]');
			$this->form_validation->set_rules('username', 'Nombre de Usuario', 'required|trim|xss_clean');
		}
		return $this->mdl_usuario->validate($this);
	}

	public function login($bar = false) {
		if ($this->milib->logueado()) {
			redirect('user');
		}
		if ($this->validate_email_and_pass()) {
			$params = array();
			$db_array = $this->mdl_usuario->db_array();
			$params['select'] = 'usuario_id, status, nivel_acceso, username, img';
			$params['where']['email'] = $db_array['email'];
			$params['where']['pass'] = generar_md5($db_array['pass']);
			$params['return_row'] = true;

			$query = $this->mdl_usuario->get($params);
			if (count($query) == 1) {
				//creamos sesi�n general de logueado y su id si este est� activo
				if ($query->status == 1) {
					$this->session->set_userdata("logged_in", true);
					$this->session->set_userdata("sesion_id_user", $query->usuario_id);
					$this->session->set_userdata("nivel_acceso", $query->nivel_acceso);
					$this->session->set_userdata("username", $query->username);
					$this->session->set_userdata("img", $query->img);
					/* si el usuario quizo entrar mas antes a un area privada
					 * entonces debemos redireccionarle a la seccion que queria entrar
					 * una vez se autentique correctamente, sino a la por default(zona_privada)
					 */
					if ($this->session->userdata('last_index') !== false)
						redirect($this->session->userdata('last_index'));
					redirect('user/index');
				}
				else {
					$this->session->set_flashdata('custom_error', 'Su cuenta a&uacute;n no est&aacute; activa, por favor activela desde su correo electr&oacute;nico.');
					redirect('user/login');
				}
			} else {
				$this->session->set_flashdata('custom_error', 'Email o contrase&ntilde;a incorrectos.');
				redirect('user/login');
			}
		}

		if ($bar) {
			$this->load->view('login_bar');
		} else {
			$this->load->model('facebook_full_model');
			$data['url_login'] = $this->facebook_full_model->get_url_login();
			$this->template->add_js(base_js() . "user/no_logueado.js");
			$this->template->write('header_title', 'Ingresa a tu cuenta En Acercate-eventos');
			$this->template->write('title', 'Ingresa a tu cuenta');
			$this->template->write_view('system_messages', 'dashboard/system_messages', $data);
			$vista_login = preg_match('/\/user\/login/', $this->uri->ruri_string());
			$this->template->write_view('content', 'login_registro', array('es_vista_login' => $vista_login));
			$this->template->render();
		}
	}

	public function logout() {
		session_start();
		$redir = $this->session->userdata('last_nav');
		unset($_SESSION['oauth_token']);
		unset($_SESSION['oauth_token_secret']);
		session_destroy();
		$this->session->sess_destroy();
		$this->session->sess_create();
		/* para redireccionar a la ultima vista antes de desloguearse */
		$this->session->set_flashdata('custom_error', 'Se cerr&oacute; su sesi&oacute;n con &eacute;xito');
		if ($redir !== false)
			redirect($redir);
		redirect('user/login');
	}

	//inicio sesion twitter
	public function logfacebook() {
		//llamamos la funcion del modelo face
		$this->load->model('facebook_full_model');
		$this->load->model('facebook/mdl_facebook');
		$user_face = $this->facebook_full_model->get_user_facebook();
		//si esta la session del user_facebook mandamos a la funcion sino a login
		if ($user_face)
			$this->facebook_login($user_face);
		else
			redirect('user/login');
	}

	//inicio sesion facebook
	private function facebook_login($user_face) {
		//verificando si lo tenemos en user_facebook
		$params['select'] = 'id_user_facebook, email';
		$params['where']['id_user_facebook'] = $user_face;
		$params['return_row'] = true;
		$fila_user = $this->mdl_facebook->get($params);

		if (count($fila_user) == 0) { //no esta en user_facebook
			//verificando si email de usuario esta en tabla usuario
			$user_info = $this->facebook_full_model->get_user_basic_description();
			$params = array();
			$params['select'] = 'usuario_id, nombres, email, nivel_acceso';
			$params['where']['email'] = $user_info['email'];
			$params['return_row'] = true;
			$user_mail = $this->mdl_usuario->get($params);
			if (count($user_mail) != 0) {
				//usuario esta en table user guardamos al usuario en user_facebook y lo relacionamos
				$params = array();
				$params['id_user_facebook'] = $user_face;
				$params['email'] = $user_mail->email;
				$params['proveedor'] = 'facebook';
				//no esta en user_facebook guardamos
				$this->mdl_facebook->save($params, null, false);

				//publicando en muro de user facebook
				//$this->facebook_full_model->publicar_estado($user_info['first_name'].' se acaba de unir a Tacobooks.com, la red de ebooks.', 'www.tacobooks.com', '', 'Tacobooks', '');
				//creamos sesiones
				$this->session->set_userdata('id_face', $userr_face);
				$this->session->set_userdata("logged_in", true);
				$this->session->set_userdata("sesion_id_user", $user_mail->usuario_id);
				$this->session->set_userdata("nivel_acceso", $user_mail->nivel_acceso);
				$this->session->set_userdata("username", $user_mail->username);
				//mensaje de alerta
				$this->load->helper('enviar_mail');
				enviar_mails("info@dennisbot.com", "Acercate-eventos", $this->config->item('site_email'), '', '', 'Registro de nuevo usuario', "Email: ".$user_mail->email."<br />"."Usuario: ".$user_mail->username."<br />");
				redirect('user');
			}
			//guardamos en ambas tablas usuario y user_facebook
			$data_user = array('nombres' => $user_info['first_name'], 'apellidos' => $user_info['last_name'], 'pass' => generar_md5('sinpass'), 'email' => $user_info['email'], 'status' => '1', 'img' => '0', 'created_at' => strtotime("now"), 'url' => a_url_amigable($user_info['first_name']), 'nivel_acceso' => 2);
			$user_id = $this->mdl_usuario->save($data_user, null, false, true);
			if ($user_id != '') {
				$params = array();
				$params['id_user_facebook'] = $user_face;
				$params['usuario_id'] = $user_id;
				$params['email'] = $user_info['email'];
				$params['proveedor'] = 'facebook';
				//guardando en tabla user_facebook
				$this->mdl_facebook->save($params, null, false);

				//creamos sesiones
				$this->session->set_userdata('id_face', $user_face);
				$this->session->set_userdata("logged_in", true);
				$this->session->set_userdata("sesion_id_user", $user_id);
				$this->session->set_userdata("nivel_acceso", 2);
				$this->session->set_userdata("username", $user_info['first_name']);

				//publicando en muro de user facebook
				//$this->facebook_full_model->publicar_estado($user_info['first_name'].' se acaba de unir a Tacobooks.com, la red de ebooks.', 'www.tacobooks.com', '', 'Tacobooks', '');
				redirect('user');
			}
		} else { //user esta en la bd obtenemos su id mediante el id_user_facebook
			$params = array();
			$params['select'] = 'usuario_id';
			$params['where']['id_user_facebook'] = $user_face;
			$params['return_row'] = true;
			$userid = $this->mdl_facebook->get($params);

			//obtenemos el id y nombre del user
			$params = array();
			$params['select'] = 'usuario_id, nombres, nivel_acceso';
			$params['where']['usuario_id'] = $userid->usuario_id;
			$params['return_row'] = true;
			$user = $this->mdl_usuario->get($params);

			$this->session->set_userdata("logged_in", true);
			$this->session->set_userdata('id_face', $user_face);
			$this->session->set_userdata("sesion_id_user", $user->usuario_id);
			$this->session->set_userdata("nivel_acceso", $user->nivel_acceso);
			$this->session->set_userdata("username", $user->nombres);
			redirect('user');
		}
	}

	//registro de usuario logueado por twitter
	public function join() {

		if ($this->milib->logueado()) {
			redirect('user');
		}
		if ($this->validate_email_and_pass('email')) {
			$db_array = $this->mdl_usuario->db_array();
			// linea tentativa para que tambien ponga su nickname para mostrar
			$db_array['username'] = $this->input->post("username", true);

			$params = array();
			$params['where']['email'] = $db_array['email'];
			$params['select'] = 'username';
			$params['return_row'] = true;

			//si no retorna filas significa que no se repite el email
			if (count($this->mdl_usuario->get($params)) == 0) {

				//chancamos params con los parametros enviados con el form via post (es decir pass y email)
				$params = $db_array;
				// crearle un acceso 2 (nivel creacion evento)
				$params['nivel_acceso'] = '2';
				$params['status'] = '1';
				$params['url'] = a_url_amigable($this->input->post("username"));
				$params['codigo_activacion'] = generar_codigo_activacion();
				$params['pass'] = generar_md5($params['pass']);
				$params['img'] = $this->session->userdata("iduser") . '.jpg';
				$params['created_at'] = strtotime("now");
				save_image_from_url($this->session->userdata("img_user_twitter"), $this->session->userdata("iduser") . 'jpg');

				// guardamos en tabla user y facebook
				$this->load->model('facebook/mdl_facebook');
				$successu = $this->mdl_usuario->save($params, null, false, true);

				// datos para tabla facebook
				$params = array();
				$params['id_user_facebook'] = $this->input->post("iduser");
				$params['usuario_id'] = $successu;
				$params['username'] = $this->input->post("username");
				$params['email'] = $this->input->post("email");
				$params['proveedor'] = 'twitter';
				$successf = $this->mdl_facebook->save($params, null, false, false);

				if ($successu && $successf) {
					// enviar mail
					//$this->load->helper('enviar_mail');
					//$mensaje = "Bienvenido a Apueventos, su registro con su cuenta de Twitter fue realizado correctamente, ahora disfrute de nuestros servicios";
					//enviar_mails($this->config->item('site_email'), $this->config->item('site_name'), $this->input->post('email'), '', '', 'Confirmación de creación de cuenta', mensaje_alerta_correo('Mensaje de bienvenida', $mensaje, $this->input->post('email')));
					// inicio de sesion
					$this->session->set_userdata("logged_in", true);
					$this->session->set_userdata("sesion_id_user", $successu);
					$this->session->set_userdata("nivel_acceso", '2');
					$this->session->set_userdata("username", $this->input->post("username"));

					$this->session->set_flashdata('custom_success', 'Su registro con su cuenta de Twitter fue realizado correctamente, ahora puede crear eventos.');
					//echo 'session '.$this->session->flashdata('custom_success');
					//exit();
					redirect('user');
				} else {
					$this->session->set_flashdata('custom_error', 'Ha ocurrido un error mientras se guardaba sus datos, intentelo de nuevo');
					redirect('user/log/twitter');
				}
			} else {
				$this->session->set_flashdata('custom_error', 'El email ya fue registrado por favor elija otro o inicie sesi&oacute;n normalmente, olvidaste tu contraseña?');
				redirect('user/join/twitter');
			}
		}
		$data['user'] = $this->session->userdata("user_twitter");
		$data['iduser'] = $this->session->userdata("iduser");
		$data['img'] = $this->session->userdata("img_user_twitter");

		$this->template->write('header_title', 'Registro de sesi&oacute;n Twitter');
		$this->template->write('title', 'Inicio de sesi&oacute;n con su cuenta de twitter');
		$this->template->write_view('system_messages', 'dashboard/system_messages');
		$this->template->write_view('content', 'login_twitter', $data);
		$this->template->render();
	}

	//inicio sesion twitter
	public function twitter() {
		if ($this->milib->logueado()) {
			redirect('user');
		}
		$this->load->library('twitter');
		$this->twitter->sesion_twitter();
	}

	//registro twitter
	public function registro_twitter() {
		$this->load->library('twitter');
		$data_user = $this->twitter->get_data_twitter();
		// sino hay error mandamos al formulario de registro, cc, obtenemos nuevos tokens
		if (!isset($data_user->error)) {
			$data['user'] = $data_user->name;
			$data['iduser'] = $data_user->id;
			$data['img'] = $data_user->profile_image_url;

			//verificando user twitter
			$this->load->model('facebook/mdl_facebook');
			$params['select'] = 'id_user_facebook, usuario_id, username';
			$params['where']['id_user_facebook'] = $data['iduser'];
			$params['return_row'] = true;
			$fila_user = $this->mdl_facebook->get($params);

			//usuario esta en la bd iniciamos sesion
			if (count($fila_user) > 0) {
				$this->session->set_userdata("logged_in", true);
				$this->session->set_userdata("sesion_id_user", $fila_user->usuario_id);
				$this->session->set_userdata("nivel_acceso", '2');
				$this->session->set_userdata("username", $fila_user->username);
				redirect('user');
			} else {
				//mensaje de alerta
				$this->load->helper('enviar_mail');
				enviar_mails("info@dennisbot.com", "Acercate-eventos", $this->config->item('site_email'), '', '', 'Registro de nuevo usuario', "Email: Twitter sin email<br />"."Usuario: ".$data_user->name."<br />");
				$this->session->set_userdata("user_twitter", $data_user->name);
				$this->session->set_userdata("iduser", $data_user->id);
				$this->session->set_userdata("img_user_twitter", $data_user->profile_image_url);
				$this->template->write('header_title', 'Registro de sesi&oacute;n Twitter');
				$this->template->write('title', 'Crea tu cuenta para Acercate-eventos');
				$this->template->write_view('system_messages', 'dashboard/system_messages');
				$this->template->write_view('content', 'login_twitter', $data);
				$this->template->render();
			}
		}
		else
			$this->twitter();
	}

	//activar usuario
	public function activate() {

		$cod_activacion = $this->uri->segment(3);
		//codigo de activacion vacio
		if ($cod_activacion != '') {
			$params = array();
			$params['where']['codigo_activacion'] = $cod_activacion;
			$params['select'] = 'usuario_id, status, nivel_acceso, username';
			$params['return_row'] = true;

			$query = $this->mdl_usuario->get($params);
			//si encontro al usuario
			if (count($query) == 1) {

				if ($query->status == 1) {
					$this->session->set_flashdata('custom_error', 'Usted ya activo su cuenta, puede iniciar sesi&oacute;n normalmente.');
					redirect('user/login');
				} else {
					$params = array();
					$params['status'] = 1;
					$username = $query->username;
					if ($username == "")
						$username = "Usuario";
					if ($this->mdl_usuario->save($params, $query->usuario_id, false)) {
						$this->session->set_userdata("logged_in", true);
						$this->session->set_userdata("sesion_id_user", $query->usuario_id);
						$this->session->set_userdata("nivel_acceso", $query->nivel_acceso);
						$this->session->set_userdata("username", $username);
						$this->session->set_flashdata('custom_success', 'Bienvenido Usuario, ahora puedes agregar tus eventos.');
						redirect('user');
					} else {
						$this->session->set_flashdata('custom_success', 'Lo sentimos, no pudimos validar al usuario, int&eacute;ntelo nuevamente.');
						redirect('user/registro');
					}
				}
			}
			else
				$this->session->set_flashdata('custom_error', 'El c&oacute;digo ingresado no corresponde a su registro de usuario.');
		}
		redirect('user/registro');
	}

	public function registro() {
		if ($this->milib->logueado()) {
			redirect('user');
		}

		if ($this->validate_email_and_pass("registro")) {
			$db_array = $this->mdl_usuario->db_array();
			/* linea tentativa para que tambien ponga su nickname para mostrar */
			$db_array['username'] = $this->input->post("username", true);
			$params = array();
			$params['where']['email'] = $db_array['email'];
			$params['select'] = 'usuario_id, username';
			$params['return_row'] = true;
			$user_face_twit_esceniko = $this->mdl_usuario->get($params);

			//si no retorna filas significa que no se repite el email
			if (count($user_face_twit_esceniko) == 0) {

				//chancamos params con los parametros enviados con el form via post (es decir pass y email)
				$params = $db_array;
				log_message('error', print_r($params, true));
				//hasheamos la clave
				$params['pass'] = generar_md5($params['pass']);
				/* crearle un acceso 2 (nivel creacion evento) */
				$params['nivel_acceso'] = '2';
				$params['codigo_activacion'] = generar_codigo_activacion();
				$params['created_at'] = strtotime("now");

				$success = $this->mdl_usuario->save($params, uri_assoc('usuario_id'), false);
				log_message('error', print_r($params, true));
				log_message('error', $success);
				
				if ($success) {
					$this->load->helper('enviar_mail');
					$mensaje = "Bienvenido a Acercate-eventos, el link de activación de su cuenta es el siguiente: " . anchor('user/activate/' . $params['codigo_activacion'], 'Activar cuenta') . ", dele clic para hacer la confirmación de su cuenta de usuario.";
					enviar_mails($this->config->item('site_email'), $this->config->item('site_name'), $this->input->post('email'), '', '', 'Confirmación de creación de cuenta', mensaje_alerta_correo('Mensaje de bienvenida', $mensaje, $this->input->post('email')));
					enviar_mails("info@dennisbot.com", "Acercate-eventos", $this->config->item('site_email'), '', '', 'Registro de nuevo usuario', "Email: ".$this->input->post('email')."<br />"."Usuario: ".$this->input->post('username')."<br />");
					$this->session->set_flashdata('custom_success', 'Su registro fue correcto, revise su correo para activar su cuenta y poder empezar a crear eventos. (Tambi&eacute;n revise su bandeja de correo no deseado)');
				}
				else
					$this->session->set_flashdata('custom_error', 'ha ocurrido un error mientras se guardaba sus datos, intentelo de nuevo');

				redirect('user/login');
			}
			else {
				$this->load->model('facebook/mdl_facebook');
				$params = array();
				$params['where']['usuario_id'] = $user_face_twit_esceniko->usuario_id;
				$params['select'] = 'usuario_id';
				$params['return_row'] = true;
				if(count($this->mdl_facebook->get($params)) > 0){
					$this->session->set_flashdata('custom_error', 'Ud. ya regitr&oacute; una cuenta por Facebook, por favor inicie sesi&oacute;n mediante este servicio o reg&iacute;strese con otro email.');
					redirect('user/registro');
				}
				else{
					$this->session->set_flashdata('custom_error', 'El email ya fue registrado por favor elija otro, olvidaste tu contraseña? recup&eacute;rala '.anchor('user/recuperar/', 'aqu&iacute;'));
					redirect('user/registro');	
				}
			}
		}
		$this->load->model('facebook_full_model');
		$data['url_login'] = $this->facebook_full_model->get_url_login();
		$this->template->write('title', 'Registro de cuenta nueva');
		$this->template->add_js(base_js() . "user/no_logueado.js");
		$this->template->write_view('system_messages', 'dashboard/system_messages');
		$data['es_vista_login'] =preg_match('/\/user\/login/', $this->uri->ruri_string());
		$this->template->write_view('content', 'login_registro', $data);
		$this->template->render();
	}

	//cuenta de usuario
	public function cuenta() {
		$todo_eventos = $this->eventos_creados($this->session->userdata('sesion_id_user'));
		$data['eventos'] = $todo_eventos['eventos'];
		$data['page_links'] = $todo_eventos['page_links'];

		$data['suscripciones'] = $this->suscripciones_de_usuario($this->session->userdata('sesion_id_user'));

		$data['eventos_inscritos'] = $this->eventos_inscritos($this->session->userdata('sesion_id_user'));

		$params = array();
		$user_id = $this->session->userdata('sesion_id_user');
		if (isset($_POST['cambiar_pass'])) {
			$this->form_validation->set_rules('pass', 'Contraseña', 'required|trim|xss_clean');
			$this->form_validation->set_rules('repeatpass', 'Repetir contraseña', 'required|trim|xss_clean|min_length[5]|matches[pass]');
			if ($this->mdl_usuario->validate()) {
				$success = $this->mdl_usuario->save(array('pass' => generar_md5($this->input->post('pass'))), $user_id, false);
				if ($success) {
					$this->session->set_flashdata('custom_success', 'Contraseña modificada correctamente.');
					redirect('user/index/');
				}
				else {
					$this->session->set_flashdata('custom_error', 'La contraseña no pudo ser modificada intentelo nuevamente.');
					redirect('user/cuenta');
				}
			}
			$update = true;
			$activar_tab = "datos_pass";
		}
		else {
			//validando datos de edicion
			if ($this->validate()) {
				$params = $this->mdl_usuario->db_array();
				$params['fecha_nac'] = strtotime(standardize_date($params['fecha_nac']));
				$this->mdl_usuario->save($params, $user_id, false);
				$this->session->set_flashdata('custom_success', 'Cuenta actualizada correctamente.');
				redirect('user');
			}
			$activar_tab = "datos_personal";
		}

		//devolviendo datos de la bd edit
		if ((!$_POST AND $user_id) || isset($update)) {
			$this->mdl_usuario->prep_validation($user_id);
		}
		else //validando la fecha
			$this->mdl_usuario->set_form_value('fecha_nac', strtotime(standardize_date($this->input->post('fecha_nac'))));

		//datos picker
		$format = $this->mdl_mcb_data->setting("default_date_format_picker");
		$mask = $this->mdl_mcb_data->setting("default_date_format_mask");
		$base_url = base_url();
		$javascript_inline = '
			//tabs
			$(document).ready(function() {
				$(\'#myTab a\').click(function (e) {
					e.preventDefault();
					$(this).tab(\'show\');
				});
				$(\'#myTab a[href="#'.$activar_tab.'"]\').tab(\'show\');
			});

			$(".datepicker").datepicker({maxDate: new Date(2003, 1 - 1, 0), minDate: new Date(1940, 1 - 1, 1), changeYear: true, changeMonth: true, dateFormat: "' . $format . '" });
			$(".datepicker").mask("' . $mask . '");

			//para enviar el formulario
			$("#btn_submit").click(function() {
				$("#evento").submit();
			});
			$("#btn_cancel").click(function() {
				window.location.href="' . $base_url . 'evento";
			});

			$("#pais_id").change(function() {
				$("#ciudad_id > option").remove();
				var pais_id = $("#pais_id").val();
				$.ajax({
					type: "post",
					url: "' . $base_url . 'evento/get_ciudades/" + pais_id,
					dataType: "json",
					success: function(cities) {
						$.each(cities, function(id, ciudad){
							var opt = $("<option />");
							opt.val(ciudad.ciudad_id);
							opt.text(ciudad.nombre);
							$("#ciudad_id").append(opt);
						});
					}
				})
			});
		';

		//datos pais y ciudades
		$this->load->model(array('pais/mdl_pais', 'ciudad/mdl_ciudad'));
		$data['paises'] = $this->mdl_pais->get(array('order_by' => 'nombre'));

		$params = array();
		$params['where']['ciudad_id'] = $this->mdl_usuario->form_value('ciudad_id');
		$params['select'] = "pais_id, ciudad_id";
		$params['order_by'] = "nombre";
		$params['return_row'] = true;

		$ciudad = $this->mdl_ciudad->get($params);

		if (count($ciudad) == 1) {

			$data['pais_id'] = $ciudad->pais_id;
			$data['ciudad_id'] = $ciudad->ciudad_id;

			$params = array();
			$params['where']['pais_id'] = $ciudad->pais_id;
			$params['select'] = "ciudad_id, nombre";
			$params['order_by'] = "nombre";

			$data['ciudades'] = $this->mdl_ciudad->get($params);
		} else {
			$data['pais_id'] = 0;
			$data['ciudad_id'] = 0;
		}

		$this->template->add_js($javascript_inline, 'embed', false);
		$this->template->add_js(base_url() . "assets/image_crud/js/fileuploader.js", "import", false, 'footer', true);
		$this->template->add_css(base_url() . "assets/image_crud/css/fileuploader.css", "link", false, true);
		$upload_url = site_url("user/user_async/subir_imagen_ajax");
		$javascript_inline = "
			function foto_user(filename, full_path) {
				$('#img').val(filename);
				$('#foto-user').attr('src', full_path + filename + '_standard.jpg');
				if ($('.qq-upload-list').children('li').length == 2) {
					$('.qq-upload-list li:first-child').remove();
				}
				$('.qq-upload-list li:first-child').append('(cargar otra imagen si quiere reemplazar)');
				console.log('reloading imagen de evento: %s_thumb.jpg y full_path: %s', filename, full_path + filename + '_thumb.jpg');
			}
			function createUploader(){
			var uploader = new qq.FileUploader({
				element: document.getElementById('file-uploader'),
				template: '<div class=\"qq-uploader\">' +
					'<div class=\"qq-upload-drop-area\"><span>upload-drop-area</span></div>' +
					'<div class=\"qq-upload-button\">elegir foto</div>' +
					'<ul class=\"qq-upload-list\"></ul>' +
					'</div>',
				fileTemplate: '<li>' +
					'<span class=\"qq-upload-file\"></span>' +
					'<span class=\"qq-upload-spinner\"></span>' +
					'<span class=\"qq-upload-size\"></span>' +
					'<a class=\"qq-upload-cancel\" href=\"#\">subida cancelada</a>' +
					'<span class=\"qq-upload-failed-text\">error subiendo, vuelva a intentarlo</span>' +
					'</li>',
				action: '$upload_url',
				debug: false,
				onComplete: function(id, fileName, responseJSON){
								console.log(id);
								console.log(fileName);
								console.log(responseJSON);
								if ($('#img').val() != '') {
									$.ajax({
										type: 'post',
										url: '$base_url/user/user_async/eliminar_img_user/' + $('#img').val()
									});
								}
								foto_user(responseJSON['filename'], responseJSON['full_path']);
					//loadPhotoGallery();
								}
						});
				}
				createUploader();
			";
		$this->template->add_js($javascript_inline, "embed");
		$this->template->write('title', 'Panel de cuenta personal');
		$this->template->write_view('content', 'cuenta_edit', $data);
		$this->template->render();
	}

	//recuperar contrasena
	public function recuperar() {
		if (!$this->milib->logueado()) {
			if ($this->validate_email_and_pass('email')) {
				$nuevo_pass = generar_password();
				if ($this->enviar_nuevo_pass($this->input->post('email'), $nuevo_pass)) {
					$this->load->helper('enviar_mail');
					$mensaje = "Una solicitud para restablecer la contraseña de su cuenta se ha realizado en eventos.com.<br /><br />Para ingresar a su cuenta dirijase a: <a href='" . base_url() . "user/login'>" . base_url() . "user/login</a> y use la siguiente contraseña temporal: " . $nuevo_pass . ", luego cambiela para más seguridad en su cuenta de usuario.";
					enviar_mails($this->config->item('site_email'), $this->config->item('site_name'), $this->input->post('email'), '', '', 'Requerimiento de nueva contraseña', mensaje_alerta_correo('Envio de nueva contraseña', $mensaje, $this->input->post('email')));
					$this->session->set_flashdata('custom_success', 'Su nueva contraseña fue enviada a su cuenta de email.');
					redirect('user/login');
				}
				else {
					$this->session->set_flashdata('custom_error', 'Usuario no se encuentra en la base de datos o su cuenta no est&aacute; activa.');
					redirect('user/recuperar');
				}

			}
			$this->load->model('facebook_full_model');
			$this->load->model('facebook/mdl_facebook');
			$data['url_login'] = $this->facebook_full_model->get_url_login();
			$this->template->write('title', 'Recuperaci&oacute;n de contrase&ntilde;a');
			$this->template->write_view('system_messages', 'dashboard/system_messages', $data);
			$this->template->write_view('content', 'recover');
			$this->template->render();
		}
		else
			redirect('user');
	}

	//cambiar contrasena
	public function contrasena() {

		$params = array();
		$user_id = $this->session->userdata('sesion_id_user');
		$this->form_validation->set_rules('pass', 'Contraseña', 'required|trim|xss_clean');
		$this->form_validation->set_rules('repeatpass', 'Repetir contraseña', 'required|trim|matches[pass]|xss_clean');

		if ($this->mdl_usuario->validate()) {

			$params = array();
			$user_id = $this->session->userdata('sesion_id_user');
			$this->form_validation->set_rules('pass', 'Contraseña', 'required|trim|xss_clean');
			$this->form_validation->set_rules('repeatpass', 'Repetir contraseña', 'required|trim|matches[pass]|xss_clean');

			if ($this->mdl_usuario->validate()) {
				echo 'valido';
				$this->session->set_flashdata('custom_error', 'Usuario no se encuentra en la base de datos o no activo su cuenta.');
			}

			//devolviendo datos de la bd edit
			if (!$_POST AND $user_id) {
				$this->mdl_usuario->prep_validation($user_id);
			}


			$this->template->write('title', 'Configuraci&oacute;n de cuenta ');
			$this->template->write_view('content', 'cuenta_edit');
			$this->template->render();
		}
	}

	//recuperar link de activacion
	public function link_activacion() {
		if (!$this->milib->logueado()) {
			if ($this->validate_email_and_pass('email')) {

				$params['where']['email'] = $this->input->post('email');
				$params['select'] = 'email, status, codigo_activacion';
				$params['return_row'] = true;
				$user = $this->mdl_usuario->get($params);
				if (count($user) > 0) {
					if ($user->status == 0) {
						$this->load->helper('enviar_mail');
						$mensaje = "Bienvenido nuevamente a Acercate-eventos, el link de activación de su cuenta que solicitó es el siguiente: " . anchor('user/activate/' . $user->codigo_activacion, 'Activar cuenta') . ", dele clic para hacer la confirmación de su cuenta de usuario.";
						enviar_mails($this->config->item('site_email'), $this->config->item('site_name'), $this->input->post('email'), $cc, $bcc, 'Confirmación de creación de cuenta', mensaje_alerta_correo('Reenvio de link de activación de cuenta', $mensaje, $this->input->post('email')));
						$this->session->set_flashdata('custom_success', 'Su link de activaci&oacute;n fue enviado a su cuenta de email.');
						redirect('user/login');
					} else {
						$this->session->set_flashdata("custom_error", "El email ingresado ya est&aacute; registrado, si olvido su contraseña recuperela en este formulario");
						redirect('user/recuperar');
					}
				} else {
					$this->session->set_flashdata('custom_error', 'Usuario no est&aacute; registrado en la base de datos, puede hacerlo desde este formulario.');
					redirect('user/registro');
				}
			}
			$this->load->model('facebook_full_model');
			$this->load->model('facebook/mdl_facebook');
			$data['url_login'] = $this->facebook_full_model->get_url_login();

			$this->template->write('title', 'Recuperaci&oacute;n de link de activaci&oacute;n');
			$this->template->write_view('system_messages', 'dashboard/system_messages', $data);
			$this->template->write_view('content', 'link');
			$this->template->render();
		}
		else
			redirect('user');
	}

	//envia nuevo pass al usuario
	public function enviar_nuevo_pass($email, $nuevo_pass) {
		$success = FALSE;
		$params = array();
		$params['where']['email'] = $email;
		$params['where']['status'] = 1;
		$params['select'] = 'usuario_id';
		$params['return_row'] = true;

		$query = $this->mdl_usuario->get($params);
		if (count($query) > 0) {
			//$params['pass'] = $nuevo_pass;
			//array('pass' => generar_md5($nuevo_pass));
			$success = $this->mdl_usuario->save(array('pass' => generar_md5($nuevo_pass)), $query->usuario_id, false);
			if ($success)
				return $success;
			else
				return FALSE;
		}
		else
			return $success;
	}

	public function participar() {

		$evento_id = uri_assoc('evento_id');
		$this->load->model('mdl_inscrito');
		$params = array();
		$params['evento_id'] = $evento_id;
		$params['usuario_id'] = $this->session->userdata('sesion_id_user');
		$params['codigo_inscripcion'] = str_repeat('0', 9 - strlen($params['usuario_id'])) . $params['usuario_id'];
		$success = $this->mdl_inscrito->save($params, null, false);
		if ($success)
			$this->session->set_flashdata('custom_success', 'Se ha inscrito al evento con &eacute;xito');
		else
			$this->session->set_flashdata('custom_error', 'Hubo un error inscribiendole al evento, intente de nuevo');
		redirect("evento");
	}

	//pagina de listado de categorias (suscripcion a categorias)
	public function suscripcion() {
		$user_id = $this->session->userdata('sesion_id_user');
		$params['select'] = 'ciudad_id';
		$params['where']['usuario_id'] = $user_id;
		$params['return_row'] = true;
		$users = $this->mdl_usuario->get($params);
		$data['cat_with_events'] = $this->milib->categorias_con_eventos();

		if($users->ciudad_id != 0){
			//datos categoria de eventos
			$this->load->model('categoria/mdl_categoria');
			$this->load->model('suscrito/mdl_suscrito');

			$params = array();
			$paramsc = array();

			$params['select'] = 'categoria_id, nombre, descripcion, img, url';
			$params['order']['nombre'] = 'asc';
			$data['categorias'] = $this->mdl_categoria->get($params);

			//obteniendo suscripciones de un user
			$params['select'] = 'categoria_id, usuario_id';
			$params['where']['usuario_id'] = $user_id;
			$data['suscripciones'] = $this->mdl_suscrito->get($params);
			$data['ciudad'] = $users->ciudad_id;
			$this->template->set_template('full-width');
			$this->template->add_js(base_js() . "evento/suscripcion.js");
			//$this->template->add_js($javascript_inline, 'embed', false);
			$this->template->write('title', 'Suscripci&oacute;n a categor&iacute;a de evento');
			$this->template->write_view('content', 'suscripcion', $data);
			$this->template->render();
		}
		else{
			$data['ciudad'] = $users->ciudad_id;
			$this->template->set_template('full-width');
			$this->template->write('title', 'Suscripci&oacute;n a categor&iacute;a de evento');
			$this->template->write_view('system_messages', 'dashboard/system_messages');
			$this->template->write_view('content', 'suscripcion');
			$this->template->render();

		}
	}

	//accion de suscribirse a las categorias
	public function suscribir() {
		$last_index = $this->input->post('last_index');
		$start = strlen(base_url());
		$last_index = substr($last_index, $start);
		if ($this->milib->logueado()) {
			$this->load->model('suscrito/mdl_suscrito');
			$categoria_id = uri_assoc('categoria');
			$paramsc['usuario_id'] = $this->session->userdata('sesion_id_user');
			$paramsc['categoria_id'] = $categoria_id;
			if ($this->mdl_suscrito->save($paramsc, false, false)) {
				echo '<button class="btn btn-warning desuscribirse" id="' . $categoria_id . '">Suscrito</button>';
			}
			else {
				$this->session->set_userdata('last_index', $last_index);
				$this->session->set_flashdata('custom_error', 'Necesita iniciar sesi&oacute;n para realizar la acci&oacute;n seleccionada');
				echo 'redir';
			}
		}
		else {
			$this->session->set_userdata('last_index', $last_index);
			$this->session->set_flashdata('custom_error', 'Necesita iniciar sesi&oacute;n para realizar la acci&oacute;n seleccionada');
			echo 'redir';
		}
	}

	//accion de desuscribirse a las categorias
	public function desuscribir() {
		$last_index = $this->input->post('last_index');
		$start = strlen(base_url());
		$last_index = substr($last_index, $start);
		if ($this->milib->logueado()) {
			$this->load->model('suscrito/mdl_suscrito');
			$categoria_id = uri_assoc('categoria');
			$paramsc['usuario_id'] = $this->session->userdata('sesion_id_user');
			;
			$paramsc['categoria_id'] = $categoria_id;
			//eliminamos las suscripciones de un user
			if ($this->mdl_suscrito->delete($paramsc, false)) {
				echo '<button class="btn btn-success suscribirse" id="' . $categoria_id . '">Suscribirse</button>';
			}
			else {
				$this->session->set_userdata('last_index', $last_index);
				$this->session->set_flashdata('custom_error', 'Necesita iniciar sesi&oacute;n para realizar la acci&oacute;n seleccionada');
				echo 'redir';
			}
		}
		else {
			$this->session->set_userdata('last_index', $last_index);
			$this->session->set_flashdata('custom_error', 'Necesita iniciar sesi&oacute;n para realizar la acci&oacute;n seleccionada');
			echo 'redir';
		}
	}

	//subir imagen de usuario
	public function foto() {
		$this->load->helper('upload_files');

		$results = subir_imagen("config_user", "filename", "", TRUE, "");

		//si subio la foto modificamos al user
		if (isset($results['isuploaded']) == "exito") {
			$user_img = $this->mdl_usuario->get(array('select' => 'img',
				'where' => array('usuario_id' => $this->session->userdata('sesion_id_user')),
				'return_row' => true
					));
			if ($user_img !== NULL && $user_img->img != '') {
				/* borramos el thumb y el standard previo */
				$ruta_thumb = $user_img->img . '_thumb.jpg';
				borrar_imagen($results['file_path'], $ruta_thumb);

				$ruta_standard = $user_img->img . '_standard.jpg';
				borrar_imagen($results['file_path'], $ruta_standard);
			}

			$params['img'] = $results['raw_name'];
			$this->mdl_usuario->save($params, $this->session->userdata('sesion_id_user'), false);
			$this->session->set_flashdata('custom_success', 'Foto cambiada correctamente.');
			redirect('user/cuenta');
		} else {
			$this->session->set_flashdata('custom_error', 'Lo sentimos no pudimos subir su foto, int&eacute;ntelo nuevamente.' . $results['upload_picture_error']);
			redirect('user/cuenta');
		}
	}

	public function test() {
		/*$data['mi_ip'] = $_SERVER['REMOTE_ADDR'];
		$this->template->write_view('content','user/test/test', $data);
		$this->template->render();
		// header("refresh: 5; url=". base_url());
		return;
	*/
		$this->template->add_js("https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places", "import", false);
		$this->template->add_js(base_js() . "gmaps.js");
		$this->template->add_js(base_url() . "assets/image_crud/js/fileuploader.js", "import", false, 'footer', true);
		$this->template->add_css(base_url() . "assets/image_crud/css/fileuploader.css", "link", false, true);
		$upload_url = site_url("evento/evento_async/subir_imagen_ajax");
		$base_url  =base_url();
		$javascript_inline = "
			function foto_evento(filename, full_path) {
				$('#img').val(filename);
				$('#foto-evento').attr('src', full_path + filename + '_standard.jpg');
				if ($('.qq-upload-list').children('li').length == 2) {
				$('.qq-upload-list li:first-child').remove();
				}
				$('.qq-upload-list li:first-child').append('(cargar otra imagen si quiere reemplazar)');
				//console.log('reloading imagen de evento: %s_thumb.jpg y full_path: %s', filename, full_path + filename + '_thumb.jpg');
			}
			function createUploader() {
				var uploader = new qq.FileUploader({
				element: document.getElementById('file-uploader'),
				template: '<div class=\"qq-uploader\">' +
				'<div class=\"qq-upload-drop-area\"><span>soltar archivos aqui para subir</span></div>' +
				'<div class=\"qq-upload-button\">elegir foto</div>' +
				'<ul class=\"qq-upload-list\"></ul>' +
				'</div>',
				fileTemplate: '<li>' +
				'<span class=\"qq-upload-file\"></span>' +
				'<span class=\"qq-upload-spinner\"></span>' +
				'<span class=\"qq-upload-size\"></span>' +
				'<a class=\"qq-upload-cancel\" href=\"#\">subida cancelada</a>' +
				'<span class=\"qq-upload-failed-text\">error subiendo, vuelva a intentarlo</span>' +
				'</li>',
				action: '$upload_url',
				debug: true,
				onComplete: function(id, fileName, responseJSON){
				console.log(id);
				console.log(fileName);
				console.log(responseJSON);
				if ($('#img').val() != '') {
					$.ajax({
						type: 'post',
						url: '".$base_url."evento/evento_async/eliminar_img/' + $('#img').val()
					});
				}
				foto_evento(responseJSON['filename'], responseJSON['full_path']);
			}
			});
			}
			createUploader();
			$('a[rel=popover]').popover().click(function() {
			$(this).popover('show');
			return false;
			}).mouseleave(function(){
			$(this).popover('hide');
			});


			var map;
			var service;
			var infowindow;

			function initialize() {
			  var pyrmont = new google.maps.LatLng(-13.5276491,-71.93392279999999);

			  map = new google.maps.Map(document.getElementById('map'), {
			      mapTypeId: google.maps.MapTypeId.ROADMAP,
			      center: pyrmont,
			      zoom: 15
			    });

			  var request = {
			    location: pyrmont,
			    radius: '500',
			    types: ['store']
			  };

			  service = new google.maps.places.PlacesService(map);
			  service.search(request, callback);
			}

			function callback(results, status) {
			  if (status == google.maps.places.PlacesServiceStatus.OK) {
			    for (var i = 0; i < results.length; i++) {
			      var place = results[i];
			      console.log(place);
			      // createMarker(results[i]);
			    }
			  }
			}
		";
		// var_dump($this->template->config);exit;
		$this->template->add_js($javascript_inline, "embed");
		$this->template->write_view('content', 'test');
		$this->template->render();
	}

	public function inscritos($evento_id)
	{
		$this->load->model("inscrito/mdl_inscrito");
		$data['inscritos'] = $this->mdl_inscrito->listar_inscritos($evento_id);
		$this->template->set_template("full-width");
		if (!$this->milib->logueado())
			$this->template->add_js(base_js() . "user/no_logueado.php");
		$this->template->write("header_title", "Listado de participantes");
		$this->template->write("title", "listado de los participantes de este evento");
		$this->template->write_view("content", "inscrito/index", $data);
		$this->template->render();
	}

	function _verificar_email_registrado($email) {
		if($email != ''){
			$params = array();
			$params['select'] = 'usuario_id';
			$params['where']['email'] = $email;
			$params['return_row'] = true;
			$query = $this->mdl_usuario->get($params);
			if (count($query) == 1) {
				return true;
			}
			$this->form_validation->set_message('_verificar_email_registrado', 'El email ingresado no est&aacute; registrado en Acercate-eventos. <a href="'.base_url().'user/registro">Reg&iacute;strese aqu&iacute;</a>.');
			return false;
		}
	}

	function condiciones() {
		$data['about'] = 'condiciones';
		$this->template->write("header_title", "Condiciones de uso del servicio");
		$this->template->write("title", "Condiciones de uso del servicio");
		$this->template->write_view("content", "about", $data);
		$this->template->render();
	}

	function politicas() {
		$data['about'] = 'politicas';
		$this->template->write("header_title", "Pol&iacute;ticas de uso del servicio");
		$this->template->write("title", "Pol&iacute;ticas de uso del servicio");
		$this->template->write_view("content", "about", $data);
		$this->template->render();
	}

	public function _remap($method='', $params = array()) {
			if (!method_exists($this, $method)) {
				echo Modules::run("error");
			}
			else
				call_user_func_array(array($this, $method), $params);
	}

}

?>