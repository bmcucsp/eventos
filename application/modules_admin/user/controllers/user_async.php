    <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_async extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper(array('uri'));

        $this->load->library(array('redir'));
    }
    /* para imagenes */

    function eliminar_img_user($img) {
        $standard = base_path_foto_user() . $img . "_standard.jpg";
        @unlink($standard);
        $thumb = base_path_foto_user() . $img . "_thumb.jpg";
        @unlink($thumb);
    }

    function subir_imagen_ajax() {
        $this->load->library("image_moo");
        $this->load->helper("upload_files");
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);

        /* para la imagen */
        $nombre_foto = $_GET['qqfile'];
        $dot = strrpos($nombre_foto, ".", -1);
        $raw_name = substr($nombre_foto, 0, $dot);
        $extension = substr($nombre_foto, $dot);
        $results = array();
        $results['raw_name'] = $raw_name;
        $results['file_ext'] = $extension;
        $results['file_path'] = base_path_foto_user();
        $path = $results['file_path'] . $raw_name . ".jpg";
        $results['full_path'] = $path;

        $target = fopen($path, "w");
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        /* Resizing to 1024 x 768 if its required */
        list($width, $height) = getimagesize($path);
        if ($width > 1024 || $height > 768) {
            $this->image_moo->load($path)->resize(1024, 768)->save($path, true);
        }
        $this->load->config("upload_images");
        $config = $this->config->item("config_user");
        $results = redimensionar($results, "config_user", $config);
        /* para usuario */
        $this->load->model('mdl_usuario');
        $usuario_id = $this->session->userdata('sesion_id_user');
        $this->mdl_usuario->save(array('img' => $results['raw_name']), $usuario_id, false);
        /* ------------------------------------- */
        echo json_encode((object) array('success' => true, 'filename' => $results['raw_name'], 'full_path' => base_url_foto_user()));
    }

}

?>