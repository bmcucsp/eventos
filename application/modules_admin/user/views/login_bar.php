<li id="loginbar_block" class="dropdown">
	<?php echo anchor('#', 'Iniciar Sesi&oacute;n <b class="caret"></b>', array('class' =>'dropdown-toggle', 'data-toggle' => 'dropdown', 'id' => 'focus_login')) ?>
	<ul class="dropdown-menu">
		<li class="dropdown-block">
			<div class="centered-text">
			
				<p>
				<?php echo anchor('user/registro', 'Registrarse', array('class' => 'btn btn-info registrarse')) ?>
				</p>
				
				<?php echo form_open(site_url('user/login'), array('class' => 'form-inline dropdown-login')); ?>
					<div class="control-group email">
						<div class="controls">
							<input type="text" id="inputEmail" placeholder="Email"
								name="email">
						</div>
					</div>
					<div class="control-group clave">
						<div class="controls">
							<input type="password" id="inputPassword" placeholder="Contrase&ntilde;a"
								name="pass">
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<button type="submit" class="btn btn-success entrar">Entrar</button>
						</div>
					</div>
				</form>
			
				<div class="loginbar_social">
					<p>O con&eacute;ctate con:</p>
					<div class="facebook-login">
						
						<?php
						$CI =& get_instance();
						$CI->load->model('facebook_full_model');
						$url_login = $CI->facebook_full_model->get_url_login();
						echo $url_login;
						?>
					</div>
					<div class="twitter-login">
						<a href="<?php echo base_url();?>user/log/twitter">
						<img src="<?php echo base_img()?>twitter.png" alt="Inicio de sesi&oacute;n con Twitter" />
						</a>
					</div>
				</div>
			
			</div>
		</li>
	</ul>
</li>

<li id="loginbar_block_link" class="dropdown">
	<?php echo anchor('user/login', 'Ingresa o Reg&iacute;strate') ?>
</li>