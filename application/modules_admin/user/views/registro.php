<div class="login_registro">
<h5>Sólo necesitas tu dirección de email una contraseña y un nombre de Usuario.</h5>
<?php echo form_open(site_url('user/registro'), array('class' => 'form-inline dropdown-login')); ?>
<div class="control-group username">
            <div class="controls">
                <input type="text" id="inputUsername" placeholder="Nombre de usuario" name="username">
            </div>
        </div>
<div class="control-group email">
    <div class="controls">
        <input type="text" id="inputEmail" placeholder="Email" name="email">
    </div>
</div>
<div class="control-group clave">
    <div class="controls">
        <input type="password" id="inputPassword" placeholder="Contrase&ntilde;a" name="pass">
    </div>
</div>
<div class="control-group">
    <div class="controls botones_form">
        <button type="submit" class="btn btn-success entrar">Entrar</button>
        <?php echo anchor('user/login', 'Iniciar sesi&oacute;n', array('class' => 'btn btn-info registrarse')) ?>
    </div>
</div>
<?php echo form_close(); ?>
</div>
<div class="login_registro">
    <div style="margin-bottom: 10px"><?php echo $url_login; ?></div>
    <div style="margin-bottom: 10px"><a href="<?php echo base_url(); ?>user/log/twitter"><img src="<?php echo base_img() ?>login-twitter.jpg" alt="Inicio de sesi&oacute;n con Twitter" /></a></div>
</div>
<div class="opciones_cuenta" style="margin: 0 auto;">
    <p class="muted">¿Olvidaste la contraseña? , recup&eacute;rala <?php echo anchor('user/recuperar', 'aqui'); ?></p>
    <p class="muted">Quieres que te reenviemos tu link de activaci&oacute;n?, recuperalo <?php echo anchor('user/link', 'aqui'); ?></p>
</div>
<div class="terms">
    <ul class="terminos">
        <li><a href="/servicio/condiciones">Condiciones del servicio</a></li>
        <li><a href="/servicio/politicas">Política de privacidad</a></li>
    </ul>
</div>
