<div class="row-fluid">
	<div class="span6">
		<div id="cuenta_esceniko">
			<p>
			<?php
				if ($es_vista_login)
					echo anchor('user/registro', 'Registrate', array('class' => 'btn btn-info registrarse'));
				else
					echo anchor('user/login', 'Iniciar sesi&oacute;n', array('class' => 'btn btn-info registrarse'))
			?>
			</p>
			<?php
				if ($es_vista_login) {
					echo form_open(site_url('user/login'), array('class' => 'form-inline dropdown-login'));
				}
				else {
					echo form_open(site_url('user/registro'), array('class' => 'form-inline dropdown-login'));
					?>
					<div class="control-group username">
			            <div class="controls">
			                <input type="text" id="inputUsername" placeholder="Nombre de usuario" name="username" value="<?php echo set_value('username'); ?>" />
			            </div>
			        </div>
				<?php
				}
			?>
			<div class="control-group email">
				<div class="controls">
					<input type="text" id="inputEmail" placeholder="Email" name="email" value="<?php echo set_value('email'); ?>" />
				</div>
			</div>

			<div class="control-group clave">
				<div class="controls">
					<input type="password" id="inputPassword" placeholder="Contrase&ntilde;a" name="pass">
				</div>
			</div>

			<div class="control-group">
				<div class="controls botones_form">
					<button type="submit" class="btn btn-success entrar">Entrar</button>
				</div>
			</div>

			<?php echo form_close(); ?>

		</div>
	</div>
	<div class="span6">
		<div id="cuenta_social">
			<p>O con&eacute;ctate con:</p>
			<p class="facebook_button">
				<?php echo $url_login; ?>
			</p>
			<p class="twitter_button">
				<a href="<?php echo base_url(); ?>user/log/twitter"><img
					src="<?php echo base_img() ?>twitter.png"
					alt="Inicio de sesi&oacute;n con Twitter" /> </a>
			</p>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span6 opciones_cuenta">
		<h3>No puedes ingresar a tu cuenta?</h3>
		<ul>
			<li>Olvidaste la contrase&ntilde;a? Recup&eacute;rala <?php echo anchor('user/recuperar', 'aqu&iacute;'); ?></li>
			<li>Quieres que te reenviemos tu link de activaci&oacute;n? Recup&eacute;ralo <?php echo anchor('user/link', 'aqu&iacute;'); ?></li>
		</ul>
	</div>
	<div class="span6 terminos_condiciones">
		<h3>T&eacute;rminos y condiciones</h3>
		<ul>
			<li><a href="/servicio/condiciones">Condiciones del servicio</a></li>
			<li><a href="/servicio/politicas">Política de privacidad</a></li>
		</ul>
	</div>
</div>