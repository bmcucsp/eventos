<div class="row-fluid">
	<div class="span4">
		<h2>Publica un nuevo evento</h2>
		<p>Aprovecha las ventajas que te ofrecen los servicios de Acercate-eventos y promociona tu evento por Internet.</p>
		<center>
		<a href="<?php echo base_url(); ?>evento/agregar"><button class="btn btn-large btn-info">Crear nuevo evento</button></a>
		</center>

		<h2>Estad&iacute;sticas</h2>

		<div id ="estadistica" class="row-fluid">
			<div class="span4 dato">
				Proximos Eventos
				<br /><span><?php echo count($eventos_inscritos); ?></span>
			</div>
			<div class="span4 dato">
				Mis Eventos Creados
				<br /><span><?php echo $totalEvents; ?></span>
			</div>
			<div class="span4 dato">
				<a href="<?php echo base_url()?>user/suscripcion">Mis Suscripciones</a>
				<br /><span><?php echo count($suscripciones); ?></span>
			</div>
		</div>

		<div class="info-datos-usuario">
			<h2>Datos</h2>
			<?php
				$arreglo = $this->milib->user_campos_incompletos($this->session->userdata('sesion_id_user'));
				$danger = 0;
				if($arreglo["faltan"] > 0) {
					$danger = intval($arreglo["faltan"]/$arreglo["total"]*100);
					$success = 100 - $danger;
				}
				else {
					$success = 100;
				}
			?>
			<p>Tus datos est&aacute;n <?php echo $success . "%" ?> completos.</p>
			<div class="progress progress-striped active">
				<div class="progress">
					<div class="bar bar-success" style="width: <?php echo $success . "%"; ?>;"></div>
					<?php if ($danger > 0){ ?>
					<div class="bar bar-danger" style="width: <?php echo $danger . "%"; ?>;"></div>
					<?php } ?>
				</div>
			</div>

			<?php if ($danger > 0){ ?>
			<div class="alert alert-info">
				<?php
					foreach ($arreglo["campo"] as $value) {
						echo $value."<br/>";
					}
				?>
			</div>
			<p>Puede cambiar los datos de su cuenta <a href="<?php echo base_url()?>user/cuenta">aqu&iacute;</a></p>
			<?php } ?>

		</div>
	</div><!-- span -->

	<div class="span8">
		<ul class="nav nav-tabs" id="myTab">
			<li><a href="#eventos_inscritos" data-toggle="tab">Pr&oacute;ximos Eventos</a></li>
			<li><a href="#eventos_creados">Mis Eventos</a></li>
			<li><a href="#eventos_internet">Eventos por Internet</a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active" id="eventos_inscritos" >
				<h2>Eventos Inscritos</h2>
				<?php $this->load->view('lista-eventos-inscritos') ?>
			</div>
			<div class="tab-pane" id="eventos_creados">
				<h2>Mis Eventos Creados</h2>
				<?php $this->load->view('lista-eventos-usuario') ?>
			</div>
			<div class="tab-pane" id="eventos_internet">
				<h2>Eventos por Internet</h2>
				<?php $this->load->view('lista-eventos-online') ?>
			</div>
		</div>
	</div><!-- span -->
</div>