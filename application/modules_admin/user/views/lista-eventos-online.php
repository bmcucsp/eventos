<?php //var_dump($eventos_online); exit; ?>
<?php foreach ($eventos_online as $evento_relacionado) : ?>

<div class="evento-relacionado">

	<div class="right left-padded">

		<div class="cuando">
			<?php echo format_date_to_show($evento_relacionado->nearest_date) ?>
		</div>
		
		<div class="opciones_admin">
				<div class="boton-participar-<?php echo $evento_relacionado->evento_id; ?> boton-participar">
					<?php if (!$logged_in) : ?>
					<button class="btn isesion participar" data-evento-id="<?php echo $evento_relacionado->evento_id; ?>" rel="tooltip" data-original-title="Inicia sesi&oacute;n para participar en este evento">
						Participar
					</button>
					<?php else :
					if (isset($mis_eventos) && !in_array($evento_relacionado->evento_id, $mis_eventos)): ?>
					<button class="btn btn-success participar" data-evento-id="<?php echo $evento_relacionado->evento_id; ?>">
						Participar
					</button>
					<?php else : ?>
					<div class="btn-group">
						<?php if ($usuario_id == $evento_relacionado->usuario_id) : ?>
						<button class="btn btn-warning" data-toggle="dropdown">Asistir&eacute;</button>
						<?php else : ?>
						<button class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
							Asistir&eacute; <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li>
								<a style="cursor: pointer" data-evento-id="<?php echo $evento_relacionado->evento_id; ?>" class="cancelar_asistencia">Cancelar mi asistencia</a>
							</li>
						</ul>
						<?php endif; ?>
					</div>
					<?php endif; endif;?>
				</div>
		</div>
		
	</div>

	<div class="thumb-image">
		<a href="<?php echo base_url() ?>evento/<?php echo $evento_relacionado->url; ?>">
			<?php
			$img_thumb = ($evento_relacionado->img) ? base_url_foto_evento() . $evento_relacionado->img . "_thumb.jpg" : base_img() . "/categorias/" . $evento_relacionado->categoria_img;
			echo '<img width="90" height="60" src="' . $img_thumb . '">';
			?>
		</a>
	</div>

	<div class="nombre">
		<a href="<?php echo base_url() ?>evento/<?php echo $evento_relacionado->url; ?>">
			<?php echo $evento_relacionado->nombre ?>
		</a>
	</div>

	<div class="donde">
		<?php echo $evento_relacionado->lugar; ?>, ubicado en <?php echo $evento_relacionado->ubicacion; ?>, <?php echo $evento_relacionado->ciudad; ?> - <?php echo $evento_relacionado->pais; ?>
	</div>

</div><!-- evento -->

<?php endforeach; ?>