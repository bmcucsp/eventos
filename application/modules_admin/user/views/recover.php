<div class="row-fluid">
	<div class="span6">
		<div id="cuenta_esceniko">
    	<label>Ingrese su Email y luego le enviaremos una contrase&ntilde;a temporal a su cuenta de correo electr&oacute;nico.</label>		
			<?php echo form_open(site_url('user/recuperar'), array('class' => 'form-inline dropdown-login')); ?>	
			
			<div class="control-group email">
				<div class="controls">
					<input type="text" id="inputEmail" placeholder="Email" name="email">
				</div>
			</div>
			
			<div class="control-group">
				<div class="controls botones_form">
					<button type="submit" class="btn btn-info entrar">Enviar</button>
				</div>
			</div>
			
			<?php echo form_close(); ?>
			
		</div>
	</div>
	<div class="span6">
		<div id="cuenta_social">
			<p>O con&eacute;ctate con:</p>
			<p class="facebook_button">
				<?php echo $url_login; ?>
			</p>
			<p class="twitter_button">
				<a href="<?php echo base_url(); ?>user/log/twitter"><img
					src="<?php echo base_img() ?>twitter.png"
					alt="Inicio de sesi&oacute;n con Twitter" /> </a>
			</p>
		</div>
	</div>	
</div>

<div class="row-fluid">
	<div class="span6 opciones_cuenta">
		<h3>No puedes ingresar a tu cuenta?</h3>
		<ul>
			<li>¿Aún no tienes cuenta? Crea una <?php echo anchor('user/registro', 'aquí'); ?></li>
			<li>Quieres que te reenviemos tu link de activaci&oacute;n? Recup&eacute;ralo <?php echo anchor('user/link', 'aqui'); ?></li>
		</ul>
	</div>
	<div class="span6 terminos_condiciones">
		<h3>T&eacute;rminos y condiciones</h3>
		<ul>
			<li><a href="/servicio/condiciones">Condiciones del servicio</a></li>
			<li><a href="/servicio/politicas">Política de privacidad</a></li>
		</ul>
	</div>
</div>