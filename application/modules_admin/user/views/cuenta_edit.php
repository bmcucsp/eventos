<div class="row-fluid">
	<div class="span4">
		<h2>Estad&iacute;sticas</h2>

		<div id ="estadistica" class="row-fluid">
			<div class="span4 dato">
				Proximos Eventos
				<br /><span><?php echo count($eventos_inscritos); ?></span>
			</div>
			<div class="span4 dato">
				Mis Eventos Creados
				<br /><span><?php echo count($eventos); ?></span>
			</div>
			<div class="span4 dato">
				<a href="<?php echo base_url()?>user/suscripcion">Mis Suscripciones</a>
				<br /><span><?php echo count($suscripciones); ?></span>
			</div>
		</div>

		<div class="info-datos-usuario">
			<h2>Datos</h2>
			<?php
				$arreglo = $this->milib->user_campos_incompletos($this->session->userdata('sesion_id_user'));
				$danger = 0;
				if($arreglo["faltan"] > 0) {
					$danger = intval($arreglo["faltan"]/$arreglo["total"]*100);
					$success = 100 - $danger;
				}
				else {
					$success = 100;
				}
			?>
			<p>Tus datos estan <?php echo $success . "%" ?> completos.</p>
			<div class="progress progress-striped active">
				<div class="progress">
					<div class="bar bar-success" style="width: <?php echo $success . "%"; ?>;"></div>
					<?php if ($danger > 0){ ?>
					<div class="bar bar-danger" style="width: <?php echo $danger . "%"; ?>;"></div>
					<?php } ?>
				</div>
			</div>

			<?php if ($danger > 0){ ?>
			<div class="alert alert-info">
				<?php
					foreach ($arreglo["campo"] as $value) {
						echo $value."<br/>";
					}
				?>
			</div>
			<p>Puedes editar tu cuenta <a href="<?php echo base_url()?>user/cuenta">aqu&iacute;</a></p>
			<?php } ?>

		</div>
	</div><!-- span -->

	<div class="span8">
		<?php $this->load->view('dashboard/system_messages'); ?>

		<ul class="nav nav-tabs" id="myTab">
			<li><a href="#datos_personal" data-toggle="tab">Datos personales</a></li>
            <li><a href="#datos_pass">Cambiar contrase&ntilde;a</a></li>
            <li><a href="#foto">Foto</a></li>
		</ul>

		<div class="tab-content">
            <div id="datos_personal" class="tab-pane active">
            	<?php
            		$err_nombres = form_error('nombres');
            		$err_apellidos = form_error('apellidos');
            		$err_username = form_error('username');
            		$err_email = form_error('email');
            		$err_ciudad_id = form_error('ciudad_id');
            	?>
            	<p>Complete <strong>todos</strong> los campos del siguiente formulario:</p>
            	<form class="form-horizontal" method="post" action="<?php echo site_url($this->uri->uri_string()); ?>">
	                <div class="control-group <?php echo $err_nombres != '' ? 'error' : ''; ?>">
	                	<label class="control-label" for="nombres">Nombres:</label>
	                    <div class="controls">
	                        <input type="text" name="nombres" placeholder="Tu nombre *" value="<?php echo $this->mdl_usuario->form_value('nombres'); ?>" />
	                    </div>
	                </div>
	                <div class="control-group <?php echo $err_apellidos != '' ? 'error' : ''; ?>">
	                	<label class="control-label" for="apellidos">Apellidos:</label>
	                    <div class="controls">
	                        <input type="text" name="apellidos" placeholder="Tus apellidos *" value="<?php echo $this->mdl_usuario->form_value('apellidos'); ?>" />
	                    </div>
	                </div>
	                <div class="control-group <?php echo $err_username != '' ? 'error' : ''; ?>">
	                	<label class="control-label" for="username">Username:</label>
	                    <div class="controls">
	                        <input type="text" name="username" placeholder="Nombre de usuario *" value="<?php echo $this->mdl_usuario->form_value('username'); ?>" />
	                    </div>
	                </div>
	                <div class="control-group <?php echo $err_email != '' ? 'error' : ''; ?>">
	                	<label class="control-label" for="email">Email:</label>
	                    <div class="controls">
	                        <input type="text" name="email" placeholder="Correo electrónico *" value="<?php echo $this->mdl_usuario->form_value('email'); ?>" />
	                    </div>
	                </div>
	                <div class="control-group">
	                	<label class="control-label" for="fecha_nac">Fecha de Nacimiento:</label>
	                    <div class="controls">
	                        <input type="text" name="fecha_nac" class="datepicker" placeholder="Fecha de nacimiento" value="<?php echo format_date($this->mdl_usuario->form_value('fecha_nac')); ?>" />
	                    </div>
	                </div>
	                <div class="control-group">
	                	<label class="control-label" for="pais_id">Pais:</label>
	                    <div class="controls">
	                        <select name="pais_id" id="pais_id">
	                            <option value="">Su pa&iacute;s:</option>
	                            <?php foreach ($paises as $pais) : ?>
	                                <option value="<?php echo $pais->pais_id; ?>" <?php if ($pais_id == $pais->pais_id) : ?> selected="selected" <?php endif; ?> ><?php echo $pais->nombre; ?></option>
	                            <?php endforeach; ?>
	                        </select>
	                    </div>
	                </div>
	                <div class="control-group <?php echo $err_ciudad_id != '' ? 'error' : ''; ?>">
	                	<label class="control-label" for="ciudad_id">Ciudad:</label>
	                    <div class="controls">
	                        <select name="ciudad_id" id="ciudad_id">
	                            <option value="">Seleccione primero su pa&iacute;s:</option>
	                            <?php
	                            if (isset($ciudades))
	                                foreach ($ciudades as $ciudad) :
	                                    ?>
	                                    <option value="<?php echo $ciudad->ciudad_id; ?>" <?php if ($ciudad_id == $ciudad->ciudad_id) : ?> selected="selected" <?php endif; ?> ><?php echo $ciudad->nombre; ?></option>
	                                <?php endforeach; ?>
	                        </select>
	                    </div>
	                </div>
	                <div class="control-group">
	                	<div class="controls">
                			<input class="btn btn-info" type="submit" value="Modificar datos" />
	                	</div>
	                </div>
            	</form>
            </div>
            <div id="datos_pass" class="tab-pane">
                <dl>
                	<?php
                		$err_pass = form_error('pass');
                		$err_repeatpass = form_error('repeatpass');
                	 ?>
                	<form class="form-horizontal" method="post" action="<?php echo site_url($this->uri->uri_string()); ?>">
	                    <div class="control-group <?php echo $err_pass != '' ? 'error' : ''; ?>">
	                    	<label class="control-label" for="pass">Contraseña</label>
	                    	<div class="controls">
	                    		<input type="password" name="pass" placeholder="Contraseña" />
	                    	</div>
	                    </div>
	                    <div class="control-group <?php echo $err_repeatpass != '' ? 'error' : ''; ?>">
	                    	<label class="control-label" for="repeatpass">Repetir contraseña</label>
	                    	<div class="controls">
	                    		<input type="password" name="repeatpass" placeholder="Repetir contraseña" />
	                    	</div>
	                    </div>
	                    <div class="control-group">
	                    	<div class="controls">
	                    		<input class="btn btn-info" type="submit" value="Cambiar contrase&ntilde;a" name="cambiar_pass">
	                    	</div>
	                    </div>
                	</form>
                </dl>
            </div>

	        <div id="foto" class="tab-pane">
	            <div>
	                <?php
	                if ($this->mdl_usuario->form_value('img') && file_exists(base_path_foto_user() . $this->mdl_usuario->form_value('img').'_thumb.jpg')) {
	                    $src = base_url_foto_user() . $this->mdl_usuario->form_value('img').'_thumb.jpg';
	                } else {
	                    $src = base_url_foto_user() . 'anonimo.jpg';
	                }
	                ?>
	                <img src="<?= $src ?>" id="foto-user" border="0" width="48" height="48" />
	            </div>
	            <input type="hidden" id="img" value ="<?php echo $this->mdl_usuario->form_value('img');?>"/>
	            <div id="file-uploader"></div>
	        </div>

		</div>
	</div><!-- span -->
</div>

