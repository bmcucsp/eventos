<div id="lista-categorias">

	<?php
	if ((isset($ciudad) && $ciudad != 0) || isset($solo_categorias)){
		if((isset($suscripciones) && count($suscripciones) > 0) || isset($solo_categorias)) {
	?>
			<p>Puedes cambiar las categorias a las que estas suscrito.</p>
		<?php
		} else { ?>
			<p>Elige las categor&iacute;as a las cuales deseas suscribirte y recibir&aacute;s alertas cuando hayan eventos en tu ciudad.</p>
		<?php } ?>

	<form method="post" action="<?php echo site_url($this->uri->uri_string()); ?>">
		<?php $chunked = array_chunk($categorias, 6); ?>
		<?php foreach($chunked as $grouped): ?>

		<div class="row-fluid">

			<?php foreach($grouped as $categoria): ?>

			<div class="span2">

				<div class="media categoria background-white shadow">
					
					<?php if (isset($cat_with_events[$categoria->categoria_id])) : ?>
						<a href="<?=base_url()?>eventos/c_<?=$categoria->url?>">
						<img class="media-object" src="<?php echo base_img()?>categorias/<?php echo $categoria->img; ?>" alt="<?php echo $categoria->nombre; ?>">
						</a>
					<?php else : ?>
						<img class="media-object" src="<?php echo base_img()?>categorias/<?php echo $categoria->img; ?>" alt="<?php echo $categoria->nombre; ?>">
					<?php endif; ?>
					
					<div class="media-body">
						<h2 class="media-heading">
							<?php if (isset($cat_with_events[$categoria->categoria_id])) : ?>
								<a href="<?=base_url()?>eventos/c_<?=$categoria->url?>"><?php echo $categoria->nombre ?></a>
							<?php else : ?>
								<p><?php echo $categoria->nombre ?></p>
							<?php endif; ?>
						</h2>
						
						<p class="categoria-descripcion">
							<?php echo $categoria->descripcion; ?>
						</p>

						<div id="categoria-id-<?php echo $categoria->categoria_id; ?>">
							<?php
							//verificamos la suscripcion en cada categoria
							if(count($suscripciones) > 0){
		    					foreach ($suscripciones as $suscripcion) {
		    						if($suscripcion->categoria_id == $categoria->categoria_id){
		    							$var = 'btn-warning desuscribirse';
		    							$caption = 'Suscrito';
		    							$title = 'Cancelar suscripci&oacute;n';
		    							break;
		    						}
		    						else{
		    							$var = 'btn-success suscribirse';
		    							$caption = 'Suscribirse';
		    							$title = 'Suscribirse';
		    						}
		    					}
					    		echo '<button class="btn '.$var.'" id="'.$categoria->categoria_id.'" title="'.$title.'">'.$caption.'</button>';
		    					}
		    				else { ?>
								<button class="btn btn-success suscribirse" id="<?php echo $categoria->categoria_id ?>" title="Suscribirse">Suscribirse</button>
							<?php } ?>
						</div>

						<p class="suscritos">
							<div class="col_nro_sus">
								<?php echo $this->mdl_suscrito->contar_suscripciones($categoria->categoria_id); ?> suscritos
							</div>
						</p>

					</div> <!-- media-body -->
						
				</div><!-- media -->

			</div><!-- span -->

			<?php endforeach; ?>

		</div><!-- row -->

		<?php endforeach; ?>

	</form>
	
	<?php } else { ?>
		<p>Para suscribirte a una categor&iacute;a debes de completar el campo <strong>ciudad</strong> en tu cuenta, asi podras recibir noticias de eventos nuevos.</p>
		<p><a href="<?=base_url()?>user/cuenta" title="Cambiar mi ciudad">Seleccionar mi ciudad</a></p>
	<?php } ?>
	
	<p><strong>Nota: </strong> Una vez suscrito recibir&aacute;s a tu e-mail eventos que se realizar&aacute;n en tu ciudad. Por precauci&oacute;n no olvides revisar la bandeja de tu correo no deseado.</p>
	
</div>