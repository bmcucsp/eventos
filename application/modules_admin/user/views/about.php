<?php
if ($about == 'condiciones') { ?>
	<div>
	<p>
		Te agradecemos por usar los servicios de Acercate-eventos (en adelante, los "Servicios").<br /> 
	</p>
	<p>
		El uso de nuestros Servicios implica la aceptaci&oacute;n de estas condiciones. Te recomendamos que las leas detenidamente.
	</p>
	<p>
		Nuestros Servicios son muy diversos, por lo que en ocasiones se pueden aplicar condiciones o requisitos de productos adicionales (incluidas restricciones de edad). 
		Las condiciones adicionales estar&aacute;n disponibles junto con los Servicios pertinentes y formar&aacute;n parte del acuerdo que estableces con Acercate-eventos al usar nuestros servicios.
	</p>
	</div>
<?php

}
else{ ?>
	<p>
		Por medio de nuestra pol&iacute;tica de privacidad le ponemos al tanto de las debidas condiciones de uso en este sitio.
	</p>
	<p>
		La utilizaci&iacute;n de estos implica la aceptaci&oacute;n plena y sin reservas a todas y cada una de las disposiciones 
		incluidas en este Aviso Legal, por lo que si usted no est&aacute; de acuerdo con cualquiera de las condiciones aqu&iacute; 
		establecidas, no deber&aacute; usar u/o acceder a este sitio.
	</p>
	<p>
		Reservamos el derecho a modificar esta Declaraci&oacute;n de Privacidad en cualquier momento. Su uso continuo de cualquier porci&oacute;n de este sitio tras la notificaci&oacute;n o anuncio de tales modificaciones constituir&aacute; su aceptaci&oacute;n de tales cambios.
	</p>
	<p>
	<h2>Pol&iacute;tica de protecci&oacute;n de datos personales</h2>
	<p>
		Para utilizar algunos de los servicios o acceder a determinados contenidos, deber&aacute; proporcionar previamente ciertos datos de car&aacute;cter personal, que solo ser&aacute;n utilizados para el prop&oacute;sito que fueron recopilados.
	</p>
	<p>
		El tipo de la posible informaci&oacute;n que se le sea solicitada incluye, de manera enunciativa m&aacute;s no limitativa, su nombre, 
		direcci&oacute;n de correo electr&oacute;nico (e-mail), fecha de nacimiento, sexo, ocupaci&oacute;n, pa&iacute;s y ciudad de origen e intereses personales, entre otros, no toda la informaci&oacute;n solicitada al momento de participar en el sitio es obligatoria 
		de proporcionarse, salvo aquella que consideremos conveniente y que as&iacute; se le haga saber.
	</p>
	<p>
		Como principio general, este sitio no comparte ni revela informaci&oacute;n obtenida, excepto cuando haya sido autorizada por usted, o en los siguientes casos:
		<ul>
			<li> Cuando le sea requerido por una autoridad competente y previo el cumplimiento del tr&aacute;mite 
			legal correspondiente.</li>
			<li> Cuando a juicio de este sitio sea necesario para hacer cumplir las condiciones de uso y dem&aacute;s t&eacute;rminos de esta p&aacute;gina, o para salvaguardar la integridad de los dem&aacute;s usuarios o del sitio.</li>
		</ul>	
		Deber&aacute; estar consciente de que si usted voluntariamente revela informaci&oacute;n personal en l&iacute;nea 
		en un &aacute;rea p&uacute;blica, esa informaci&oacute;n puede ser recogida y usada por otros. 
		Nosotros no controlamos las acciones de nuestros visitantes y usuarios.
	</p>
	<h2>Pol&iacute;tica de informaci&oacute;n de los Eventos</h2>
	<p>
		acercate-eventos.dennisbot.com, es un portal que comparte informaci&oacute;n de eventos, el mismo que no se responsabiliza por la informaci&oacute;n que sea publicada, salvo que esta sea inapropiada seg&uacute;n la informaci&oacute;n mostrada a continuaci&oacute;n.
		<p>
		acercate-eventos.dennisbot.com no permite la publicaci&oacute;n de los siguientes tipos de eventos:
		</p>
		<ul>
			<li>Eventos sexuales, er&oacute;ticos, u otros similares.</li>
			<li>Eventos relacionados con peleas, abuso, u otros similares.</li>
			<li>Eventos promoviendo el consumo de alcohol, drogas, u otros similares.</li>
		</ul>
		Todos estos tipos de eventos ser&aacute;n retirados del sitio.
	</p>
<?php } ?>

<div class="span6 terminos_condiciones">
	<h3>T&eacute;rminos y condiciones</h3>
	<ul>
		<li><a href="/servicio/condiciones">Condiciones del servicio</a></li>
		<li><a href="/servicio/politicas">Política de privacidad</a></li>
	</ul>
</div>