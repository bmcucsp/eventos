<div class="padded right">
    <?php if ($logged_in) : ?>
        <?php $this->load->view('dashboard/btn_add', array('btn_value' => 'agregar evento')); ?>
    <?php endif; ?>
</div>
<?php
if (!empty($eventos)) {
    foreach ($eventos as $evento) {
        ?>
        <div class="row line-break"></div>
        <div class="row evento">
            <div class="span1 imagen">
                <a href="<?php echo site_url("evento/" . $evento->url); ?>">
                	<img alt="<?php echo $evento->nombre ?>" width="90" height="60" src="<?php echo $evento->img != '' ? base_url_foto_evento() . $evento->img . '_thumb.jpg' : base_img() . '/imgs-prueba/evento.jpg'; ?>"
                </a>
            </div><!-- span -->

            <div class="span6">
                <div class="nombre">
                    <a href="<?php echo base_url('evento/' . $evento->url); ?>"><?php echo $evento->nombre ?> </a>
                </div>

                <div class="descripcion"><?php echo $evento->descripcion ?></div>
                <div class="capacidad"><?php echo $evento->capacidad ?></div>
                <div class="presencial"><?php echo $evento->presencial ?></div>
                <div class="organizador"><?php echo $evento->organizador ?></div>

            </div><!-- span -->

            <div class="span2">
                <div class="categoria"> <span>Categoria:</span><?php echo $categorias[$evento->categoria_id]; ?> </div>
                <div class="fecha_inicio"><span>fecha de inicio:</span><?php echo format_date($evento->fecha_inicio); ?></div>
                <div class="fecha_fin"><span>fecha de fin:</span><?php echo format_date($evento->fecha_fin); ?></div>
                <div class="fecha_fin"><span>Cantidad de Inscritos:</span><?php echo $evento->num_inscritos; ?>&nbsp;&nbsp;<?php echo anchor("user/inscritos/".$evento->evento_id, "Inscritos");?></div>

                <?php if ($usuario_id == $evento->usuario_id) : ?>
                    <div class="controls">
                        <a href="<?php echo site_url('evento/agregar/evento_id/' . $evento->evento_id); ?>" title="<?php echo $this->lang->line('edit'); ?>"> <?php echo icon('edit'); ?></a>
                        <a href="<?php echo site_url('evento/delete/evento_id/' . $evento->evento_id); ?>" title="<?php echo $this->lang->line('delete'); ?>" onclick="javascript:if(!confirm('<?php echo $this->lang->line('confirm_delete'); ?>')) return false"><?php echo icon('delete'); ?></a>
                    </div>
                <?php endif; ?>

                <div id="boton-participar-<?php echo $evento->evento_id ?>">
                    <?php if (!$logged_in || isset($mis_eventos) && !in_array($evento->evento_id, $mis_eventos)) : ?>
                        <button class="btn btn-success participar" data-evento-id="<?php echo $evento->evento_id; ?>">
                            Participar
                        </button>
                    <?php else :
                        ?>
                        <div class="btn-group">
                            <?php if ($usuario_id == $evento->usuario_id) : ?>
                                <button class="btn btn-warning" data-toggle="dropdown">Asistiré</button>
                            <?php else : ?>
                                <button class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Asistiré <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a style="cursor: pointer" data-evento-id="<?php echo $evento->evento_id; ?>" class="cancelar_asistencia">Cancelar mi asistencia</a></li>
                                </ul>
                            <?php endif; ?>
                        </div>
                    <?php
                    endif;
                    ?>
                </div>
            </div>
            <div class="span2">
                <?php $this->load->view("social/social", array('custom_url' => true, 'evento' => $evento)); ?>
            </div>
        </div>
        <?php
    }
    if ($this->mdl_evento->page_links) {
        ?>
        <div id="loading" style="position: relative"></div>
        <div id="pagination" class="pagination pagination-centered">
            <ul>
                <?php echo $this->mdl_evento->page_links; ?>
            </ul>
        </div>
        <?php
    }
}
else
    echo 'No tenemos eventos';
?>