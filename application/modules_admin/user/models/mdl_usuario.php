<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_usuario extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'usuario';
        $this->primary_key = 'usuario.usuario_id';
        $this->select_fields = "SQL_CALC_FOUND_ROWS usuario.*";
        $this->order_by = 'usuario_id';
    }
    
}

?>