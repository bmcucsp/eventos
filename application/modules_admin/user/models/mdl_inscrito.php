<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_inscrito extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'inscrito';

        $this->primary_key = 'inscrito.usuario_id';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		inscrito.*";

        $this->order_by = 'inscrito.fecha_inscripcion';
    }

}

?>