<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_suscrito extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'suscrito';
        $this->primary_key = 'suscrito.usuario_id';
        $this->select_fields = "SQL_CALC_FOUND_ROWS suscrito.*";
        $this->order_by = 'categoria_id';
    }
    
    function contar_suscripciones($categoria_id) {
        $params['select'] = 'count(categoria_id) as nro';
        $params['where']['categoria_id'] = $categoria_id;
        $params['return_row'] = true;
        $data = $this->mdl_suscrito->get($params);
        return $data->nro;
    }
    
}

?>