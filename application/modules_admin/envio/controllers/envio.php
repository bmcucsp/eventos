<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Envio extends MX_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('mdl_envio');
    }

    public function index() {
    	redirect('/');
    }

	function enviar_mensajes_suscripcion_1() {
		
		// obteniendo los eventos de hoy
		$params = array();
	    $params['select'] = 'id, titulo_evento, fecha_evento, url_evento, nombre_user, email_user, enviado';
	    $params['where']['fecha_evento >'] = strtotime(date('d-m-Y'));
	    $params['where']['enviado'] = 2;
	    
	    $query = $this->mdl_envio->get($params);
		$this->load->helper('enviar_mail');
				
	    foreach ($query as $fila) {
	    	$mensaje = "Hay un nuevo evento en Acercate-eventos: " . anchor('evento/' . $fila->url_evento, $fila->titulo_evento) . " para el d&iacute;a ";
	    	$fecha_hoy = strtotime(date('d-m-Y'));
	    	//verificando que se envie email antes de tres dias o menos
	    	if( ($fila->fecha_evento - $fecha_hoy) <= 3*24*60*60 ){
	    		echo $mensaje.'<br />';
		    	$this->mdl_envio->save(array('enviado' => $fila->enviado - 1), $fila->id, false, false); 
		    	enviar_mails($this->config->item('site_email'), $this->config->item('site_name'), $fila->email_user, '', '', 'Nuevo evento ', mensaje_alerta_correo('Alerta de Eventos', $mensaje, $fila->email_user));
	    	}
	    } 	    	
		echo 'enviando ...';	
	}   

	function enviar_mensajes_suscripcion_2() {
		
		// obteniendo los eventos de hoy
		$params = array();
	    $params['select'] = 'id, titulo_evento, fecha_evento, url_evento, nombre_user, email_user, enviado';
	    $params['where']['fecha_evento >='] = '2 dias antes';
	    $params['where']['enviado'] = 1;

	    $query = $this->mdl_envio->get($params);
		$this->load->helper('enviar_mail');
	    foreach ($query as $fila) {
	    	$mensaje = "Hay un nuevo evento en Acercate-eventos: " . anchor('evento/' . $fila->url_evento, $fila->titulo_evento) . " para el d&iacute;a ";
	    	echo $mensaje.'<br />';
	    	$this->mdl_envio->save(array('enviado' => $fila->enviado - 1), $fila->id, false, false); 
	    	//enviar_mails($this->config->item('site_email'), $this->config->item('site_name'), $fila->email_user, '', '', 'Nuevo evento ', mensaje_alerta_correo('Alerta de Eventos', $mensaje, $fila->email_user));
	    } 	    	
		echo 'enviando ...';	
	}   
	
}

?>