<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_envio extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'envio_correo';
        $this->primary_key = 'id';
        $this->select_fields = "SQL_CALC_FOUND_ROWS envio_correo.*";
        $this->order_by = 'id';
    }
    
    //modificar por otro campo que no es el id
    public function guardar_envio($db_array, $where = NULL) {
        $success = FALSE;
        if ($where) {
            $this->db->where($where);
            $success = $this->db->update($this->table_name, $db_array);
        }
        return $success;
    }    
    
}

?>