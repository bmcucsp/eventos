<?php
/**
 * 
 * Template: Pagina Interna
 *
 */
?>
<?php $this->load->view('layout/header'); ?>

<div class="container">

    <div class="row">
        <div class="span12">
            <h1 class="titulo_log"><?php echo $title ?></h1>

        </div>

    </div>
    <!--/row-->

    <div class="row">
        <div class="span12 background-white shadow">
            <?php echo $system_messages ?>
            <?php echo $content ?>
        </div>


    </div>
    <!--/row-->

    <?php $this->load->view('layout/footer'); ?>