<table style="border: 1px solid #DDDDDD;background: #fff;font-family: Arial;color: #505050;font-size: 14px;line-height: 150%;">
<tr>
	<td style="padding: 5px;background: #0088CC;">
		<div style="color: #FFF;font-size: 19px;padding: 1px"><?php echo $title; ?></div>
	</td>
</tr>
<tr>
	<td>
	  <table style="font-family: Arial;color: #505050;font-size: 14px;line-height: 150%;padding: 10px">
		  <tr>
		  	<td>
				<?php echo $mensaje; ?>
		  	</td>
		  </tr>
		  <tr>
		  	<td>
		  		<?php echo $otros; ?>
		  	</td>
		  </tr>
	  </table>
	</td>
</tr>
<tr>
	<td style="padding: 5px;background: #F3F3F3">
		<div style="padding: 5px;background: #0088CC;color: #FFF;width: 100px;text-align: center">
		<a href="<?php echo base_url(); ?>" style="color: #FFF"> acercate-eventos.dennisbot.com</a>
		</div>
 	</td>
 </tr>
 <tr>
 	<td style="padding: 5px;font-size: 11px">
		Este es un mensaje de alerta que se envi&oacute; a <?php echo $email; ?>, desde tu p&aacute;gina de eventos <?php echo base_url(); ?>
		<p>Nosotros respetamos su derecho a la privacidad, vea nuestras pol&iacute;ticas.</p>
 	</td>
 </tr>
 </table>