<?php
/**
 *
 * Template: Pagina Interna
 *
 */
?>
<?php $this->load->view('layout/header'); ?>
<?php $this->load->view('layout/primera-navegacion'); ?>
<div class="container">

<div class="row">
    <div class="span12">
        <h1 id="tituloh1"><?php echo $title ?></h1>
    </div>
</div><!-- row -->

<div class="row">
    <div class="span12 background-white shadow">
        <div class="full-padded"">
            <?php echo $system_messages ?>
            <?php echo $content ?>
        </div>
    </div>
</div><!-- row -->
<?php $this->load->view('layout/footer'); ?>