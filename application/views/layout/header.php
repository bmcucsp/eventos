<!DOCTYPE html>
<html lang="<?php echo $this->config->item('language_attributes') ?>" >
    <head>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
        <meta charset="utf-8">

        <title><?php echo ($header_title) ? $header_title . " | " . $this->config->item('site_name') : "Eventos | " . $this->config->item('site_name') ?></title>

        <link type="text/css" href="<?php echo base_url(); ?>assets/jquery/ui-themes/myclientbase/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta name="keywords" content="<?php echo isset($seo_keyword)?$seo_keyword:''; ?>" />
        <meta name="description" content="<?php echo isset($seo_descripcion)?$seo_descripcion:''; ?>" />
        
        <meta name="author" content="esceniko.com">

        <!-- Bootstrap -->
        <link href="<?php echo bootstrap_css(); ?>bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo font_awesome_css(); ?>font-awesome.min.css">

        <!-- Css -->
        <link href="<?php echo base_css(); ?>estilo.css" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo base_img(); ?>mascara.png">
        <?php echo $_scripts_header ?>

        <!-- Internal Css -->
        <?php echo $_styles ?>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>

    <body>

