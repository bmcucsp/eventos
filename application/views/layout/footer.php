<?php
/**
 *
 * Footer Template
 *
 */
?>
		<hr>
		<footer>
                    <p style="text-align: center">&copy;<?php echo date("Y") . " " . $this->config->item('site_name') ?></p>
		</footer>

    </div><!-- /end .container -->
    <script src="<?php echo base_jquery();?>jquery-1.7.1.min.js"></script>
    <script src="<?php echo bootstrap_js(); ?>bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery/jquery-ui-1.8.16.custom.min.js"></script>
    <script src="<?php echo base_jquery(); ?>jquery.maskedinput-1.2.2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_js(); ?>config.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#box-errors").fadeIn(4000);
			$("#box-errors").fadeOut(6000);
			$('.fc-event').tooltip({'placement': 'top'});
			$('a.isesion').tooltip({'placement': 'top'});
			$('button.isesion').tooltip({'placement': 'top'});
            $('.bs-docs-sidenav').affix({
            	offset: {
                	top: function () { return $(window).width() <= 980 ? 290 : 210 }
                }
            })
       });
	</script>
	<?php echo $_scripts ?>
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=8700798; 
var sc_invisible=1; 
var sc_security="a01d8933"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="web statistics"
href="http://statcounter.com/free-web-stats/"
target="_blank"><img class="statcounter"
src="http://c.statcounter.com/8700798/0/a01d8933/1/"
alt="web statistics"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->	
  </body>
</html>