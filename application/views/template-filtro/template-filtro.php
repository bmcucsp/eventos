<?php
/**
 *
 * Template: Pagina Interna
 *
 */
?>
<?php $this->load->view('layout/header'); ?>
<?php $this->load->view('layout/primera-navegacion'); ?>

<div class="container">
<?php //var_dump($this->session->userdata);exit; ?>
	<div class="row">
		<div class="span12">
			<h1><?php echo $title ?></h1>
		</div>
	</div><!-- row -->

    <div class="row">
		<div class="span12 background-white shadow">
			<div class="full-padded">

				<div class="row-fluid">

					<div class="span12">
						<?php echo $barra_navegacion; ?>
					</div>

				</div><!-- row-fluid -->

				<div class="row-fluid">

					<div class="span12">
						<?php echo $breadcrumbs; ?>
					</div>

				</div><!-- row-fluid -->

				<div class="row-fluid">

					<div class="span6">
						<?php echo $system_messages ?>
						<?php echo $content ?>
					</div>

					<div class="span3">
						<?php echo anchor("eventos/calendario", "Ver calendario con todos los eventos"); ?>
            			<div id="calendario_mes_barra_lateral" class="bs-docs-sidenav" data-spy="affix" data-offset-top="200">
            				<?php echo $calendario ?>
            			</div>
						<!-- <div style="margin-top: 10px">
							<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fesceniko&amp;width=287&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:287px; height:290px;" allowTransparency="true"></iframe>
						</div>  -->            			
					</div>

					<div class="span3">
						<?php //echo $publicidad ?>
						<script type="text/javascript"><!--
						google_ad_client = "ca-pub-6968540297762595";
						/* sidebar160x600-esceniko */
						google_ad_slot = "6572735937";
						google_ad_width = 160;
						google_ad_height = 600;
						//-->
						</script>
						<script type="text/javascript"
						src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
						</script>						
					</div>

				</div><!-- row-fluid -->

			</div>
		</div>
	</div>

<?php $this->load->view('layout/footer'); ?>