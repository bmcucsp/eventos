<?php
/**
 *
 * Template: Pagina de Inicio
 *
 */
?>

<?php $this->load->view('layout/header'); ?>

<?php $this->load->view('layout/primera-navegacion'); ?>

<div class="container">
	<?php echo $error404; ?>
    <h1><?php echo $title ?></h1>
    <?php echo $content ?>
<?php $this->load->view('layout/footer'); ?>