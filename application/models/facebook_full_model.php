<?php
class Facebook_full_model extends CI_Model {

	var $datos;
	var $statusUpdate;

	function __construct() {
        parent::__construct();
       	$config = array('appId'  => '445573915488608',
                        'secret' => 'eb648275de9eddd69ac57e9885def526',
       					'cookie' => false,
                        'fileUpload' => true // Indicates if the CURL based @ syntax for file uploads is enabled.
                        );
		$this->load->library('Facebook', $config);
    }

    //funcion para obtener la data de face con los campos necesarios
    function get_facebook_data($campos) {

        $user = $this->facebook->getUser();
    	// We may or may not have this data based on whether the user is logged in.
        //
        // If we have a $user id here, it means we know the user is logged into
        // Facebook, but we don't know if the access token is valid. An access
        // token is invalid if the user logged out of Facebook.
        $profile = null;
        if($user)
        {
            try {
                // Proceed knowing you have a logged in user who's authenticated.
                $profile = $this->facebook->api('/me?fields='.$campos);
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
        }
        //.'?cancel=1'

        /*$fb_data = array('me' => $profile,
                        'uid' => $user,
                        'loginUrl' => $this->facebook->getLoginUrl(array('display' => 'popup', 'cancel_url'=> base_url(), 'req_perms' => 'email,user_birthday')),
                        'logoutUrl' => $this->facebook->getLogoutUrl(),
                    );*/
        $this->session->set_userdata('fb_data', $fb_data);
        return $this->session->userdata('fb_data');
    }

    //comprobacion y obtencion del user uid de facebook
    function get_user_facebook() {
        	$user = $this->facebook->getUser();
    		//$this->user = $this->facebook->getUser();
        	return $user;
    }

    //obtenemos la url del login a face con la data
    function get_url_login_sola() {
    	$loginUrl = $this->facebook->getLoginUrl(
	            //array('scope' => 'email,offline_access,publish_stream, user_birthday,user_location,user_work_history,user_about_me,user_hometown',
	            array('scope' => 'email,publish_stream,user_birthday,user_location,user_work_history,user_about_me,user_hometown',
	                'redirect_uri' => base_url().'user/logfacebook', 'display' => 'popup',
	            	'cancel_url' => base_url().'user/login?cancel=1'));
    	return $loginUrl;
    }

    //obtener el link de la url del login y logout de facebook
    function get_url_login() {
    	if(!$this->facebook->getUser()) {
	    	$url = $this->facebook->getLoginUrl(
		            //array('scope' => 'email,offline_access,publish_stream, user_birthday,user_location,user_work_history,user_about_me,user_hometown',
				array('scope' => 'email,publish_stream,user_birthday,user_location,user_work_history,user_about_me,user_hometown',
		                'redirect_uri' => base_url().'user/logfacebook', 'display' => 'popup',
		            	 'cancel_url' => base_url().'user/login?cancel=1'));
			//$loginUrl = '<a href="'.$url.'" onclick="login();return false;"><img src="'.base_img().'facelogin.gif" border="0" /></a>';
			$loginUrl = '<a href="'.$url.'"><img src="'.base_img().'facebook.png" border="0" /></a>';
	    	return $loginUrl;
    	}
    	else {
			$url = $this->facebook->getLogoutUrl(array('next' => base_url().'user/logout'));
			$logoutUrl = '<a href="'.$url.'"><img src="'.base_img().'facelogout.png" border="0" /></a>';
    		return $logoutUrl;
    	}
    }

    //verificacion si user esta autenticado
    function user_autenticacion() {
		if($this->get_user_facebook()){
	      	try {
	        	// Proceed knowing you have a logged in user who's authenticated.
	        	$user_profile = $this->facebook->api('/me');
	        	return $user_profile;
	      	}catch (FacebookApiException $e) {
	        	//you should use error_log($e); instead of printing the info on browser
	        	$this->show_info($e);
	        	$this->user = null;
      		}
      		return '';
    	}
    }

    /*
     * descripcion b�sica del usuario
     */
    function get_user_basic_description() {
    	$user_face = $this->get_user_facebook();
    	if($user_face){
	      	try {
	        	// Proceed knowing you have a logged in user who's authenticated.
	        	$user_profile = $this->facebook->api('/me');
	      	}catch (FacebookApiException $e) {
	        	//you should use error_log($e); instead of printing the info on browser
	        	//$this->show_info($e);
	        	error_log($e);
	        	$this->user = null;
      		}
			return $this->facebook->api("/$user_face");
    	}
    	//$userInfo = $facebook->api("/$this->user");
    }

    /*
     * obtener las conexiones del user:
     * ex: activities, albums, books, events, movies, friendlists, friends, games, links
     */
    function get_user_connections($type) {
    	$user_face = $this->get_user_facebook();
    	if($user_face){
	        try{
	            $datos = $this->facebook->api("/$user_face/".$type);
	            return $datos;
	        }
	        catch(Exception $o){
	        	error_log($e);
	            //$this->show_info($o);
	        }
	        return '';
    	}
    }

    /*
     * funcion para publicar en el muro
     */
    function publicar_estado($mensaje, $link = '', $picture = '', $name = '', $descripcion = '') {
    	$user_face = $this->get_user_facebook();
		try {
        	$statusUpdate = $this->facebook->api("/$user_face/feed", 'post',
        	array('message'=> $mensaje, 'link' => $link,'picture' => $picture, 'name' => $name, 'description' => $descripcion));
        	return $statusUpdate;
		}
        catch (FacebookApiException $e){
        	error_log($e);
        	//$this->show_info($e);
		}
		return '';
    }

    /*
     * mostrar la info en arrays
     */
    function show_info($info){
        echo '<pre>';
        print_r($info);
        echo '</pre>';
    }

}