<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		// echo exec('whoami');
		/* lineas de código para probar si se puede enviar correo con smtp o no */
		// $this->load->helper('enviar_mail');
		// enviar_mails('info@dennisbot.com', 'INFO - Admin', 'info@dennisbot.com', '', '', 'asunto :)', 'mensaje, se desarrolla más contenido :)');
		// log_message('debug', 'se acaba de enviar un correo electrónico');

		/* cargamos el evento */
		$this->load->module('evento');
		/* y luego pasamos como parametro el mensaje personalizado */
		$this->evento->pagina_inicio(true);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */