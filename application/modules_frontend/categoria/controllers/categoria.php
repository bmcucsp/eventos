<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Categoria extends MX_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('session');
		$this->load->model(array('mcb_data/mdl_mcb_data'));
		$this->load->language('mcb', $this->mdl_mcb_data->setting('default_language'));

		$this->load->helper(array('uri', 'mcb_icon'));

		$this->load->library(array('form_validation', 'redir'));

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');


		$this->_post_handler();

		$this->load->model('mdl_categoria');
	}

	public function index() {

		$this->load->model('mdl_categoria_table');

		$params = array(
			'paginate' => TRUE,
			'limit' => $this->config->item('results_per_page'),
			'page' => uri_assoc('page', 2)
		);

		$order_by = uri_assoc('order_by');

		$order = uri_assoc('order');

		switch ($order_by) {
		  case 'nombre':
			  $params['order_by'] = 'categoria.nombre ' .$order;
			  break;
		  case 'descripcion':
			  $params['order_by'] = 'categoria.descripcion ' .$order;
			  break;
		  case 'categoria_padre':
			  $params['order_by'] = 'categoria.categoria_padre ' .$order;
			  break;
		  case 'img':
			  $params['order_by'] = 'categoria.img ' .$order;
			  break;
		  case 'url':
			  $params['order_by'] = 'categoria.url ' .$order;
			  break;
		  case 'created_at':
			  $params['order_by'] = 'categoria.created_at ' .$order;
			  break;
		  case 'status':
			  $params['order_by'] = 'categoria.status ' .$order;
			  break;
		  default:
			  $params['order_by'] = 'categoria.categoria_id ' .$order;
		  }


		$data = array(
			'categorias' => $this->mdl_categoria->get($params),
			'table_headers' => $this->mdl_categoria_table->get_table_headers()
		);

		/*
		 * assets
		 */
		/*
		$this->template->add_css('nombre_archivo.css', 'link', false);
		$this->template->add_js('nombre_archivo.js', 'import', false);
		$javascript_inline = '
			$(".clase").accion({
			  //operaciones
			})
		';
		$this->template->add_js($javascript_inline, 'embed', false);
		*/

		/*
		 * template
		 */
		$this->template->write('header_title', 'Listado de Categoria');
		$this->template->write('title', 'Listado de Categoria');
		$this->template->write_view('content', 'index', $data);

		//$this->template->write_view('system_messages', 'dashboard/system_messages');
		//$this->template->write_view('sidebar', 'sidebar_view');

		$this->template->render();
	}

	public function form() {

		$categoria_id = uri_assoc('categoria_id');

		if ($this->mdl_categoria->validate()) {

			$this->mdl_categoria->save();

			$categoria_id = ($categoria_id) ? $categoria_id : $this->db->insert_id();

			redirect('categoria/form/categoria_id/' . $categoria_id);

		} else {

			if (!$_POST && $categoria_id) {

				$this->mdl_categoria->prep_validation($categoria_id);
			}

			/*
			 * template
			*/
			$this->template->write('header_title', 'Administrar Categoria');
			$this->template->write('title', 'Administrar Categoria');
			$this->template->write_view('content', 'form');

			$this->template->write_view('system_messages', 'dashboard/system_messages');
			//$this->template->write_view('sidebar', 'sidebar_view');

			$this->template->render();
		}
	}

	public function _post_handler() {

		if ($this->input->post('btn_add'))
			redirect('categoria/form');
		if ($this->input->post('btn_cancel'))
			redirect($this->session->userdata('last_index'));
	}

	public function delete() {
		$categoria_id = uri_assoc('categoria_id');
		if ($categoria_id) {
			$this->mdl_categoria->delete(array('categoria_id' => $categoria_id));
		}
		redirect('categoria');
	}

	function ver() {
		$user_id = $this->session->userdata('sesion_id_user');
		$data['cat_with_events'] = $this->milib->categorias_con_eventos();
		$data['is_logged_in'] = $user_id;
		if ($user_id) {
			$this->load->model("user/mdl_usuario");
			$params['select'] = 'ciudad_id';
			$params['where']['usuario_id'] = $user_id;
			$params['return_row'] = true;
			$users = $this->mdl_usuario->get($params);
			if($users->ciudad_id != 0) {
				//datos categoria de eventos
				$this->load->model('categoria/mdl_categoria');
				$this->load->model('suscrito/mdl_suscrito');

				$params = array();
				$paramsc = array();

				$params['select'] = 'categoria_id, nombre, descripcion, img, url';
				$params['order']['nombre'] = 'asc';
				$data['categorias'] = $this->mdl_categoria->get($params);

				//obteniendo suscripciones de un user
				$params['select'] = 'categoria_id, usuario_id';
				$params['where']['usuario_id'] = $user_id;
				$data['suscripciones'] = $this->mdl_suscrito->get($params);
				$data['ciudad'] = $users->ciudad_id;
				$this->template->set_template('full-width');
				$this->template->add_js(base_js() . "evento/suscripcion.js");
				//$this->template->add_js($javascript_inline, 'embed', false);
				$this->template->write('title', 'Categor&iacute;as de eventos');
				$this->template->write_view('content', 'user/suscripcion', $data);
				$this->template->render();
			}
			else {
				$data['ciudad'] = $users->ciudad_id;
				$this->template->set_template('full-width');
				$this->template->write('title', 'Categor&iacute;as de eventos');
				$this->template->write_view('system_messages', 'dashboard/system_messages');
				$this->template->write_view('content', 'user/suscripcion');
				$this->template->render();
			}
		}
		else {
			$this->load->model('suscrito/mdl_suscrito');
			$this->load->model('categoria/mdl_categoria');

			$params = array();
			$paramsc = array();

			$params['select'] = 'categoria_id, nombre, descripcion, img, url';
			$params['order']['nombre'] = 'asc';
			$data['categorias'] = $this->mdl_categoria->get($params);
			if($this->milib->logueado()){
				//obteniendo suscripciones de un user
				$params['select'] = 'categoria_id, usuario_id';
				$params['where']['usuario_id'] = $this->session->userdata('sesion_id_user');
				$data['suscripciones'] = $this->mdl_suscrito->get($params);
				//$data['ciudad'] = $users->ciudad_id;
			}
			if (!isset($data['suscripciones']))
				$data['suscripciones'] = array();
			$this->template->set_template('full-width');
			if (!$this->milib->logueado()) {
				$this->template->add_js(base_js() . 'user/no_logueado.js');
			}
			$this->template->add_js(base_js() . "evento/suscripcion.js");
			$this->template->write('title', 'Categor&iacute;as de eventos');
			$data['solo_categorias'] = true;
			
			//seo
			$data['seo_keyword'] = "categorias eventos, negocios, comedia, donaciones, gastronomia, cine, musica, danzas, religioso, deportes, medicina, ciencia, tecnologia, arte, sociales, viajes y turismo, eventos, publicidad, cine, publicar, publicaciones";
			$data['seo_descripcion'] = "Categorias Acercate-eventos, publicar eventos en internet, portal de eventos - Esceniko.com";
			
			$this->template->write('header_title', 'Categor&iacute;as de eventos');			
			$this->template->write_view('content', 'user/suscripcion', $data);
			$this->template->render();
		}
	}

}

?>