<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_categoria extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'categoria';

        $this->primary_key = 'categoria.categoria_id';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		categoria.*";

        $this->order_by = 'categoria_id';
        
    }

    public function validate() {
        
        $this->form_validation->set_rules('nombre','nombre', 'required|trim|xss_clean');
        $this->form_validation->set_rules('descripcion','descripcion', 'trim|xss_clean');
        $this->form_validation->set_rules('categoria_padre','categoria_padre', 'trim|xss_clean');
        $this->form_validation->set_rules('img','img', 'trim|xss_clean');
        $this->form_validation->set_rules('url','url', 'required|trim|xss_clean');
        $this->form_validation->set_rules('created_at','created_at', 'trim|xss_clean');
        $this->form_validation->set_rules('status','status', 'trim|xss_clean');

        return parent::validate();
    }

    public function save() {

        parent::save(parent::db_array(), uri_assoc('categoria_id'));
    }
    
    public function eventos_categoria_proximos($categoria_id) {
    		$where = 'where categoria_id = '.$categoria_id;
			$statement = "
			   select SQL_CALC_FOUND_ROWS recent_events.evento_id from
					(select * from(
							select *, fecha_inicio as nearest_date from evento
							where es_repetido = 0
							and fecha_inicio >= UNIX_TIMESTAMP(now())
						) evento
						$where
					union
					select evento.*, rep.nearest_date from
						(select a.evento_id, a.nearest_date from(
								select evento_id,
								FROM_UNIXTIME(repeat_start) rs,
								if (UNIX_TIMESTAMP(now()) >  repeat_start,
								if (repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval > repeat_end,
									repeat_start + floor((repeat_end -  repeat_start) / repeat_interval) * repeat_interval,
									repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval
								   )
								,repeat_start
								) as nearest_date
								from events_meta
								where repeat_end >= UNIX_TIMESTAMP(now())
							) a where a.nearest_date >= UNIX_TIMESTAMP(now())
						) rep
						inner join
						evento
						on rep.evento_id = evento.evento_id
					) recent_events
					$where
					order by nearest_date";
		$query = $this->db->query($statement);
		$res_repeated = ($query && $query->num_rows > 0) ? $query->result() : array();
		return $res_repeated;
    }
    
   

}
?>