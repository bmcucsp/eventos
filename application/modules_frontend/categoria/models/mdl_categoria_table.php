<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_categoria_table extends CI_Model {

    public function get_table_headers() {

        $order = (uri_assoc('order')) == 'asc' ? 'desc' : 'asc';

        $headers = array(
        'categoria_id' => anchor('categoria/index/order_by/categoria_id/order/'.$order, 'categoria_id'),
        'nombre' => anchor('categoria/index/order_by/nombre/order/'.$order, 'nombre'),
        'descripcion' => anchor('categoria/index/order_by/descripcion/order/'.$order, 'descripcion'),
        'categoria_padre' => anchor('categoria/index/order_by/categoria_padre/order/'.$order, 'categoria_padre'),
        'img' => anchor('categoria/index/order_by/img/order/'.$order, 'img'),
        'url' => anchor('categoria/index/order_by/url/order/'.$order, 'url'),
        'created_at' => anchor('categoria/index/order_by/created_at/order/'.$order, 'created_at'),
        'status' => anchor('categoria/index/order_by/status/order/'.$order, 'status'),
        );


        return $headers;
    }

}

?>
