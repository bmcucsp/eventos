<div class="padded">
    <form method="post" action="<?php echo site_url($this->uri->uri_string()); ?>">

        <dl>
            <dd>
            	<input type="hidden" name="categoria_id" value="<?php echo $this->mdl_categoria->form_value('categoria_id'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* nombre </label></dt>
            <dd>
                <input type="text" name="nombre" value="<?php echo $this->mdl_categoria->form_value('nombre'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* descripcion </label></dt>
            <dd>
                <textarea cols="26" rows="8" name="descripcion" id="descripcion"><?php echo $this->mdl_categoria->form_value('descripcion'); ?></textarea>
            </dd>
        </dl>
        <dl>
            <dt><label>* categoria_padre </label></dt>
            <dd>
                <input type="text" name="categoria_padre" value="<?php echo $this->mdl_categoria->form_value('categoria_padre'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* img </label></dt>
            <dd>
                <input type="text" name="img" value="<?php echo $this->mdl_categoria->form_value('img'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* url </label></dt>
            <dd>
                <input type="text" name="url" value="<?php echo $this->mdl_categoria->form_value('url'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* created_at </label></dt>
            <dd>
                <input type="text" name="created_at" value="<?php echo $this->mdl_categoria->form_value('created_at'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* status </label></dt>
            <dd>
                <input type="text" name="status" value="<?php echo $this->mdl_categoria->form_value('status'); ?>" />
            </dd>
        </dl>



        <input type="submit" id="btn_cancel" class="btn btn-danger" name="btn_cancel" value="<?php echo $this->lang->line('cancel'); ?>" />
        <input type="submit" id="btn_submit" class="btn btn-success" name="btn_submit" value="<?php echo $this->lang->line('submit'); ?>" />

    </form>
</div><!-- padded -->

<div class="controles">
	<ul class="nav nav-list">
		<li><?php echo anchor('categoria/index', '<i class="icon-list"></i> Listado de Categorias') ?></li>
	</ul>
</div>