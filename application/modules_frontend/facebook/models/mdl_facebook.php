<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_facebook extends MY_Model {

    public function __construct() {
        parent::__construct();

        $this->table_name = 'user_facebook';

        $this->primary_key = 'user_facebook.id_user_facebook';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		user_facebook.*";

        $this->order_by = 'usuario_id';
    }

}
?>