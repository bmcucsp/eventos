<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facebook extends MX_Controller {
	
	public function __construct() {
        parent::__construct();

        $this->_post_handler();
        $this->load->model('mcb_data/mdl_mcb_data');
        $this->load->model('mdl_facebook');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */