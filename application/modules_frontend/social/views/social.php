<div id="social">
    <ul>
    	<li id="facebook_megusta">
    		<div id="fb-root"></div>
			<script src="http://connect.facebook.net/es_ES/all.js#appId=128897243865016&amp;xfbml=1"></script>
			<fb:like href="" send="false" layout="button_count" width="110" show_faces="false" font=""></fb:like>
    	</li>
        <li id="facebook_recomendar">
            <div id="fb-root"></div>
            <script>
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {
                        console.log("return");
                        return;
                    }
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            </script>
            <?php if (!isset($custom_url) || !$custom_url): ?>
            	<fb:like send="false" layout="button_count" width="450" show_faces="false" action="recommend" font="arial"></fb:like>
        	<?php else: ?>
            	<div class="fb-like" data-href="<?php echo site_url('evento/' . $evento->url); ?>" data-send="true" data-width="450" data-show-faces="true" data-action="recommend"></div>
        	<?php endif; ?>
		</li>
        
        <li id="google">
            <!-- Place this tag where you want the share button to render. -->
            <div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php echo site_url("evento/" . $evento->url); ?>"></div>

            <!-- Place this tag after the last share tag. -->
            <script>
                window.___gcfg = {
                    lang: 'es-419'
                };

                (function() {
                    var po = document.createElement('script');
                    po.type = 'text/javascript';
                    po.async = true;
                    po.src = 'https://apis.google.com/js/plusone.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(po, s);
                })();
            </script>
        </li>

        <li id="twitter">
            <?php
            $url = "";
            $url = (isset($custom_url) && $custom_url) ? 'data-url="' . site_url("evento/" . $evento->url) . '"' : "";
            ?>
            <a href="https://twitter.com/share" class="twitter-share-button" <?php echo $url; ?> data-via="tecnosurf1" data-lang="es" data-dnt="true">Twittear</a>
            <script>
                    !function(d,s,id){
                    var js,fjs=d.getElementsByTagName(s)[0];
                    if(!d.getElementById(id)){
                        js=d.createElement(s);
                        js.id=id;
                        js.src="//platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js,fjs);
                    }
                }(document,"script","twitter-wjs");
            </script>
        </li>

        <!-- 
        <li id="mail">
            <a href="#" alt="compartir evento con un amigo">
                <img alt="compartir evento con un amigo" src="<?php echo base_img() . "enviar-a-un-amigo.png" ?>">
            </a>
        </li>
        -->
    </ul>
</div><!-- social -->