<table style="width: 100%;">
    <tr>

        <?php
                foreach ($table_headers as $key => $value) { ?>
                <th><?php echo $table_headers[$key]; ?></th>
                <?php } ?>
                <th><?php echo $this->lang->line('actions'); ?></th>


    </tr>

    <?php foreach ($tests as $test) { ?>
        <tr>
            <?php
            foreach ($test as $value) {
                ?>
                <td><?php echo $value; ?></td>
            <?php } ?>


            <td class="last">
                <a href="<?php echo site_url('test/form/idtest/' . $test->idtest); ?>" title="<?php echo $this->lang->line('edit'); ?>">
                    <?php echo icon('edit'); ?>
                </a>
                <a href="<?php echo site_url('test/delete/idtest/' . $test->idtest); ?>" title="<?php echo $this->lang->line('delete'); ?>" onclick="javascript:if(!confirm('<?php echo $this->lang->line('confirm_delete'); ?>')) return false">
                    <?php echo icon('delete'); ?>
                </a>
            </td>
        </tr>

    <?php } ?>
</table>

<?php if ($this->mdl_test->page_links) { ?>
    <div id="pagination">
        <?php echo $this->mdl_test->page_links; ?>
    </div>
<?php } ?>