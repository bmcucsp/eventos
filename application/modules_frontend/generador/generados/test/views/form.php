<div class="grid_10" id="content_wrapper">

    <div class="section_wrapper">

        <?php $this->load->view('dashboard/system_messages'); ?>

        <div class="content toggle">

            <form method="post" action="<?php echo site_url($this->uri->uri_string()); ?>">
                
                <dl>
                <input type="hidden" name="idtest" value="<?php echo $this->mdl_test->form_value('idtest'); ?>" />
                </dl>
                <dl>
                	<dt><label>* titulo </label></dt>
                <dd>
<input type="text" name="titulo" value="<?php echo $this->mdl_test->form_value('titulo'); ?>" />
                </dd>
                	</dl>
                <dl>
                	<dt><label>* contenido </label></dt>
                <dd>
<input type="text" name="contenido" value="<?php echo $this->mdl_test->form_value('contenido'); ?>" />
                </dd>
                	</dl>
                <dl>
                	<dt><label>* url </label></dt>
                <dd>
<input type="text" name="url" value="<?php echo $this->mdl_test->form_value('url'); ?>" />
                </dd>
                	</dl>

                
                <?php foreach ($custom_fields as $custom_field) { ?>
                    <dl>
                        <dt><label><?php echo $custom_field->field_name; ?>: </label></dt>
                        <dd><input type="text" name="<?php echo $custom_field->column_name; ?>" id="<?php echo $custom_field->column_name; ?>" value="<?php echo $this->mdl_empleado->form_value($custom_field->column_name); ?>" /></dd>
                    </dl>
                <?php } ?>

                <div style="clear: both;">&nbsp;</div>

                <input type="submit" id="btn_submit" name="btn_submit" value="<?php echo $this->lang->line('submit'); ?>" />
                <input type="submit" id="btn_cancel" name="btn_cancel" value="<?php echo $this->lang->line('cancel'); ?>" />

            </form>

        </div>
        <?php echo anchor('test/index', 'lists');?>

    </div>

</div>
