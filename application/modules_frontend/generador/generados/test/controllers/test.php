<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Test extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('session');

        $this->load->model(array('mcb_data/mdl_mcb_data'));
        
        $this->load->language('mcb', $this->mdl_mcb_data->setting('default_language'));

        $this->load->model('fields/mdl_fields');

        $this->load->helper('url');

        $this->load->database();

        $this->load->helper(array('uri', 'mcb_icon'));

        $this->load->library(array('form_validation', 'redir'));

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');


        $this->_post_handler();

        $this->load->model('mdl_test');
    }

    public function index() {

        $this->load->model('mdl_test_table');

        $params = array(
            'paginate' => TRUE,
            'limit' => $this->config->item('results_per_page'),
            'page' => uri_assoc('page')
        );

        $order_by = uri_assoc('order_by');

        $order = uri_assoc('order');

        switch ($order_by) {
          case 'titulo':
              $params['order_by'] = 'test.titulo ' .$order;
              break;
          case 'contenido':
              $params['order_by'] = 'test.contenido ' .$order;
              break;
          case 'url':
              $params['order_by'] = 'test.url ' .$order;
              break;
          default:
              $params['order_by'] = 'test.idtest ' .$order;
          }


        $data = array(
            'tests' => $this->mdl_test->get($params),
            'table_headers' => $this->mdl_test_table->get_table_headers()
        );

        $this->load->view('index', $data);
    }

    public function form() {

        $idtest = uri_assoc('idtest');
        
        if ($this->mdl_test->validate()) {
            
            $this->mdl_test->save();

            $idtest = ($idtest) ? $idtest : $this->db->insert_id();

            redirect('test/form/idtest/' . $idtest);
            
        } else {
            
            if (!$_POST && $idtest) {

                $this->mdl_test->prep_validation($idtest);
            }
            
            $this->load->view('form', array('custom_fields' => $this->mdl_test->custom_fields));
        }
    }

    public function _post_handler() {
        
        if ($this->input->post('btn_add'))
            redirect('test/form');
        if ($this->input->post('btn_cancel'))
            redirect($this->session->userdata('last_index'));
    }

    public function delete() {

        $idtest = uri_assoc('idtest');

        if ($idtest) {
            $this->mdl_test->delete(array('idtest' => $idtest));
        }
        
        redirect('test');
    }

}

?>