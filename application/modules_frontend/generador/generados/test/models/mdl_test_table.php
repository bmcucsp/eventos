<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_test_table extends CI_Model {

    public function get_table_headers() {

        $order = (uri_assoc('order')) == 'asc' ? 'desc' : 'asc';

        $headers = array(
        'idtest' => anchor('test/index/order_by/idtest/order/'.$order, 'idtest'),
        'titulo' => anchor('test/index/order_by/titulo/order/'.$order, 'titulo'),
        'contenido' => anchor('test/index/order_by/contenido/order/'.$order, 'contenido'),
        'url' => anchor('test/index/order_by/url/order/'.$order, 'url'),
        );


        return $headers;
    }

}

?>
