<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_test extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'test';

        $this->primary_key = 'test.idtest';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		test.*";

        $this->order_by = 'idtest';

        $this->custom_fields = $this->mdl_fields->get_object_fields(3);
    }

    public function validate() {
        
        $this->form_validation->set_rules('titulo','titulo', 'required|trim|xss_clean');
        $this->form_validation->set_rules('contenido','contenido', 'required|trim|xss_clean');
        $this->form_validation->set_rules('url','url', 'required|trim|xss_clean');

        foreach ($this->custom_fields as $custom_field) {

            $this->form_validation->set_rules($custom_field->column_name, $custom_field->field_name);
        }

        return parent::validate();
    }

    public function save() {

        parent::save(parent::db_array(), uri_assoc('idtest'));
    }

}
?>