<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Suscrito extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('session');

        $this->load->model(array('mcb_data/mdl_mcb_data'));
        
        $this->load->language('mcb', $this->mdl_mcb_data->setting('default_language'));

        $this->load->helper(array('uri', 'mcb_icon'));

        $this->load->library(array('form_validation', 'redir'));

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');


        $this->_post_handler();

        $this->load->model('mdl_suscrito');
    }

    public function index() {

        $this->load->model('mdl_suscrito_table');

        $params = array(
            'paginate' => TRUE,
            'limit' => $this->config->item('results_per_page'),
            'page' => uri_assoc('page')
        );

        $order_by = uri_assoc('order_by');

        $order = uri_assoc('order');

        switch ($order_by) {
          default:
              $params['order_by'] = 'suscrito.categoria_id ' .$order;
          }


        $data = array(
            'suscritos' => $this->mdl_suscrito->get($params),
            'table_headers' => $this->mdl_suscrito_table->get_table_headers()
        );

        /*
		 * assets
		 */
		/*
        $this->template->add_css('nombre_archivo.css', 'link', false);
		$this->template->add_js('nombre_archivo.js', 'import', false);
		$javascript_inline = '
	    	$(".clase").accion({
	    	  //operaciones
	    	})
		';
		$this->template->add_js($javascript_inline, 'embed', false);
		*/
		
		/*
		 * template
		 */
		$this->template->write('header_title', 'Listado de Suscrito');
		$this->template->write('title', 'Listado de Suscrito');
		$this->template->write_view('content', 'index', $data);
		
		//$this->template->write_view('system_messages', 'dashboard/system_messages');
		//$this->template->write_view('sidebar', 'sidebar_view');
		
		$this->template->render();
    }

    public function form() {

        $categoria_id = uri_assoc('categoria_id');
        
        if ($this->mdl_suscrito->validate()) {
            
            $this->mdl_suscrito->save();

            $categoria_id = ($categoria_id) ? $categoria_id : $this->db->insert_id();

            redirect('suscrito/form/categoria_id/' . $categoria_id);
            
        } else {
            
            if (!$_POST && $categoria_id) {

                $this->mdl_suscrito->prep_validation($categoria_id);
            }
            
            /*
             * template
            */
            $this->template->write('header_title', 'Administrar Suscrito');
            $this->template->write('title', 'Administrar Suscrito');
            $this->template->write_view('content', 'form', array('custom_fields' => $this->mdl_suscrito->custom_fields));
            
            $this->template->write_view('system_messages', 'dashboard/system_messages');
            //$this->template->write_view('sidebar', 'sidebar_view');
            
            $this->template->render();
        }
    }

    public function _post_handler() {
        
        if ($this->input->post('btn_add'))
            redirect('suscrito/form');
        if ($this->input->post('btn_cancel'))
            redirect($this->session->userdata('last_index'));
    }

    public function delete() {

        $categoria_id = uri_assoc('categoria_id');

        if ($categoria_id) {
            $this->mdl_suscrito->delete(array('categoria_id' => $categoria_id));
        }
        
        redirect('suscrito');
    }

}

?>