<table>
	<tr>
		<?php
                foreach ($table_headers as $key => $value) { ?>
                <th><?php echo $table_headers[$key]; ?></th>
                <?php } ?>
                <th><?php echo $this->lang->line('actions'); ?></th>

	</tr>
	
	<?php foreach ($suscritos as $suscrito) { ?>
	<tr>
		<?php foreach ($suscrito as $value) { ?>
		<td>
			<?php echo $value; ?>
		</td>
		<?php } ?>
	
		<td>
			<a href="<?php echo site_url('suscrito/form/categoria_id/' . $suscrito->categoria_id); ?>" title="<?php echo $this->lang->line('edit'); ?>">
			<?php echo icon('edit'); ?>
			</a>
			<a href="<?php echo site_url('suscrito/delete/categoria_id/' . $suscrito->categoria_id); ?>" title="<?php echo $this->lang->line('delete'); ?>" onclick="javascript:if(!confirm('<?php echo $this->lang->line('confirm_delete'); ?>')) return false">
			<?php echo icon('delete'); ?>
			</a>
		</td>
	</tr>
	<?php } ?>
</table>

<?php if ($this->mdl_suscrito->page_links) { ?>
    <div id="pagination">
        <?php echo $this->mdl_suscrito->page_links; ?>
    </div>
<?php } ?>