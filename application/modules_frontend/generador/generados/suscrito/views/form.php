<div class="padded">
<form method="post" action="<?php echo site_url($this->uri->uri_string()); ?>">

	<dl>
                <input type="hidden" name="categoria_id" value="<?php echo $this->mdl_suscrito->form_value('categoria_id'); ?>" />
                </dl>


	<?php foreach ($custom_fields as $custom_field) { ?>
	<dl>
		<dt>
			<label><?php echo $custom_field->field_name; ?>: </label>
		</dt>
		<dd>
			<input type="text" name="<?php echo $custom_field->column_name; ?>"
				id="<?php echo $custom_field->column_name; ?>"
				value="<?php echo $this->mdl_empleado->form_value($custom_field->column_name); ?>" />
		</dd>
	</dl>
	<?php } ?>

	<input type="submit" id="btn_cancel" class="btn btn-danger" name="btn_cancel" value="<?php echo $this->lang->line('cancel'); ?>" />
	<input type="submit" id="btn_submit" class="btn btn-success" name="btn_submit" value="<?php echo $this->lang->line('submit'); ?>" />

</form>
</div><!-- padded -->

<div class="controles">
	<ul class="nav nav-list">
		<li><?php echo anchor('suscrito/index', '<i class=icon-list></i> Listado de suscritos');?></li>
	</ul>
</div>