<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_suscrito_table extends CI_Model {

    public function get_table_headers() {

        $order = (uri_assoc('order')) == 'asc' ? 'desc' : 'asc';

        $headers = array(
        'categoria_id' => anchor('suscrito/index/order_by/categoria_id/order/'.$order, 'categoria_id'),
        );


        return $headers;
    }

}

?>
