<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_suscrito extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'suscrito';

        $this->primary_key = 'suscrito.categoria_id';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		suscrito.*";

        $this->order_by = 'categoria_id';

        $this->custom_fields = $this->mdl_fields->get_object_fields(3);
    }

    public function validate() {
        

        foreach ($this->custom_fields as $custom_field) {

            $this->form_validation->set_rules($custom_field->column_name, $custom_field->field_name);
        }

        return parent::validate();
    }

    public function save() {

        parent::save(parent::db_array(), uri_assoc('categoria_id'));
    }

}
?>