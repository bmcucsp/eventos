<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_local extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'local';

        $this->primary_key = 'local.local_id';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		local.*";

        $this->order_by = 'local_id';
    }

    public function validate() {

        $this->form_validation->set_rules('nombre', 'nombre', 'required|trim|xss_clean');
        $this->form_validation->set_rules('direccion', 'direccion', 'required|trim|xss_clean');
        $this->form_validation->set_rules('referencia', 'referencia', 'trim|xss_clean');
        $this->form_validation->set_rules('latitud', 'latitud', 'trim|xss_clean');
        $this->form_validation->set_rules('longitud', 'longitud', 'trim|xss_clean');
        $this->form_validation->set_rules('ciudad_id', 'ciudad_id', 'required|trim|xss_clean');


        return parent::validate();
    }

    public function save() {

        parent::save(parent::db_array(), uri_assoc('local_id'));
    }

}

?>