<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_local_table extends CI_Model {

    public function get_table_headers() {

        $order = (uri_assoc('order')) == 'asc' ? 'desc' : 'asc';

        $headers = array(
        'local_id' => anchor('local/index/order_by/local_id/order/'.$order, 'local_id'),
        'nombre' => anchor('local/index/order_by/nombre/order/'.$order, 'nombre'),
        'direccion' => anchor('local/index/order_by/direccion/order/'.$order, 'direccion'),
        'referencia' => anchor('local/index/order_by/referencia/order/'.$order, 'referencia'),
        'latitud' => anchor('local/index/order_by/latitud/order/'.$order, 'latitud'),
        'longitud' => anchor('local/index/order_by/longitud/order/'.$order, 'longitud'),
        'ciudad_id' => anchor('local/index/order_by/ciudad_id/order/'.$order, 'ciudad_id'),
        );


        return $headers;
    }

}

?>
