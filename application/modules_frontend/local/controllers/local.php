<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Local extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('session');

        $this->load->model(array('mcb_data/mdl_mcb_data'));
        
        $this->load->language('mcb', $this->mdl_mcb_data->setting('default_language'));

        $this->load->helper(array('uri', 'mcb_icon'));

        $this->load->library(array('form_validation', 'redir'));

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');


        $this->_post_handler();

        $this->load->model('mdl_local');
    }

    public function index() {

        $this->load->model('mdl_local_table');

        $params = array(
            'paginate' => TRUE,
            'limit' => $this->config->item('results_per_page'),
            'page' => uri_assoc('page')
        );

        $order_by = uri_assoc('order_by');

        $order = uri_assoc('order');

        switch ($order_by) {
          case 'nombre':
              $params['order_by'] = 'local.nombre ' .$order;
              break;
          case 'direccion':
              $params['order_by'] = 'local.direccion ' .$order;
              break;
          case 'referencia':
              $params['order_by'] = 'local.referencia ' .$order;
              break;
          case 'latitud':
              $params['order_by'] = 'local.latitud ' .$order;
              break;
          case 'longitud':
              $params['order_by'] = 'local.longitud ' .$order;
              break;
          case 'ciudad_id':
              $params['order_by'] = 'local.ciudad_id ' .$order;
              break;
          default:
              $params['order_by'] = 'local.local_id ' .$order;
          }


        $data = array(
            'locals' => $this->mdl_local->get($params),
            'table_headers' => $this->mdl_local_table->get_table_headers()
        );

        /*
		 * assets
		 */
		/*
        $this->template->add_css('nombre_archivo.css', 'link', false);
		$this->template->add_js('nombre_archivo.js', 'import', false);
		$javascript_inline = '
	    	$(".clase").accion({
	    	  //operaciones
	    	})
		';
		$this->template->add_js($javascript_inline, 'embed', false);
		*/
		
		/*
		 * template
		 */
		$this->template->write('header_title', 'Listado de Local');
		$this->template->write('title', 'Listado de Local');
		$this->template->write_view('content', 'index', $data);
		
		//$this->template->write_view('system_messages', 'dashboard/system_messages');
		//$this->template->write_view('sidebar', 'sidebar_view');
		
		$this->template->render();
    }

    public function form() {

        $local_id = uri_assoc('local_id');
        
        if ($this->mdl_local->validate()) {
            
            $this->mdl_local->save();

            $local_id = ($local_id) ? $local_id : $this->db->insert_id();

            redirect('local/form/local_id/' . $local_id);
            
        } else {
            
            if (!$_POST && $local_id) {

                $this->mdl_local->prep_validation($local_id);
            }
            
            /*
             * template
            */
            $this->template->write('header_title', 'Administrar Local');
            $this->template->write('title', 'Administrar Local');
            $this->template->write_view('content', 'form');
            
            $this->template->write_view('system_messages', 'dashboard/system_messages');
            //$this->template->write_view('sidebar', 'sidebar_view');
            
            $this->template->render();
        }
    }

    public function _post_handler() {
        
        if ($this->input->post('btn_add'))
            redirect('local/form');
        if ($this->input->post('btn_cancel'))
            redirect($this->session->userdata('last_index'));
    }

    public function delete() {

        $local_id = uri_assoc('local_id');

        if ($local_id) {
            $this->mdl_local->delete(array('local_id' => $local_id));
        }
        
        redirect('local');
    }

}

?>