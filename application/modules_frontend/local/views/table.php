<table>
	<tr>
		<?php
                foreach ($table_headers as $key => $value) { ?>
                <th><?php echo $table_headers[$key]; ?></th>
                <?php } ?>
                <th><?php echo $this->lang->line('actions'); ?></th>

	</tr>
	
	<?php foreach ($locals as $local) { ?>
	<tr>
		<?php foreach ($local as $value) { ?>
		<td>
			<?php echo $value; ?>
		</td>
		<?php } ?>
	
		<td>
			<a href="<?php echo site_url('local/form/local_id/' . $local->local_id); ?>" title="<?php echo $this->lang->line('edit'); ?>">
			<?php echo icon('edit'); ?>
			</a>
			<a href="<?php echo site_url('local/delete/local_id/' . $local->local_id); ?>" title="<?php echo $this->lang->line('delete'); ?>" onclick="javascript:if(!confirm('<?php echo $this->lang->line('confirm_delete'); ?>')) return false">
			<?php echo icon('delete'); ?>
			</a>
		</td>
	</tr>
	<?php } ?>
</table>

<?php if ($this->mdl_local->page_links) { ?>
    <div id="pagination">
        <?php echo $this->mdl_local->page_links; ?>
    </div>
<?php } ?>