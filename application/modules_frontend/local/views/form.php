<div class="padded">
    <form method="post" action="<?php echo site_url($this->uri->uri_string()); ?>">

        <dl>
            <input type="hidden" name="local_id" value="<?php echo $this->mdl_local->form_value('local_id'); ?>" />
        </dl>
        <dl>
            <dt><label>* nombre </label></dt>
            <dd>
                <input type="text" name="nombre" value="<?php echo $this->mdl_local->form_value('nombre'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* direccion </label></dt>
            <dd>
                <input type="text" name="direccion" value="<?php echo $this->mdl_local->form_value('direccion'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* referencia </label></dt>
            <dd>
                <input type="text" name="referencia" value="<?php echo $this->mdl_local->form_value('referencia'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* latitud </label></dt>
            <dd>
                <input type="text" name="latitud" value="<?php echo $this->mdl_local->form_value('latitud'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* longitud </label></dt>
            <dd>
                <input type="text" name="longitud" value="<?php echo $this->mdl_local->form_value('longitud'); ?>" />
            </dd>
        </dl>
        <dl>
            <dt><label>* ciudad_id </label></dt>
            <dd>
                <input type="text" name="ciudad_id" value="<?php echo $this->mdl_local->form_value('ciudad_id'); ?>" />
            </dd>
        </dl>



        <input type="submit" id="btn_cancel" class="btn btn-danger" name="btn_cancel" value="<?php echo $this->lang->line('cancel'); ?>" />
        <input type="submit" id="btn_submit" class="btn btn-success" name="btn_submit" value="<?php echo $this->lang->line('submit'); ?>" />

    </form>
</div><!-- padded -->

<div class="controles">
	<ul class="nav nav-list">
		<li><?php echo anchor('local/index', '<i class="icon-list"></i> Listado de Localidades') ?></li>
	</ul>
</div>