<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_pais extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'pais';

        $this->primary_key = 'pais.pais_id';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		pais.*";

        $this->order_by = 'pais_id';
    }

    public function validate() {
        
        $this->form_validation->set_rules('nombre', 'nombre', 'required|trim|xss_clean');
        $this->form_validation->set_rules('code', 'code', 'required|trim|xss_clean');
        $this->form_validation->set_rules('url', 'url', 'required|trim|xss_clean');
        return parent::validate($this);
    }

    public function save() {
        
        $campos = parent::db_array();

        parent::save($campos, uri_assoc('pais_id'));
        
    }
    
    public function get_info($info) {
//        var_dump($info);exit;
        $params = array();
        $params['select'] = 'pais_id, nombre';
        $params['where'] = "code = '{$info['Country']}' or code = '{$info['CountryCode2']}' or code = '{$info['CountryCode3']}'";
        $params['where'] .= " or nombre = '{$info['CountryName']}'";
        $params['return_row'] = true;
        return parent::get($params);
    }
}

?>