<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_ciudad extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'ciudad';

        $this->primary_key = 'ciudad.ciudad_id';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		ciudad.*";

        $this->order_by = 'ciudad_id';

    }

    public function validate() {
        
        $this->form_validation->set_rules('pais_id', 'pais_id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('nombre', 'nombre', 'required|trim|xss_clean');
        $this->form_validation->set_rules('url', 'url', 'required|trim|xss_clean');
        return parent::validate($this);
    }
    
    public function save() {
        
        $campos = parent::db_array();

        parent::save($campos, uri_assoc('ciudad_id'));
        
    }
    public function get_info($info, $pais_id) {
        $params = array();
        $params['select'] = 'ciudad_id, nombre';
        $params['where'] = "pais_id = '$pais_id' and (nombre = '{$info['RegionName']}' or nombre = '{$info['CityName']}')";
        $params['return_row'] = true;
        return parent::get($params);
    }
}

?>