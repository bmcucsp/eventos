<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Evento_async extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper(array('uri'));
        $this->load->library(array('redir'));
    }

    public function cancelar_asistencia() {
        $evento_id = uri_assoc('evento_id', 4);
        $large = uri_assoc('large', 4);
        if ($large) $large = "btn-large";
        else $large = "";

        $last_index = $this->input->post('last_index');
        $start = strlen(base_url());
        $last_index = substr($last_index, $start);

        $usuario_id = $this->session->userdata('sesion_id_user');
        $this->load->model("user/mdl_inscrito");

        $params = array();
        $params['evento_id'] = $evento_id;
        $params['usuario_id'] = $usuario_id;

        if ($usuario_id) {
            $this->mdl_inscrito->delete($params, false);
            echo json_encode(
                (object)array(
                'content' => '<button class="btn btn-success '.$large.' participar" data-evento-id="'.$evento_id.'">Participar
                            </button>', 'large' => ($large == 'btn-large')
                )
            );
        } else {
            $this->session->set_userdata('last_index', $last_index);
            $this->session->set_flashdata('custom_error', 'Necesita iniciar sesi&oacute;n para realizar esta acci&oacute;n');
            echo json_encode((object)array('redir' => 'redir'));
        }
    }

    function participar() {
        $evento_id = uri_assoc('evento_id', 4);
        $large = uri_assoc('large', 4);
        if ($large) $large = "btn-large";
        else $large = "";
        $last_index = $this->input->post('last_index');

        $start = strlen(base_url());
        $last_index = substr($last_index, $start);

        $usuario_id = $this->session->userdata('sesion_id_user');
        $this->load->model('user/mdl_inscrito');
        $params = array();
        $params['evento_id'] = $evento_id;
        $params['usuario_id'] = $this->session->userdata('sesion_id_user');
        $params['codigo_inscripcion'] = str_repeat('0', 9 - strlen($params['usuario_id'])) . $params['usuario_id'];
        $success = false;
        if ($usuario_id)
            $success = $this->mdl_inscrito->save($params, null, false);
        if ($success) {
            echo json_encode((object)array('content' => '<div class="btn-group"><button class="btn btn-warning '.$large.' dropdown-toggle" data-toggle="dropdown">Asistir&eacute; <span class="caret"></span></button>
             <ul class="dropdown-menu">
             <li><a style="cursor: pointer" data-evento-id="'.$evento_id .'" class="cancelar_asistencia">Cancelar mi asistencia</a></li>
             </ul></div>', 'large' => ($large == 'btn-large')));
        } else {
            $this->session->set_userdata('last_index', $last_index);
            $this->session->set_flashdata('custom_error', 'Necesita iniciar sesi&oacute;n para realizar esta acci&oacute;n');
            echo json_encode((object)array('redir' => true));
        }
    }

    /* para imagenes */

    function eliminar_img($img) {
        $normal = base_path_foto_temp() . $img . ".jpg";
        @unlink($normal);
        $standard = base_path_foto_temp() . $img . "_standard.jpg";
        @unlink($standard);
        $thumb = base_path_foto_temp() . $img . "_thumb.jpg";
        @unlink($thumb);
    }

    function subir_imagen_ajax() {
        $this->load->library("image_moo");
        $this->load->helper("upload_files");
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);

        /* para la imagen */
        $nombre_foto = $_GET['qqfile'];
        $dot = strrpos($nombre_foto, ".", -1);
        $raw_name = substr($nombre_foto, 0, $dot);
        $extension = substr($nombre_foto, $dot);
        $results = array();
        $results['raw_name'] = $raw_name;
        $results['file_ext'] = '.jpg';
        //$results['file_path'] = realpath(".") . "\assets\uploads\\temp\\";
        $results['file_path'] = base_path_foto_temp();
        $path = $results['file_path'] . $raw_name . ".jpg";
        $results['full_path'] = $path;

        $target = fopen($path, "w");
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        /* Resizing to 1024 x 768 if its required */
        list($width, $height) = getimagesize($path);
        if ($width > 1024 || $height > 768) {
            $this->image_moo->load($path)->resize(1024, 768)->save($path, true);
        }
        $this->load->config("upload_images");
        $config = $this->config->item("config_evento");
        $results = redimensionar($results, "config_evento", $config);
        /* ------------------------------------- */
        echo json_encode((object) array('success' => true, 'filename' => $results['raw_name'], 'full_path' => base_url_foto_temp()));
    }

}

?>