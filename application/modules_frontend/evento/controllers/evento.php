<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Evento extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mcb_data/mdl_mcb_data');
		$this->mdl_mcb_data->set_session_data();
		$this->load->language('mcb', 'spanish');
		$this->load->helper(array('uri', 'mcb_icon', 'mcb_date'));
		$this->load->library(array('form_validation', 'redir'));
		// $this->form_validation->set_error_delimiters('<span class="help-inline">', '<span>');
		$this->form_validation->set_error_delimiters('<span>', '<span>');
		// $this->form_validation->set_error_delimiters('<span class="alert alert-error">', '</span>');

		$this->_post_handler();
		$this->load->model('mdl_evento');
		$this->load->model('local/mdl_local');
	}
	/**
	 * [recuperar_mis_id_eventos description]
	 * @return Array ids de mis eventos
	 */
	private function recuperar_mis_id_eventos() {
		$this->load->model('user/mdl_inscrito');
		$params = array();
		$params['select'] = 'evento_id';
		$params['where']['usuario_id'] = $this->session->userdata('sesion_id_user');
		$mis_eventos = $this->mdl_inscrito->get($params);
		$id_eventos = array();
		foreach ($mis_eventos as $mi_evento) {
			$id_eventos[] = $mi_evento->evento_id;
		}
		return $id_eventos;
	}

	public function index() {
		$this->filtro();
	}

	public function validate_new_event() {
		$this->form_validation->set_rules('nombre', '"T&iacute;tulo del evento"', 'required|trim|xss_clean');
		$this->form_validation->set_rules('categoria_id', '"Categoria"', 'required|trim|xss_clean');
		$this->form_validation->set_rules('descripcion', '"Detalles del evento"', 'required|trim|xss_clean');
		$this->form_validation->set_rules('organizador', '"Organizador"', 'required|trim|xss_clean');
		$this->form_validation->set_rules('detalle_organizador', '"Detalle organizador"', 'trim|xss_clean');
		$this->form_validation->set_rules('fecha_inicio', '"Fecha inicio del evento"', 'required|trim|xss_clean|callback__es_fecha');
		$this->form_validation->set_rules('fecha_fin', '"Fecha fin del evento"', 'required|trim|xss_clean|callback__es_fecha|callback__es_fecha_ordenada');
		$this->form_validation->set_rules('pais_id', '"Pais"', 'required|trim|xss_clean');
		$this->form_validation->set_rules('ciudad_id', '"Ciudad"', 'required|trim|xss_clean');
		$this->form_validation->set_rules('lugar', '"Nombre del local"', 'required|trim|xss_clean');
		$this->form_validation->set_rules('ubicacion', '"Ubicacion/Direccion"', 'required|trim|xss_clean');
		$this->form_validation->set_rules('img', '"debe subir una imagen"', 'trim|xss_clean');
		$this->form_validation->set_rules('publico', '"P&uacute;blico"', 'required|trim|xss_clean');
		$this->form_validation->set_rules('detalle_costo', '"Detalle costo"', 'trim|xss_clean');

		$this->form_validation->set_rules('presencial', '"Presencial"', 'required|trim|xss_clean');

		$this->form_validation->set_rules('referencia', 'referencia', 'trim|xss_clean');
		$this->form_validation->set_rules('latitud', 'latitud', 'trim|xss_clean');
		$this->form_validation->set_rules('longitud', 'longitud', 'trim|xss_clean');

		$this->form_validation->set_rules('dirigido_a', 'dirigido a:', 'trim|xss_clean');
		$this->form_validation->set_rules('capacidad', 'capacidad', 'trim|xss_clean');

		$this->form_validation->set_rules('ends_after', 'Termina despues de', 'trim|xss_clean');

		return $this->mdl_evento->validate($this);
	}

	function _es_fecha($fecha_evento) {

		$fecha = explode("/", $fecha_evento);
		if (count($fecha) != 3 || trim($fecha[0]) == "" ||
			trim($fecha[1]) == "" || trim($fecha[2]) == "" ||
			!checkdate($fecha[1], $fecha[0], $fecha[2])) {
			$this->form_validation->set_message('_es_fecha', 'Ingrese una fecha correcta: %s ');
			return false;
		}
		return true;
	}
	public function _es_fecha_ordenada($fecha_fin)
	{
		$hora = $this->input->post('hour_start', true);
		$minuto = $this->input->post('minute_start', true);
		$meridiano = $this->input->post('meridian_start', true);

		/* si es que es las 12 pm entonces tenemos que ponerlo a cero y luego sumamos
		 * si es 12 am se pondra a cero tmb pero se quedara 0 al final
		*/
		$hora = ($hora % 12);
		$hora += ($meridiano == "PM") ? 12 : 0;

		$hora_end = $this->input->post('hour_end', true);
		$minuto_end = $this->input->post('minute_end', true);
		$meridiano_end = $this->input->post('meridian_end', true);

		$hora_end = ($hora_end % 12);
		$hora_end += ($meridiano_end == "PM") ? 12 : 0;

		$ini_seconds = strtotime(standardize_date($this->input->post('fecha_inicio', true))) + ($minuto + $hora * 60) * 60;
		$fin_seconds = strtotime(standardize_date($fecha_fin)) + ($minuto_end + $hora_end * 60) * 60;
		if ($ini_seconds >= $fin_seconds) {
			$this->form_validation->set_message('_es_fecha_ordenada', 'La fecha inicial debe ser menor a la fecha final');
			return false;
		}
		return true;
	}

	function _verificar_tipo_repeticion($repeat_type) {
		if ($repeat_type == "") {
			$this->form_validation->set_message('_verificar_tipo_repeticion', 'seleccione un tipo de repeticion: %s');
			return false;
		}
		return true;
	}
	/**
	 * guardar en base de datos las repeticiones para el evento $evento_id
	 * @param  integer $evento_id
	 * @param  Array $arreglo_repeticiones
	 * @return boolean si se llevo a cabo el guardado en base de datos
	 */
	private function repeticiones($evento_id, $arreglo_repeticiones) {
		/* verificamos que exista el arreglo post (osea que no haya cancelado la repeticion) */
		if (!$arreglo_repeticiones)
			return false;
		$repite = $this->input->post('repeat_type', true);
		switch ($repite) {
			case 'diariamente':
				$num_dias = $arreglo_repeticiones["num_dias"];
				if (!$num_dias)
					break;
				$num_dias = (int) $num_dias;
				$hora = $arreglo_repeticiones['hour_start_daily'];
				$minuto = $arreglo_repeticiones['minute_start_daily'];
				$meridiano = $arreglo_repeticiones['meridian_start_daily'];

				/* si es que es las 12 pm entonces tenemos que ponerlo a cero y luego sumamos
				 * si es 12 am se pondra a cero tmb pero se quedara 0 al final
				*/

				$hora = ($hora % 12);
				$hora += ($meridiano == "PM") ? 12 : 0;

				$hora_end = $arreglo_repeticiones['hour_end_daily'];
				$minuto_end = $arreglo_repeticiones['minute_end_daily'];
				$meridiano_end = $arreglo_repeticiones['meridian_end_daily'];

				/* si es que es las 12 pm entonces tenemos que ponerlo a cero y luego sumamos
				 * si es 12 am se pondra a cero tmb pero se quedara 0 al final
				*/

				$hora_end = ($hora_end % 12);
				$hora_end += ($meridiano_end == "PM") ? 12 : 0;

				/* para el primer registro de fecha inicio */
				$fecha_inicio = strtotime(standardize_date($arreglo_repeticiones["repeat_start"])) + ($hora * 60 + $minuto) * 60;
				$fecha_fin = strtotime(standardize_date($arreglo_repeticiones["repeat_end"])) + ($hora_end * 60 + $minuto_end) * 60;

				$params = array(
						'evento_id' => $evento_id,
						'repeat_start' => $fecha_inicio,
						'repeat_end' => $fecha_fin,
						'repeat_interval' => $num_dias * 24 * 60 * 60,
						'repeat_type' => $repite
				);
				$this->mdl_evento_repeticion->save($params, '', false);
				break;
			case 'semanalmente':
				$num_semanas = $arreglo_repeticiones["num_semanas"];
				$dias_start = $arreglo_repeticiones["dias"];
				if (!$num_semanas)
					break;
				$num_semanas = (int) $num_semanas;
				$hora = $arreglo_repeticiones['hour_start_weekly'];
				$minuto = $arreglo_repeticiones['minute_start_weekly'];
				$meridiano = $arreglo_repeticiones['meridian_start_weekly'];

				/* si es que es las 12 pm entonces tenemos que ponerlo a cero y luego sumamos
				 * si es 12 am se pondra a cero tmb pero se quedara 0 al final
				*/

				$hora = ($hora % 12);
				$hora += ($meridiano == "PM") ? 12 : 0;

				$hora_end = $arreglo_repeticiones['hour_end_weekly'];
				$minuto_end = $arreglo_repeticiones['minute_end_weekly'];
				$meridiano_end = $arreglo_repeticiones['meridian_end_weekly'];

				/* si es que es las 12 pm entonces tenemos que ponerlo a cero y luego sumamos
				 * si es 12 am se pondra a cero tmb pero se quedara 0 al final
				*/

				$hora_end = ($hora_end % 12);
				$hora_end += ($meridiano_end == "PM") ? 12 : 0;

				/* para el primer registro de fecha inicio */
				$fecha_inicio = strtotime(standardize_date($arreglo_repeticiones["repeat_start"]));
				$fecha_fin = strtotime(standardize_date($arreglo_repeticiones["repeat_end"])) + ($hora_end * 60 + $minuto_end) * 60;
				foreach ($dias_start as $dia) {
					$params = array(
							'evento_id' => $evento_id,
							'repeat_start' => strtotime($dia . " this week " . date("Y-m-d", $fecha_inicio)) + ($hora * 60 + $minuto) * 60,
							'repeat_end' => $fecha_fin,
							'repeat_interval' => $num_semanas * 7 * 24 * 60 * 60,
							'repeat_type' => $repite
					);
					$this->mdl_evento_repeticion->save($params, '', false);
				}
				break;

			default:
				return false;
				break;
		}
		return true;
	}

	private function subir_imagen_evento($evento_id = null) {
		/* verificamos que hay algo para subir */
		$this->load->helper('upload_files');
		$results = subir_imagen("config_evento", "filename", "", TRUE, "");
		//si subio la foto modificamos al user
		if (isset($results['isuploaded']) == "exito") {
			if ($evento_id) {
				$user_img = $this->mdl_evento->get(array('select' => 'img',
						'where' => array('evento_id' => $evento_id),
						'return_row' => true
				));
			}
			if (isset($user_img) && $user_img !== NULL && $user_img->img != '') {
				$dot_pos = strpos($user_img->img, ".");
				/* tmb borramos el thumb y el standard */
				$ruta_thumb = substr($user_img->img, 0, $dot_pos) . '_thumb' . substr($user_img->img, $dot_pos);
				borrar_imagen($results['file_path'], $ruta_thumb);
				$ruta_standard = substr($user_img->img, 0, $dot_pos) . '_standard' . substr($user_img->img, $dot_pos);
				borrar_imagen($results['file_path'], $ruta_standard);
			}
			$params['img'] = $results['raw_name'];
			return $params['img'];
		}
		$this->session->set_flashdata("custom_error", $results['upload_picture_error']);
		return false;
	}

	public function agregar() {
		log_message('error', print_r($this->input->post(), true));
		$evento_id = uri_assoc('evento_id');
		$evento_repite = $this->input->post('select_repite', true) == 'evento_repite';
		if ($evento_repite) {
			$repeat_type = $this->input->post('repeat_type', true);
			$this->form_validation->set_rules('repeat_type', "tipo de repeticion", "trim|xss_clean|callback__verificar_tipo_repeticion");
			switch ($repeat_type) {
				case "diariamente":
					$this->form_validation->set_rules('arreglo_repeticiones[num_dias]', '"numero de dias"', "trim|xss_clean|integer");
					$this->form_validation->set_rules('arreglo_repeticiones[repeat_start]', '"fecha inicio de repeticion"', "required|trim|xss_clean|__es_fecha");
					$this->form_validation->set_rules('arreglo_repeticiones[repeat_end]', '"fecha fin de repeticion"', "required|trim|xss_clean|__es_fecha");

					$this->form_validation->set_rules('arreglo_repeticiones[hour_start_daily]', '"hora de inicio repeticion"', "required|trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[minute_start_daily]', '"minuto de inicio de repeticion"', "required|trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[meridian_start_daily]', '"meridiano de inicio de repeticion"', "required|trim|xss_clean");

					$this->form_validation->set_rules('arreglo_repeticiones[hour_end_daily]', '"hora de fin de repeticion"', "required|trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[minute_end_daily]', '"minuto de fin de repeticion"', "required|trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[meridian_end_daily]', '"meridiano de fin de repeticion"', "required|trim|xss_clean");
					break;
				case "semanalmente":
					$this->form_validation->set_rules('arreglo_repeticiones[dias][]', '"dias a la semana que repetiran"', "required|trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[num_semanas]', '"numero de dias"', "trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[repeat_start]', '"fecha inicio de repeticion"', "required|trim|xss_clean|__es_fecha");
					$this->form_validation->set_rules('arreglo_repeticiones[repeat_end]', '"fecha fin de repeticion"', "required|trim|xss_clean|__es_fecha");

					$this->form_validation->set_rules('arreglo_repeticiones[hour_start_weekly]', '"hora de inicio repeticion"', "required|trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[minute_start_weekly]', '"minuto de inicio de repeticion"', "required|trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[meridian_start_weekly]', '"meridiano de inicio de repeticion"', "required|trim|xss_clean");

					$this->form_validation->set_rules('arreglo_repeticiones[hour_end_weekly]', '"hora de fin de repeticion"', "required|trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[minute_end_weekly]', '"minuto de fin de repeticion"', "required|trim|xss_clean");
					$this->form_validation->set_rules('arreglo_repeticiones[meridian_end_weekly]', '"meridiano de fin de repeticion"', "required|trim|xss_clean");
					break;
				default:
					break;
			}
		}
		if ($this->validate_new_event()) {
			/* subiremos imagen via ajax, estas lineas x el momento se comentan */
				//            $res = $this->subir_imagen_evento($evento_id);
				//            if ($res)
				//                $params['img'] = $res;
				$params = $this->mdl_evento->db_array();
			if ($evento_id != null) {
				$params_evento = array();
				$params_evento['select'] = 'evento_id, img';
				$params_evento['return_row'] = true;
				$params_evento['where']['evento_id'] = $evento_id;
				$evento = $this->mdl_evento->get($params_evento);
				if (!empty($evento) && $evento->img != $params['img']) {
					@unlink(base_path_foto_evento() . $evento->img . '_standard.jpg');
					@unlink(base_path_foto_evento() . $evento->img . '_thumb.jpg');
					@unlink(base_path_foto_evento() . $evento->img . '.jpg');
				}
			}
			if ($params['img'] != '') {
				if (file_exists(base_path_foto_temp() . $params['img'] . '_standard.jpg'))
					if (copy(base_path_foto_temp() . $params['img'] . '_standard.jpg', base_path_foto_evento() . $params['img'] . '_standard.jpg')) {
						@unlink(base_path_foto_temp() . $params['img'] . '_standard.jpg');
					}
				if (file_exists(base_path_foto_temp() . $params['img'] . '_thumb.jpg'))
					if (copy(base_path_foto_temp() . $params['img'] . '_thumb.jpg', base_path_foto_evento() . $params['img'] . '_thumb.jpg')) {
						@unlink(base_path_foto_temp() . $params['img'] . '_thumb.jpg');
					}
				if (file_exists(base_path_foto_temp() . $params['img'] . '.jpg'))
					if (copy(base_path_foto_temp() . $params['img'] . '.jpg', base_path_foto_evento() . $params['img'] . '.jpg')) {
						@unlink(base_path_foto_temp() . $params['img'] . '.jpg');
					}
			}
			if ($params['dirigido_a'] == "") {
				$params['dirigido_a'] = "p&uacute;blico en general";
			}
			if ($this->input->post("condicion", true) == 'gratuito')
				$params['detalle_costo'] = 'gratuito';
			if ($this->input->post("combo_tiene_itinerario", true) == '')
				$params['programa'] = '';
			/* si no tiene detalle de organizador limpiamos el campo de detalle del organizador  */
			if (!$this->input->post("has_detail_organizer", true)) {
				$params['detalle_organizador'] = '';
			}
			if (!$this->input->post("agregar_ubicacion_referencial", true)) {
				$params['referencia'] = '';
				$params['latitud'] = '';
				$params['longitud'] = '';
			}
			$hora = $this->input->post('hour_start', true);
			$minuto = $this->input->post('minute_start', true);
			$meridiano = $this->input->post('meridian_start', true);

			/* si es que es las 12 pm entonces tenemos que ponerlo a cero y luego sumamos
			 * si es 12 am se pondra a cero tmb pero se quedara 0 al final
			*/
			$hora = ($hora % 12);
			$hora += ($meridiano == "PM") ? 12 : 0;

			$hora_end = $this->input->post('hour_end', true);
			$minuto_end = $this->input->post('minute_end', true);
			$meridiano_end = $this->input->post('meridian_end', true);

			$hora_end = ($hora_end % 12);
			$hora_end += ($meridiano_end == "PM") ? 12 : 0;

			$params['fecha_inicio'] = strtotime(standardize_date($params['fecha_inicio'])) + ($minuto + $hora * 60) * 60;
			$params['fecha_fin'] = strtotime(standardize_date($params['fecha_fin'])) + ($minuto_end + $hora_end * 60) * 60;

			if ($evento_id === NULL)
				$params['url'] = $this->milib->verificar_url_repetida(a_url_amigable($params['nombre']));

			$params['usuario_id'] = $this->session->userdata('sesion_id_user');
			$params['es_repetido'] = ($this->input->post("select_repite", true) == "evento_repite") ? true : false;

			/* para que nos devuelva el id insertado pasamos true al ultimo parametro */
			$this->load->model("mdl_evento_repeticion");
			if ($evento_id != null) {
				$this->mdl_evento_repeticion->delete(array("evento_id" => $evento_id), false);
			}
			$id_inserted = $this->mdl_evento->save($params, $evento_id, false, true);
			/* si no pudo insertar */
			if ($id_inserted === false) {
				$this->session->set_flashdata('custom_error', 'ocurri&oacute; un error registrando el evento, int&eacute;ntelo de nuevo');
				redirect('user');
			}
			else {
				if (!$evento_id)
					$this->session->set_flashdata('custom_success', '¡El evento ha sido creado!');
				else
					$this->session->set_flashdata('custom_success', '¡El evento ha sido actualizado!');
			}
			//guardando el evento en la tabla envios
			modules::run('suscrito/guardar_envios_suscripcion', (isset($evento_id)) ? $evento_id:$id_inserted);

			if (!$evento_id) {
				/* esto es para inscribir al creador a su evento, solo cuando lo creó no cuando lo esta actualizando */
				$this->load->model('user/mdl_inscrito');
				$params = array();
				$params['evento_id'] = $id_inserted;
				$params['usuario_id'] = $this->session->userdata('sesion_id_user');
				$params['codigo_inscripcion'] = str_repeat('0', 9 - strlen($params['usuario_id'])) . $params['usuario_id'];
				$this->mdl_inscrito->save($params, null, false);
			}
			$evento_id = ($evento_id) ? $evento_id : $id_inserted;
			if ($this->input->post("select_repite", true) == "evento_repite") {
				$exito = $this->repeticiones($evento_id, $this->input->post("arreglo_repeticiones", true));
				if (!$exito)
					$this->mdl_evento->save(array('es_repetido' => false), $evento_id, false);
			}
			redirect('user');
		} else {
			$actualizacion = true;
			if (!$_POST && $evento_id) {
				/* previamente verificamos que el evento que quiere modificar corresponde a uno creado por el mismo */
				$evento = $this->mdl_evento->get(array('where' => array('evento_id' => $evento_id), 'return_row' => true));
				if (count($evento) == 0 || $evento->usuario_id != $this->session->userdata('sesion_id_user')) {
					$this->session->set_flashdata('custom_error', 'Usted no tiene permiso para realizar esa acci&oacute;n');
					redirect("eventos");
				}
				$this->mdl_evento->prep_validation($evento_id);
			} else {
				/* significa que no paso la validacion ni es actualizacion tonces tenemos que corregir
				 * el problema con la forma de manipulacion de fechas */
				$hora = $this->input->post('hour_start', true);
				$minuto = $this->input->post('minute_start', true);
				$meridiano = $this->input->post('meridian_start', true);
				/* si es que es las 12 pm entonces tenemos que ponerlo a cero y luego sumamos
				 * si es 12 am se pondra a cero tmb pero se quedara 0 al final
				*/
				$hora = ($hora % 12);
				$hora += ($meridiano == "PM") ? 12 : 0;

				$hora_end = $this->input->post('hour_end', true);
				$minuto_end = $this->input->post('minute_end', true);
				$meridiano_end = $this->input->post('meridian_end', true);

				$hora_end = ($hora_end % 12);
				$hora_end += ($meridiano_end == "PM") ? 12 : 0;

				$this->mdl_evento->set_form_value('fecha_inicio', strtotime(standardize_date($this->mdl_evento->form_value('fecha_inicio'))) + ($minuto + $hora * 60) * 60);
				$this->mdl_evento->set_form_value('fecha_fin', strtotime(standardize_date($this->mdl_evento->form_value('fecha_fin'))) + ($minuto_end + $hora_end * 60) * 60);
				$actualizacion = false;
			}
			/* load template */
			$this->view_form($actualizacion, $evento_id);
		}
	}

	private function view_form($actualizacion = false, $evento_id = null) {
		$format = $this->mdl_mcb_data->setting("default_date_format_picker");
		$mask = $this->mdl_mcb_data->setting("default_date_format_mask");
		$base_url = base_url();
		$javascript_inline = '
			$(".nav li").click(function(e) {
				$(".nav li").removeClass("active");
				var $this = $(this);
				if (!$this.hasClass("active")) {
					$this.addClass("active");
				}
			});

			$("#datepicker").datepicker({
			minDate: 0,
			changeYear: true,
			changeMonth: true,
			dateFormat: "' . $format . '"
			});

			$(".datepicker").datepicker({
			dateFormat: "' . $format . '",
			onSelect : function (dateText, inst) {
				if (inst.id == "fecha_inicio") {
					$("#fecha_fin").datepicker("option", "minDate", $("#fecha_inicio").datepicker("getDate"));

					var enddate = $("#fecha_inicio").datepicker("getDate");
				    enddate.setDate(enddate.getDate());
				    $("#fecha_fin").datepicker("setDate", enddate);
				}
				if (inst.id == "id_daily-repeat_start") {
					$("#id_daily-repeat_end").datepicker("option",
					"minDate",
					$("#id_daily-repeat_start").datepicker("getDate"));
				}

				if (inst.id == "id_weekly-repeat_start") {
					$("#id_weekly-repeat_end").datepicker("option",
					"minDate",
					$("#id_weekly-repeat_start").datepicker("getDate"));
				}
			}
			});

			$("#datepicker").mask("' . $mask . '");
			$(".datepicker").mask("' . $mask . '");
			CKEDITOR.replace("detalle_organizador", {toolbar:"Miconfig"});
			CKEDITOR.replace("descripcion", {toolbar:"Miconfig"});
		';
		$this->template->set_template('full-width');
		//ckedit
		$this->template->add_js(base_js() . 'ckeditor/ckeditor.js');
		//maps
		$this->template->add_js("https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places", "import", false);
		$this->template->add_js($javascript_inline, 'embed');
		/* para setear el mapa inicial a la parte de su pais mas cercano */
		$this->template->add_js(base_js() . 'google_maps.js');
		if (!$this->session->userdata('logged_in'))
			$this->template->add_js(base_js() . "user/no_logueado.js");
		$this->template->add_js(base_js() . 'evento/view_form.js');
		$css = '  #map_canvas {
				height: 400px;
				width: 600px;
				margin-top: 0.6em;
		}';
		$this->template->add_css($css, 'embed', false);

		$this->load->model(array('pais/mdl_pais', 'ciudad/mdl_ciudad'));
		$data = array();
		$data['pais_id'] = $this->mdl_evento->form_value('pais_id');
		$data['ciudad_id'] = $this->mdl_evento->form_value('ciudad_id');
		$data['paises'] = $this->mdl_pais->get(array('order_by' => 'nombre', 'select' => 'pais_id, nombre'));
		$data['ciudades'] = $this->mdl_ciudad->get(array('order_by' => 'nombre', 'select' => 'ciudad_id, nombre', 'where' => array('pais_id' => $data['pais_id'])));
		$data['categorias'] = $this->milib->categorias_ordenadas("categoria_id, nombre");
		/* si el evento es repetido */
		if ($actualizacion && $this->mdl_evento->form_value('es_repetido')) {
			$this->load->model('mdl_evento_repeticion');
			$params = array();
			$params['where']['evento_id'] = $this->mdl_evento->form_value('evento_id');
			$data['repeticiones'] = $this->mdl_evento_repeticion->get($params);
			$data['repeat_type'] = $data['repeticiones'][0]->repeat_type;
			$data['evento_repetido'] = true;
		} else {
			if ($this->input->post("arreglo_repeticiones")) {
				$data['arreglo_repeticiones'] = $this->input->post('arreglo_repeticiones', true);
				$data['repeat_type'] = $this->input->post('repeat_type', true);
				$data['evento_repetido'] = true;
			}
			else
				$data['evento_repetido'] = false;
		}
		$data['actualizacion'] = $actualizacion;
		$data['evento_id'] = $evento_id;
		$this->template->add_js(base_url() . "assets/image_crud/js/fileuploader.js", "import", false, 'footer', true);
		$this->template->add_css(base_url() . "assets/image_crud/css/fileuploader.css", "link", false, true);
		$upload_url = site_url("evento/evento_async/subir_imagen_ajax");
		$javascript_inline = "
			function foto_evento(filename, full_path) {
				$('#img').val(filename);
				$('#foto-evento').attr('src', full_path + filename + '_standard.jpg');
				if ($('.qq-upload-list').children('li').length == 2) {
				$('.qq-upload-list li:first-child').remove();
				}
				$('.qq-upload-list li:first-child').append('(cargar otra imagen si quiere reemplazar)');
				//console.log('reloading imagen de evento: %s_thumb.jpg y full_path: %s', filename, full_path + filename + '_thumb.jpg');
			}
			function createUploader() {
				var uploader = new qq.FileUploader({
				element: document.getElementById('file-uploader'),
				template: '<div class=\"qq-uploader\">' +
				'<div class=\"qq-upload-drop-area\"><span>soltar archivos aqui para subir</span></div>' +
				'<div class=\"qq-upload-button\">elegir foto</div>' +
				'<ul class=\"qq-upload-list\"></ul>' +
				'</div>',
				fileTemplate: '<li>' +
				'<span class=\"qq-upload-file\"></span>' +
				'<span class=\"qq-upload-spinner\"></span>' +
				'<span class=\"qq-upload-size\"></span>' +
				'<a class=\"qq-upload-cancel\" href=\"#\">subida cancelada</a>' +
				'<span class=\"qq-upload-failed-text\">error subiendo, vuelva a intentarlo</span>' +
				'</li>',
				action: '$upload_url',
				debug: true,
				onComplete: function(id, fileName, responseJSON){
				/*console.log(id);
				console.log(fileName);
				console.log(responseJSON);*/
				if ($('#img').val() != '') {
					$.ajax({
						type: 'post',
						url: '".$base_url."evento/evento_async/eliminar_img/' + $('#img').val()
					});
				}
				foto_evento(responseJSON['filename'], responseJSON['full_path']);
			}
			});
			}
			createUploader();
			$('a[rel=popover]').popover().click(function() {
			$(this).popover('show');
			return false;
			}).mouseleave(function(){
			$(this).popover('hide');
			});
		";
		$this->template->add_js($javascript_inline, "embed");
		$this->template->write('header_title', 'Administrar Evento');
		$this->template->write('title', 'Publicar Evento');
		$this->template->write_view('content', 'form', $data);
		$this->template->render();
	}

	public function categoria() {
		$url_categoria = $this->uri->segment(2);
		$this->load->model('categoria/mdl_categoria');
		$params = array();
		$params['select'] = "categoria_id";
		$params['where'] = "url = '" . $url_categoria . "'";
		$params['return_row'] = true;
		$categoria = $this->mdl_categoria->get($params);

		$params = array();
		$params['where']['categoria_id'] = $categoria->categoria_id;
		$params['where']['fecha_inicio >='] = time();
		$data = array();
		$data['eventos'] = $this->mdl_evento->get($params);

		$data['categorias'] = $this->milib->categorias();
		$data['usuario_id'] = $this->session->userdata('sesion_id_user') ? $this->session->userdata('sesion_id_user') : 0;
		$data['logged_in'] = $this->session->userdata('logged_in');

		//$this->template->write('header_title', $url_categoria);
		$this->template->write('title', 'Descubre m&aacute;s eventos');
		$this->template->write_view('system_messages', 'dashboard/system_messages');
		$this->template->write_view('content', 'index', $data);
		$this->template->render();
	}

	public function pagina_inicio($error404 = null) {
		$params = array();
		/* recuperamos categorias */
		$this->load->model('categoria/mdl_categoria');
		$this->load->model("ciudad/mdl_ciudad");
		$this->load->model("pais/mdl_pais");
		$data['cat_with_events'] = $this->milib->categorias_con_eventos();
		$params['select'] = '*';
		$params['where']['nombre !='] = 'otro';
		$params['order_by'] = 'nombre';
		$data['categorias'] = $this->mdl_categoria->get($params);
		$data['categorias_imagenes'] = $data['categorias'];
		$eventos_populares = $this->mdl_evento->evento_mas_popular_x_categoria();
		$evento_popular_x_categoria = array();
		$evento_repetido = array();
		foreach ($eventos_populares as $value) {
			$evento_popular_x_categoria[$value->categoria_id] = $value->evento_id;
			$evento_repetido[$value->categoria_id] = $value->es_repetido == '1';
		}

		/* recuperamos eventos x categoria */
		$i = 0;
		$proximos_eventos = $this->mdl_evento->proximos_eventos(array('where' => array('status' => 1)));
		// var_dump($proximos_eventos);exit;
		//var_dump($proximos_eventos); exit();
		foreach ($data['categorias'] as $categoria) {
			/*$params['select'] = "*";
			$params['where']['categoria_id'] = $categoria->categoria_id;
			$params['order_by'] = 'fecha_inicio';
			$params['limit'] = 4;*/
			// $data['categorias'][$i]->eventos = $this->mdl_evento->get($params);
			$data['categorias'][$i]->eventos = array();
			$cont = 0;
			foreach ($proximos_eventos as $evento_proximo) {
				if ($evento_proximo->categoria_id == $categoria->categoria_id) {
					$data['categorias'][$i]->eventos[] = $evento_proximo;
					$cont++;
				}
				//si ya tenemos los 4 más proximos ya no mostramos más.
				if ($cont == 4) break;
			}
			if (isset($evento_popular_x_categoria[$categoria->categoria_id])) :
				$repetido = false;
				$id_evento_cur_cat = $evento_popular_x_categoria[$categoria->categoria_id];
				/* verificamos que el evento popular no haya salido previamente en los resultado para mostrar
					de esa manera evitamos eliminar el ultimo y agregar el mas popular */
				for ($k = 0, $cant = count($data['categorias'][$i]->eventos); $k < $cant && !$repetido; $k++) {
					if ($data['categorias'][$i]->eventos[$k]->evento_id == $id_evento_cur_cat) {
						$repetido = true;
					}
				}
				if (!$repetido) {
					$evento_popular = $this->mdl_evento->get_evento($id_evento_cur_cat, $evento_repetido[$categoria->categoria_id]);
					/* borramos el ultimo y luego agregamos el mas popular */
					if (!empty($evento_popular)) {
						array_pop($data['categorias'][$i]->eventos);
						$data['categorias'][$i]->eventos[] = $evento_popular;
					}
				}
			endif;
			/* retorna arreglo de objectos*/
			/* si no hay eventos eliminamos la categoria del array */
			if (count($data['categorias'][$i]->eventos) == 0) {
				unset($data['categorias'][$i]);
			}
			else {
				$j=0;
				foreach ($data['categorias'][$i]->eventos as $evento) {
					/* recuperamos paises y ciudades */
					$params_ciudad = array();
					$params_ciudad['select'] = 'ciudad_id, pais_id, nombre';
					$params_ciudad['where']['ciudad_id'] = $data['categorias'][$i]->eventos[$j]->ciudad_id;
					$params_ciudad['return_row'] = true;
					$ciudad = $this->mdl_ciudad->get($params_ciudad);
					$data['categorias'][$i]->eventos[$j]->ciudad = $ciudad->nombre;

					$params_pais = array();
					$params_pais['select'] = "nombre";
					$params_pais['where']['pais_id'] = $ciudad->pais_id;
					$params_pais['return_row'] = true;
					$pais = $this->mdl_pais->get($params_pais);
					$data['categorias'][$i]->eventos[$j]->pais = $pais->nombre;
					$j++;
				}
			}
			$i++;
		}

		/* info para determinar el boton participar */
		$data['logged_in'] = $this->session->userdata('logged_in');
		$data['usuario_id'] = $this->session->userdata('sesion_id_user') ? $this->session->userdata('sesion_id_user') : 0;
		$data['mis_eventos'] = $this->recuperar_mis_id_eventos();

		$this->template->set_template('default');
		if (!$this->session->userdata('logged_in'))
			$this->template->add_js(base_js() . "user/no_logueado.js");
		if ($error404) {
			$this->template->write('error404', '<div class="alert alert-error alert-white"><h4>Error 404: p&aacute;gina no encontrada</h4></div>');
		}
		$javascript_inline = '
				$(document).ready(function() {
				$(".flexslider").flexslider({
						animation: "slide",
						animationLoop: true,
						slideshowSpeed: 5000,
						controlNav: true,
						directionNav: true,
						touch: true,
						itemWidth: 235,
						itemMargin: 0,
						minItems: 2,
				});
				});
				';
		$this->template->add_js($javascript_inline, 'embed');
		$this->template->add_js(base_js() . "jquery.flexslider-min.js");
		$this->template->add_css(base_css() . 'flexslider.css');
		$this->template->add_js(base_js() . "evento/actions_user_events.js");

		//seo
		$data['seo_keyword'] = "eventos, publicidad, cine, publicar, publicaciones";
		$data['seo_descripcion'] = "Publicar eventos en internet, portal de eventos - Acercate-eventos.com";

		$this->template->write('header_title', "Eventos, conciertos, conferencias, ferias");
		$this->template->write('title', '¿Qu&eacute; hay para visitar hoy?');
		$this->template->write_view('content', 'evento/pagina_inicio_view', $data);
		$this->template->render();
	}

	public function detalle() {
		$url_evento = $this->uri->segment(2);
		$params = array();
		$params['select']  = "evento_id, es_repetido, nro_visitas";
		$params['where']['url'] = $url_evento;
		$params['return_row'] = true;
		$data = array();
		$data['evento'] = $this->mdl_evento->get($params);
		if (empty($data['evento'])) {
			redirect("error");
		}

		/* controlar cuantas visitas esta teniendo nuestra pagina especifica (detalle para este evento) */
		$nro_visitas = $data['evento']->nro_visitas;
		$this->mdl_evento->save(array('nro_visitas' => $nro_visitas + 1), $data['evento']->evento_id, false, false);

		$data['evento'] = $this->mdl_evento->get_evento($data['evento']->evento_id, $data['evento']->es_repetido);
		if ($data['evento']->es_repetido == '1') {
			$this->load->model('evento/mdl_evento_repeticion');
			$params = array();
			$params['where']['evento_id'] = $data['evento']->evento_id;
			$params['return_row'] = true;
			$data['events_meta'] = $this->mdl_evento_repeticion->get($params);
		}
		/* recuperamos paises y ciudades */
		$this->load->model("ciudad/mdl_ciudad");
		$params = array();
		$params['select'] = 'ciudad_id, pais_id, nombre';
		$params['where']['ciudad_id'] = $data['evento']->ciudad_id;
		$params['return_row'] = true;
		$ciudad = $this->mdl_ciudad->get($params);
		$data['evento']->ciudad = $ciudad->nombre;
		$this->load->model("pais/mdl_pais");
		$params = array();
		$params['select'] = "nombre";
		$params['where']['pais_id'] = $ciudad->pais_id;
		$params['return_row'] = true;
		$pais = $this->mdl_pais->get($params);
		$data['evento']->pais = $pais->nombre;

		/* recuperamos su categoria */
		$this->load->model("categoria/mdl_categoria");
		$params = array();
		$params['select'] = "nombre, url, img";
		$params['where']['categoria_id'] = $data['evento']->categoria_id;
		$params['return_row'] = true;
		$categoria = $this->mdl_categoria->get($params);
		$data['evento']->categoria = $categoria->nombre;
		$data['categoria'] = new stdClass;
		$data['categoria']->url = $categoria->url;
		$data['categoria']->img = $categoria->img;

		/* recuperamos eventos relacionados */
		$params['select'] = "*";
		$params['where']['categoria_id'] = $data['evento']->categoria_id;
		$params['where']['evento_id != '] = $data['evento']->evento_id;
		$params['where']['status'] = 1;
		$params['paginate'] = true;
		$params['page'] = uri_page();
		$params['limit'] = $this->config->item('results_per_page_detail');
		// var_dump($params);exit;
		$data['eventos_relacionados'] = $this->mdl_evento->proximos_eventos($params);
		/*var_dump($data['eventos_relacionados']);
		var_dump($this->mdl_evento->page_links);exit;*/
		$i=0;
		foreach ($data['eventos_relacionados'] as $evento) {
			/* recuperamos paises y ciudades */
			$params_ciudad = array();
			$params_ciudad['select'] = 'ciudad_id, pais_id, nombre';
			$params_ciudad['where']['ciudad_id'] = $evento->ciudad_id;
			$params_ciudad['return_row'] = true;
			$ciudad = $this->mdl_ciudad->get($params_ciudad);
			$data['eventos_relacionados'][$i]->ciudad = $ciudad->nombre;

			$params_pais = array();
			$params_pais['select'] = "nombre";
			$params_pais['where']['pais_id'] = $ciudad->pais_id;
			$params_pais['return_row'] = true;
			$pais = $this->mdl_pais->get($params_pais);
			$data['eventos_relacionados'][$i]->pais = $pais->nombre;
			$i++;
		}

		/* ----------------------------------------------------------------- */
		/* si evento es repetido, determinamos las fechas programadas */
		/*
		 if ($data['evento']->es_repetido) {

		var_dump($this->mdl_evento->retrieve_repeated_events(strtotime(date('m/d/Y')), " AND ev.evento_id = " . $data['evento']->evento_id));
		exit;
		}
		*/
		/* ----------------------------------------------------------------- */


		/* info para determinar el boton participar */
		$data['categorias'] = $this->milib->categorias();
		$data['logged_in'] = $this->session->userdata('logged_in');
		$data['usuario_id'] = $this->session->userdata('sesion_id_user') ? $this->session->userdata('sesion_id_user') : 0;
		$data['mis_eventos'] = $this->recuperar_mis_id_eventos();

		$this->template->set_template('full-width');
		if ($data['evento']->latitud != '' && $data['evento']->longitud != '') {

			$this->template->add_js("https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places", "import", false);
			//$this->template->add_js("https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false", "import", false);
			$this->template->add_js(base_js() . "evento/actions_user_events.js");
			$js_mapas = "
					var times = 0;
					function initialize() {
						var centro = new google.maps.LatLng(" . $data['evento']->latitud . ", " . $data['evento']->longitud . ");
						var mapOptions = {
							scrollwheel: false,
							navigationControl: true,
							mapTypeControl: false,
							scaleControl: false,
							draggable: true,
							zoom: 18,
							center: centro,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};
						map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
						var marker = new google.maps.Marker({
							map: map
						});
						marker.setPosition(mapOptions.center);
					}
					//google.maps.event.addDomListener(window, 'load', initialize);
					$('#modal-mapa').on('shown', function() {
						if (times == 0) {
							initialize();
							times++;
						}
					});
							";
			$this->template->add_js($js_mapas, 'embed', false);
		}

		//seo
		$data['seo_keyword'] = str_replace(" ", ", ", $data['evento']->nombre).", eventos, publicidad, cine, publicar, publicaciones";
		$data['seo_descripcion'] = $data['evento']->nombre. ", publicar eventos en internet, portal de eventos - Acercate-eventos.com";


		if (!$this->session->userdata('logged_in'))
			$this->template->add_js(base_js() . "user/no_logueado.js");
		$this->template->add_js(base_js() . 'evento/actions_user_events.js');
		$this->template->write('header_title', $data['evento']->nombre);
		$this->template->write('title', $data['evento']->nombre);
		$this->template->write_view('content', 'detalle_evento', $data);
		$this->template->render();
	}

	public function _post_handler() {
		if ($this->input->post('btn_add'))
			redirect('evento/agregar');
		if ($this->input->post('btn_cancel'))
			redirect('user');
	}

	public function get_ciudades($pais_id = "") {
		$this->load->model('ciudad/mdl_ciudad');
		$params = array();
		$params['where']['pais_id'] = $pais_id;
		$params['select'] = "ciudad_id, nombre";
		$params['order_by'] = "nombre";
		$res = $this->mdl_ciudad->get($params);
		$res = (empty($res)) ? array() : $res;
		echo json_encode($res);
	}

	function desactivar_evento() {
		if ($this->milib->logueado()) {
			$evento_id = uri_assoc('evento_id');
			if ($evento_id){
				$params['status'] = 0;
				if ($this->mdl_evento->save($params, $evento_id)) {
					echo "
						<script>
						$('.ajaxify').ajaxify();
						</script>
					";
						echo '
						<a class="btn btn-info ajaxify" target="#status_update_' . $evento_id . '" href="' . base_url() . 'evento/activar_evento/evento_id/' . $evento_id . '">Activar</a>
					';
				}
				else {
					$this->session->set_userdata('last_index', $last_index);
					$this->session->set_flashdata('custom_error', 'Necesitas iniciar sesi&oacute;n para realizar la acci&oacute;n seleccionada.');
				}

			}
		}
		else {
			$this->session->set_userdata('last_index', $last_index);
			$this->session->set_flashdata('custom_error', 'Necesitas iniciar sesi&oacute;n para realizar la acci&oacute;n seleccionada.');
		}
	}

	function activar_evento() {
		if ($this->milib->logueado()) {
			$evento_id = uri_assoc('evento_id');
			if ($evento_id){
				$params['status'] = 1;
				if ($this->mdl_evento->save($params, $evento_id)) {
					echo "
						<script>
						$('.ajaxify').ajaxify();
						</script>
					";
					echo '
						<a class="btn btn-info ajaxify" target="#status_update_' . $evento_id . '" href="' . base_url() . 'evento/desactivar_evento/evento_id/' . $evento_id . '">Desactivar</a>
					';
				}
				else {
					$this->session->set_userdata('last_index', $last_index);
					$this->session->set_flashdata('custom_error', 'Necesitas iniciar sesi&oacute;n para realizar la acci&oacute;n seleccionada');
				}

			}
		}
		else {
			$this->session->set_userdata('last_index', $last_index);
			$this->session->set_flashdata('custom_error', 'Necesitas iniciar sesi&oacute;n para realizar la acci&oacute;n seleccionada');
		}
	}

	public function delete() {
		$evento_id = uri_assoc('evento_id');

		if ($evento_id) {
			$this->mdl_evento->delete(array('evento_id' => $evento_id));
		}

		redirect('eventos');
	}

	public function barra_horizontal_filtro($embebido = false) {
		$data = array();
		$data['pais_id'] = 0;
		$data['ciudad_id'] = 0;
		if (strpos($this->uri->ruri_string(), "evento/filtro") != FALSE) {
			$segments = $this->uri->segment_array();
			for ($i = 2; $i <= count($segments); $i++) {
				if (substr($segments[$i], 0, 1) == 'p')
					$data['pais_id'] = substr($segments[$i], 2);
				if (substr($segments[$i], 0, 1) == 'i')
					$data['ciudad_id'] = substr($segments[$i], 2);
			}
		}
		$this->load->model(array("pais/mdl_pais", "ciudad/mdl_ciudad"));
		//cargamos los datos para la categoria de filtro por paises y ciudades
		$data['paises'] = $this->mdl_pais->get(array('order_by' => 'nombre', 'select' => 'pais_id, nombre'));
		$data['ciudades'] = $this->mdl_ciudad->get(array('order_by' => 'nombre', 'select' => 'ciudad_id, nombre', 'where' => array('pais_id' => $data['pais_id'])));
		if ($embebido)
			$this->load->view("navs/barra_navegacion", $data);
		else {
			$this->template->set_template("template-barra-navegacion");
			$this->template->add_js(base_js() . "evento/script_barra_horizontal.js");
			//            $this->template->add_js("action_user_eventos");
			if (!$this->session->userdata('logged_in'))
				$this->template->add_js(base_js() . "user/no_logueado.js");
			$this->template->write("title", 'test de la barra horizontal de navegacion');
			$this->template->write_view('barra_navegacion', 'navs/barra_navegacion', $data);
			$this->template->write('calendario', Modules::run('evento/calendariomes', true));
			$this->template->write('content', 'contenido de la vista de prueba');
			$this->template->render();
		}
	}

	public function mapa() {
		$this->load->view("navs/mapa");
		$content = $this->load->view('navs/mapa', null, true);
		var_dump($content);exit;
	}

	public function calendario() {

		//obteniendo eventos para mostrar en el calendario
		$paramse = array();
		$paramse['select'] = 'evento_id, categoria_id, nombre, lugar, url, fecha_inicio, fecha_fin, es_repetido';
		/* asegurarnos que no son eventos desactivados */
		$paramse['where']['status'] = 1;
		//where mes now limit
		$eventos = $this->mdl_evento->get($paramse);
		$data['filas'] = $eventos;

		$this->load->model("mdl_evento_repeticion");
		$this->load->model('categoria/mdl_categoria');
		//listando eventos
		$eventos_js_html = "";
		$repetidos_js_html = "";
		foreach ($eventos as $evento) {
			$params = array();
			$params['select'] = 'color';
			$params['where']['categoria_id'] = $evento->categoria_id;
			$params['return_row'] = true;
			$categoria = $this->mdl_categoria->get($params);
			$bgcolor = $categoria->color;
			if ($evento->es_repetido == 1) {
				//obteniendo los eventos repetidos
				$paramsr['select'] = 'evento_id, repeat_start, repeat_end, repeat_interval';
				$paramsr['where']['evento_id'] = $evento->evento_id;
				$repeats = $this->mdl_evento_repeticion->get($paramsr);
				foreach ($repeats as $repeat) {
					//$nro = ($repeat->repeat_interval/86400);
					$fechaini = $repeat->repeat_start;
					//echo obtener_gmt_calendar($fechaini, 0).'<br />';
					while ($fechaini <= $repeat->repeat_end) {
						$repetidos_js_html .= "{
								title: '" . $evento->nombre . "',
			    			start: '" . obtener_gmt_calendar($fechaini, 0) . "',
			    					url: '" . base_url() . "evento/" . $evento->url . "',
			    					alt: '$evento->lugar',
			    					color: '$bgcolor',
			    					allDay: false
					},";
						$fechaini = $fechaini + $repeat->repeat_interval;
					}
				}
			} else {
				$fechai = $evento->fecha_inicio;
				$fechaf = $evento->fecha_fin;
				do {
					$eventos_js_html .= "{
							title: '" . $evento->nombre . "',
									start: '" . obtener_gmt_calendar($fechai, -5) . "',
											url: '" . base_url() . "evento/" . $evento->url . "',
											alt: 'Lugar: $evento->lugar' + ' - Hora: " . obtener_gmt_hora($fechai, -5) . "',
											color: '$bgcolor',
											allDay: false },";
					$fechai = $fechai + 86400;
				} while ($fechaf > $fechai);
			}
		}
		$javascript_inline = "
				$(document).ready(function() {
				$('#calendar').fullCalendar({
					header: {
						left: '',
						center: 'title',
						right: 'prev, next today'
					},
					editable: false,
					events: [
						" . $eventos_js_html . $repetidos_js_html . "
					]
				});
			});
        ";

        //para msotrar la leyenda de las categorias
        $params = array();
        $params['select'] = 'nombre, url, color';
        $data['categorias'] = $this->mdl_categoria->get($params);
        //cargando scripts necesarios para calendar
        $this->template->set_template("full-width");
        $this->template->add_css('fullcalendar.css', 'link', false);
        $this->template->add_css('fullcalendar.print.css', 'link', 'print');

        $this->template->add_js($javascript_inline, 'embed', false);
        $this->template->add_js(base_js() . 'evento/script_barra_horizontal.js');
        if (!$this->session->userdata('logged_in'))
            $this->template->add_js(base_js() . 'evento/no_logueado.js');
        $this->template->add_js(base_jquery() . 'fullcalendar/fullcalendar.js', 'import', false);

        $this->template->write('title', 'Calendario de eventos');
        $this->template->write('barra_navegacion', Modules::run('evento/barra_horizontal_filtro', true));
        $this->template->write_view('content', 'calendario', $data);
        $this->template->render();
    }

    public function calendariomes($embebido = false) {

        /* calendario detallado pequenio */
        $prefs = array('show_next_prev' => true, 'day_type' => 'short', 'next_prev_url' => build_segment_prev_next());
        $base_url = base_url();
        $prefs['template'] = '

		   {table_open}<table width="100%"class="calendar">{/table_open}

		   {heading_row_start}<tr>{/heading_row_start}

		   {heading_previous_cell}<th><a href="{previous_url}" class="correct_calendar_table">&lt;&lt;</a></th>{/heading_previous_cell}
		   {heading_title_cell}<th colspan="{colspan}" style="text-align: center">{heading}</th>{/heading_title_cell}
		   {heading_next_cell}<th><a href="{next_url}" class="correct_calendar_table">&gt;&gt;</a></th>{/heading_next_cell}

		   {heading_row_end}</tr>{/heading_row_end}

		   {week_row_start}<tr>{/week_row_start}
		   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr>{/cal_row_start}
		   {cal_cell_start}<td>{/cal_cell_start}

		   {cal_cell_content}<a href="{content}" class="correct_calendar_table">{day}</a>{/cal_cell_content}
		   {cal_cell_content_today}<div class="highlight div_circulo"><a href="{content}" class="correct_calendar_table"><img src="' . $base_url . 'assets/apueventos/img/circulo.png" class="circulo" />{day}</a></div>{/cal_cell_content_today}

		   {cal_cell_no_content}{day}{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="highlight div_circulo"><img src="' . $base_url . 'assets/apueventos/img/circulo.png" class="circulo" />{day}</div>{/cal_cell_no_content_today}

		   {cal_cell_blank}&nbsp;{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}
		';

        $this->load->library('calendar', $prefs);
        $mesc = uri_this_month();
        $anioc = uri_this_year();

        $data["mesc"] = $mesc;
        $data["anioc"] = $anioc;

        //obteniendo eventos para mostrar en el calendario
        $this->load->model("mdl_evento_repeticion");
        $eventosmonth = array();

        $paramse['select'] = 'evento_id, nombre, url, fecha_inicio, es_repetido, status';
        $paramse['where']['status'] = 1;
        $filas = $this->mdl_evento->get($paramse);
        foreach ($filas as $fila) {
        	/* verificamos que no se puede generar calendario de eventos deshabilitados/eliminados */
            if ($fila->es_repetido == 1) {
                //obteniendo los eventos repetidos
                $paramsr['select'] = 'evento_id, repeat_start, repeat_end, repeat_interval';
                $paramsr['where']['evento_id'] = $fila->evento_id;
                $repeats = $this->mdl_evento_repeticion->get($paramsr);

                foreach ($repeats as $repeat) {
                    //calculando los eventos siguientes de los eventos repetidos
                    if ($fila->evento_id == 42) {
                    	//FB::log($repeats, "repeats 42:\n");
                    }
                    $fechaini = $repeat->repeat_start;
                    while ($fechaini <= $repeat->repeat_end) {
                    		$mescambio = date('m', $fechaini);
                            $aniocambio = date('Y', $fechaini);
                            $clave = date('j', $fechaini);
                        if ($mescambio == $mesc && $aniocambio == $anioc) {
                            $eventosmonth[$clave] = build_segment("f_$aniocambio-$mescambio-$clave");
                        }
                        $fechaini = $fechaini + $repeat->repeat_interval;
                    }
                }
            } else {
//                var_dump($fila->fecha_inicio);exit;
                $fechad = date('j', (int)$fila->fecha_inicio);
                $fecham = date('m', (int)$fila->fecha_inicio);
                $fechay = date('Y', (int)$fila->fecha_inicio);
                if (($fecham == $mesc) && ($fechay == $anioc)) {
                    $eventosmonth[$fechad] = build_segment("f_$fechay-$fecham-$fechad");
                }
            }
        }

        $data['filas'] = $eventosmonth;
        if ($embebido === true || $this->input->post("ajax")) {
            $this->load->view("calendario-mes", $data);
        } else {
            $this->template->write('title', 'Calendario de eventos');
            $this->template->write_view('content', 'calendario-mes', $data);
            $this->template->render();
        }
    }

    public function fecha($anio, $mes, $dia) {

        $tiempo_seg = mktime(0, 0, 0, $mes, $dia, $anio);
        $ayer = $tiempo_seg - 86400;
        $mana = $tiempo_seg + 86400;

        $params = array();
        $params['where']['fecha_inicio <'] = $mana;
        $params['where']['fecha_inicio >'] = $ayer;

        //$params['select'] = 'evento_id, nombre, descripcion, capacidad, fecha_inicio, fecha_fin, url, usuario_id, ';

        $data['eventos'] = $this->mdl_evento->get($params);
        echo count($data['eventos']);

        $data['usuario_id'] = $this->session->userdata('sesion_id_user') ? $this->session->userdata('sesion_id_user') : 0;
        $data['logged_in'] = $this->session->userdata('logged_in');

        $data['categorias'] = $this->milib->categorias();

        $this->template->write('title', 'Descubre m&aacute;s eventos');
        $this->template->write_view('content', 'index', $data);
        $this->template->render();
    }

    public function test() {
        $this->template->set_template("template-sidebar");
        $this->template->write_view("content", "navs/navs");
        $javascript_inline = "$(\"#mytab a\").click(function() {
            $(this).tab('show');
            console.log(\"entro \" + Math.random());
            return false;
        });
        $('.tooltips').tooltip({selector: \"a[rel=tooltip]\"});
//        $('.popovers').popover({selector: \"a[rel=popover]\"});
        $('a[rel=popover]').popover().click(function() {
            $(this).popover('show');
            return false;
        }).mouseleave(function(){
            $(this).popover('hide');
        });
        ";

        $this->template->add_js($javascript_inline, "embed");
        $this->template->render();
        return;
        $javascript_inline = '
            //para recuperar la ip del cliente actual
            // 1. Your Data Here
            function my_callback(json) {
                console.log(json);
                console.log("IP :" + json.IP + " nCOUNTRY: " + json.COUNTRY);
            }

            function my_callback2(json) {
                // more information at http://api.easyjquery.com/navs/demo-ip.php
                console.log(json);
                console.log("IP :" + json.IP + " nCOUNTRY: " + json.COUNTRY + " City: " + json.cityName + " regionName: " + json.regionName);
            }

            // 2. Setup Callback Function
            EasyjQuery_Get_IP("my_callback"); // fastest version
            //EasyjQuery_Get_IP("my_callback2","full"); // full version
            ';
        $this->template->set_template("template-barra-navegacion");
        $this->template->add_js("http://api.easyjquery.com/easyjquery.js");
        $this->template->add_js($javascript_inline, "embed");
        $this->template->write("title", "titulo de la prueba");
        $this->template->write_view("barra_navegacion", "navs/barra_navegacion");
        $this->template->write_view("content", "navs/index");
        $this->template->render();
    }

	public function filtro() {
		/* usado para buscar (pais, ciudad) */
		$segment = $this->uri->segment_array();
		/* ahora verificamos si hay datos de busqueda */
		if ($this->input->post('q') != '') {
			/* si es que hay busqueda construimos el segment q_* y redireccionamos
			para que este incluido en la URL */
            $cadena_busqueda = strip_tags($this->input->post('q'));
            $array = array(".", "*", "_", "�", "/", "%", "&", "'", "=", "?");
            $cadena_busqueda = str_replace($array, '', $cadena_busqueda);
            $cadena_busqueda = preg_replace('/\s+/', ' ', $cadena_busqueda);
            $cadena_busqueda = trim($cadena_busqueda);
            if (str_word_count($cadena_busqueda) > 1)
                $cadena_busqueda = str_replace(" ", "+", $cadena_busqueda);
            if ($cadena_busqueda == '')
            	$this->session->set_flashdata('custom_error', 'no ha escrito nada para la b&uacute;squeda');

        	$cadena_busqueda = 'q_' . $cadena_busqueda;
            $found = false;
            for ($i = 1; $i <= count($segment); $i++) {
            	if (strlen($segment[$i]) < 3) continue;
            	if (substr($segment[$i], 0, 2) == substr($cadena_busqueda, 0, 2)) {
            		$segment[$i] = $cadena_busqueda;
            		$found = true;
            	}
            }
            if (!$found && strlen($cadena_busqueda) > 2) $segment[] = $cadena_busqueda;
            $seg = implode('/', $segment);
            redirect($seg);
        }
		$filtro_no_p_i = array('c', 'f', 't', 'q');
        $found = 0;
		$res = array();
		$res_segment = array();
		for ($i = 1, $max = count($segment); $i <= $max; $i++) {
			if (strlen($segment[$i]) < 3) continue;
			if (substr($segment[$i], 0, 2) == 'p_' || substr($segment[$i], 0, 2) == 'i_') {
				$found++;
				/* nos cercioramos que los datos sean enteros */
				if (preg_match('/^\d+$/', substr($segment[$i], 2))) {
					$res[] = $segment[$i];
				}
				else {
					/* por defecto a mi ciudad si es que no son validos los segmentos */
					$res[] = $segment[$i][0] == 'p' ? 'p_176' : 'i_2747';
					$segment[$i] = $segment[$i][0] == 'p' ? 'p_176' : 'i_2747';
				}
			}
			else {
				if ($segment[$i][1] == '_' && in_array($segment[$i][0], $filtro_no_p_i)) {
					$res_segment[] = $segment[$i];
					switch ($segment[$i][0]) {
						case 'c':
							$params = array();
							$params['select'] = "categoria_id";
							$params['where']['url'] = substr($segment[$i], 2);
							$params['return_row'] = true;
							$this->load->model("categoria/mdl_categoria");
							$categoria = $this->mdl_categoria->get($params);
							if (empty($categoria)) unset($segment[$i]);
							break;

						case 'f':
							if (!preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{1,2}$/", substr($segment[$i], 2))
                    			&& !preg_match("/^[0-9]{4}-[0-9]{2}$/", substr($segment[$i], 2))
                    			&& substr($segment[$i], 2) != "estemes"
                    			&& substr($segment[$i], 2) != "estasemana"
                    			&& substr($segment[$i], 2) != "hoydia") {
                    			unset($segment[$i]);
                    		}
                    		break;
					}
				}
			}
		}
		if ($found == 0) {
			$segment = array();
			/* para mostrar la info del visitante */
			$info = retrieve_user_info_json();
			$this->load->model("pais/mdl_pais");
			$pais = $this->mdl_pais->get_info($info);
			if (!empty($pais)) {
				$segment[0] = "p_" . $pais->pais_id;
				$this->load->model("ciudad/mdl_ciudad");
				$ciudad = $this->mdl_ciudad->get_info($info, $pais->pais_id);
				if (!empty($ciudad)) {
					$segment[1] = "i_" . $ciudad->ciudad_id;
				}
			}
			/* si no hay coincidencias elegir uno arbitrario (cusquito pe) */
			if (count($segment) == 0) {
				$segment = array('p_176', 'i_2747');
			}
			$seg = "";
			$seg = '/' . implode('/', $segment);
			$seg .= '/' . implode('/', $res_segment);
			/* para evitar que se pierda el flashdata debido a la doble redireccion
			 (de la primera si es que fue busqueda y esta sgte si es que no habia pais y ciudad por defecto) */
			if ($this->session->flashdata('custom_error'))
				$this->session->set_flashdata('custom_error', $this->session->flashdata('custom_error'));
			redirect('eventos'.$seg);
		}
		/* c = categoria, f = fecha, t = tags, p = pais, i = ciudad , q=cadena de busqueda */
		$filtro = array("c", "f", "t", "p", "i", "q");
		$where = array("status" => 1);
		foreach($segment as $seg) {
			if (strlen($seg) >= 3 && $seg[1] == '_' && in_array($seg[0], $filtro)) {
				switch ($seg[0]) {
					case "c":
						$params = array();
						$params['select'] = "categoria_id";
						$params['where']['url'] = substr($seg, 2);
						$params['return_row'] = true;
						$this->load->model("categoria/mdl_categoria");
						$categoria = $this->mdl_categoria->get($params);
						$where["categoria_id"] = $categoria->categoria_id;
						break;
					case "f":
						switch (substr($seg, 2)) {
							case "estemes":
								$fecha_ini = time();
	        					$fecha_fin = strtotime(date("Ym") . cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"))) + 24 * 60 * 60 - 1;
	        					$where['nearest_date >='] = $fecha_ini;
	        					$where['nearest_date <='] = $fecha_fin;
	        					break;
							case "estasemana":
								$fecha_ini = time();
	        					$fecha_fin = strtotime("sun this week " . date("Ymd")) + 24 * 60 * 60 - 1;
								$where['nearest_date >='] = $fecha_ini;
	        					$where['nearest_date <='] = $fecha_fin;
								break;
							case "hoydia":
								$fecha_fin = strtotime(date("Ymd", time())) + 24 * 60 * 60 - 1;
								$where['nearest_date >='] = time();
								$where['nearest_date <='] = $fecha_fin;
								break;
							default:
								$fecha = substr($seg, 2);
								$fecha_ini = strtotime($fecha);
								$fecha_fin = $fecha_ini + 24 * 60 * 60 - 1;
								$where['nearest_date >='] = $fecha_ini;
								$where['nearest_date <='] = $fecha_fin;
								$to_calendar = true;
								break;
						}
						break;
					case "p":
						$pais_id = substr($seg, 2);
						$where['pais_id'] = $pais_id;
						break;
					case "i":
						$ciudad_id = substr($seg, 2);
						$where['ciudad_id'] = $ciudad_id;
						break;
					case "q":
						$terminos = substr($seg, 2);
						$q = str_replace("+", ' ', $terminos);
			            $trozos = explode(" ", $q);
			            $q = "";
			            foreach ($trozos as $value) {
			            	$q = $value . '* ';
			            }
						$where['MATCH(nombre) AGAINST(\'' . $q . '\' IN BOOLEAN MODE)'] = "";
						break;
					default:
						break;
				}
			}
		}

        $data = array(
        		'segment' => $segment,
        		/* esto de categorias , ahora creo que ya no se usa (x borrar) */
        		'categorias' => $this->milib->categorias(),
        		'logged_in' => $this->session->userdata('logged_in'),
        		'usuario_id' => $this->session->userdata('sesion_id_user') ? $this->session->userdata('sesion_id_user') : 0,
        		/* Seccion para recuperar los eventos del usuario actual
        		 * y segun sea poner sus botones de inscrito a este evento o participar
        		 */
        		'mis_eventos' => $this->recuperar_mis_id_eventos()
        );
		/* recuperamos y adjuntamos pais, ciudad y categoria a eventos */
		$data['categoria'] = array();
        $params = array(
        		'paginate' => true,
        		'limit' => $this->config->item('results_per_page'),
        		'page' => uri_page(),
        		'where' => $where,
        		'to_calendar' => isset($to_calendar),
        		'fecha_ini' => isset($fecha_ini) ? $fecha_ini : false
        );

		$data['eventos'] = $this->mdl_evento->proximos_eventos($params);

		$this->load->model("categoria/mdl_categoria");
		$this->load->model("ciudad/mdl_ciudad");
		$this->load->model("pais/mdl_pais");
		$j = 0;
		foreach ($data['eventos'] as $evento) {
			$params_ciudad = array();
			$params_ciudad['select'] = 'ciudad_id, pais_id, nombre';
			$params_ciudad['where']['ciudad_id'] = $data['eventos'][$j]->ciudad_id;
			$params_ciudad['return_row'] = true;
			$ciudad = $this->mdl_ciudad->get($params_ciudad);
			$data['eventos'][$j]->ciudad = $ciudad->nombre;

			$params_pais = array();
			$params_pais['select'] = "nombre";
			$params_pais['where']['pais_id'] = $ciudad->pais_id;
			$params_pais['return_row'] = true;
			$pais = $this->mdl_pais->get($params_pais);
			$data['eventos'][$j]->pais = $pais->nombre;

			/* recuperamos su categoria */
			$params_categoria = array();
			$params_categoria['select'] = "categoria_id, nombre, url, img";
			$params_categoria['where']['categoria_id'] = $data['eventos'][$j]->categoria_id;
			$params_categoria['return_row'] = true;
			$categoria = $this->mdl_categoria->get($params_categoria);
			$data['eventos'][$j]->categoria = $categoria->nombre;
			$data['eventos'][$j]->categoria_url = $categoria->url;
			$data['eventos'][$j]->categoria_img = $categoria->img;
			//$data['categoria'][$data['eventos'][$j]->categoria_id] = $categoria->img;
			$j++;
		}

		/* publicidad */
		//podemos agregar un modulo de publicadad para gestionas eventos promocionados
		//$data['publicidad'] = $this->publicidad->eventos_promocionados();
		$data['publicidad'] = 'http://www.acercate-eventos.dennisbot.com/assets/uploads/eventos/evento_741bb0881cecd02e.jpg';

		if ($this->input->post("ajax")) {
			$this->load->view("index", $data);
		} else {
			$this->render_template_barra_horizontal($data);
		}
	}


    //contando los eventos por categoria
    function nro_eventos_categoria($categoria_id) {
        /*$params['select'] = 'count(evento_id) as nro';
        $params['where']['categoria_id'] = $categoria_id;
        $params['return_row'] = true;
        $data = $this->mdl_evento->get($params);
        return $data->nro;*/
    	$params['where'] = array('categoria_id' => $categoria_id);
    	$nro = $this->mdl_evento->proximos_eventos($params);
    }

	private function render_template_barra_horizontal($data = array(), $title = 'Eventos') {
		/* estos 3 van juntos */
		$this->template->set_template("template-barra-navegacion");
		$this->template->add_js(base_js() . "evento/script_barra_horizontal.js");
		$this->template->add_js(base_js() . "evento/actions_user_events.js");

		/* esto para la caja de login dinamica */
		if (!$this->session->userdata('logged_in'))
			$this->template->add_js(base_js() . "user/no_logueado.js");

		//seo
		$breadcrumb = $this->milib->nombres_url_segment($data['segment']);
		$breadcrumb_data = '';
		foreach ($breadcrumb as $variable) {
			$breadcrumb_data = $breadcrumb_data.' '.$variable;
		}

		$data['seo_keyword'] = "eventos ".$breadcrumb_data.", publicidad, cine, publicar, publicaciones";
		$data['seo_descripcion'] = "Eventos: ". $breadcrumb_data.", publicar eventos en internet, portal de eventos - Acercate-eventos.com";

		$this->template->write('header_title', 'Eventos:'. $breadcrumb_data);
		$this->template->write('title', $title);
		$this->template->write_view('system_messages', 'dashboard/system_messages');

		/* esta region es la razon de ser de este template */
		$this->template->write('barra_navegacion', Modules::run("evento/barra_horizontal_filtro", true));
		$this->template->add_js(base_js() . "evento/filtro_badges.js");
		$this->template->write_view('breadcrumbs', 'breadcrumb/index', array("segment" => $data['segment']));

		$this->template->write_view('content', 'index', $data);
		$this->template->write('calendario', Modules::run('evento/calendariomes', true));
		//$this->template->write('publicidad', Modules::run('publicidad', true));
		$this->template->write_view('publicidad', 'publicidad/index', $data['publicidad']);
		$this->template->render();
	}
}

?>