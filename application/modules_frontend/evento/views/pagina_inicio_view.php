<!-- animacion -->
<?php if ( isset($categorias)) : ?>

	<div class="flexslider">

		<ul class="slides">
			<?php foreach ($categorias_imagenes as $categoria) : ?>
			<li>
				<?php if (isset($cat_with_events[$categoria->categoria_id])) : ?>
					<a href="<?=base_url()?>eventos/c_<?=$categoria->url?>">
				<?php endif; ?>
				<img src="<?php echo base_img(); ?>categorias/<?php echo $categoria->img ?>" alt="<?php echo $categoria->nombre ?>">
				<h2><?php echo $categoria->nombre ?></h2>
				<?php if (isset($cat_with_events[$categoria->categoria_id])) : ?></a><?php endif; ?>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>

<?php endif; ?>

<!-- eventos por categoria -->

<?php $chunked = array_chunk($categorias, 2); ?>

<div id="home-categories" class="row">

	<div class="span12 background-white shadow">

		<?php foreach($chunked as $grouped): ?>

		<div class="row">

			<?php foreach($grouped as $categoria): ?>

			<div class="span6">

				<div class="elemento">
				<div class="ver_todo"><a href="<?php echo base_url() ?>eventos/c_<?php echo $categoria->url; ?>"><span>ver todo</span> <img src="<?php echo base_img() ?>evento-icons/plus.png" /></a></div>
				<h2>
					<span style="background:<?php echo $categoria->color; ?>"></span>
					<a href="<?php echo base_url() . "eventos/c_" . $categoria->url ?>"><?php echo $categoria->nombre; ?></a>
				</h2>

				<?php foreach($categoria->eventos as $evento): ?>
					<div class="evento-relacionado">
						<div class="right left-padded">
							<!--div class="cuando"><?php  //echo  format_date($evento->fecha_inicio) ?></div-->
							<div class="cuando">
								<?php
								echo format_date_to_show($evento->nearest_date); ?>
							</div>
							<div class="boton-participar-<?php echo $evento->evento_id ?>">
								<?php if (!$logged_in) : ?>
									<button class="btn isesion participar" data-evento-id="<?php echo $evento->evento_id; ?>" rel="tooltip" data-original-title="Inicia sesi&oacute;n para participar en este evento">
										Participar
									</button>
									<?php else :
									if (isset($mis_eventos) && !in_array($evento->evento_id, $mis_eventos)): ?>
									<button class="btn btn-success participar" data-evento-id="<?php echo $evento->evento_id; ?>">
										Participar
									</button>
									<?php else : ?>
									<div class="btn-group">
										<?php if ($usuario_id == $evento->usuario_id) : ?>
										<button class="btn btn-warning" data-toggle="dropdown">Asistir&eacute;</button>
										<?php else : ?>
										<button class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
											Asistir&eacute; <span class="caret"></span>
										</button>
										<ul class="dropdown-menu">
											<li>
												<a style="cursor: pointer" data-evento-id="<?php echo $evento->evento_id; ?>" class="cancelar_asistencia">Cancelar mi asistencia</a>
											</li>
										</ul>
										<?php endif; ?>
									</div>
									<?php endif; endif;?>
							</div>
						</div>

						<div class="thumb-image">
							<a href="<?php echo base_url() ?>evento/<?php echo $evento->url; ?>">
							<?php
							$img_thumb = ($evento->img)? base_url_foto_evento() . $evento->img ."_thumb.jpg" : base_img() . "/categorias/" . $categoria->img;
							echo '<img width="90" height="60" src="' . $img_thumb . '">';
							?>
							</a>
						</div>

						<div class="nombre">
							<a href="<?php echo base_url() ?>evento/<?php echo $evento->url; ?>">
								<?php echo $evento->nombre ?>
							</a>
						</div>

						<div class="donde">
							<?php echo $evento->lugar; ?>, ubicado en <?php echo $evento->ubicacion; ?>, <?php echo $evento->ciudad; ?> - <?php echo $evento->pais; ?>
						</div>

					</div><!-- evento -->
				<?php endforeach; ?>
				</div><!-- elemento -->

			</div><!-- span6 -->

			<?php endforeach; ?>

		</div><!-- row -->

		<?php endforeach; ?>

	</div><!-- span12 -->

</div><!-- row -->