<?php if (isset($eventos) && !empty($eventos)) : ?>
	<div id="lista-eventos-relacionados">

		<?php foreach ($eventos as $evento_relacionado) : ?>
		<div class="evento-relacionado">

			<div class="right left-padded">

				<div class="cuando">
					<?php
						echo format_date_to_show($evento_relacionado->nearest_date);
					?>
				</div>
				<?php if (time() < $evento_relacionado->nearest_date) : ?>
					<div class="boton-participar-<?php echo $evento_relacionado->evento_id; ?>">
						<?php if (!$logged_in) : ?>
						<button class="btn isesion participar" data-evento-id="<?php echo $evento_relacionado->evento_id; ?>" rel="tooltip" data-original-title="Inicia sesi&oacute;n para participar en este evento">
							Participar
						</button>
						<?php else :
						if (isset($mis_eventos) && !in_array($evento_relacionado->evento_id, $mis_eventos)): ?>
						<button class="btn btn-success participar" data-evento-id="<?php echo $evento_relacionado->evento_id; ?>">
							Participar
						</button>
						<?php else : ?>
						<div class="btn-group">
							<?php if ($usuario_id == $evento_relacionado->usuario_id) : ?>
							<button class="btn btn-warning" data-toggle="dropdown">Asistir&eacute;</button>
							<?php else : ?>
							<button class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
								Asistir&eacute; <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a style="cursor: pointer" data-evento-id="<?php echo $evento_relacionado->evento_id; ?>" class="cancelar_asistencia">Cancelar mi asistencia</a>
								</li>
							</ul>
							<?php endif; ?>
						</div>
						<?php endif; endif;?>
					</div>
				<?php else: ?>
				<span class="label label-important" data-evento-id="<?= $evento_relacionado->evento_id; ?>">Evento Finalizado</span>
				<?php  endif; ?>
			</div>

			<div class="thumb-image">
				<a href="<?php echo base_url() ?>evento/<?php echo $evento_relacionado->url; ?>">
					<?php
					$img_thumb = (isset($evento_relacionado->img) && $evento_relacionado->img != '') ? base_url_foto_evento() . $evento_relacionado->img . "_thumb.jpg" : base_img() . "/categorias/" . $evento_relacionado->categoria_img;
					echo '<img width="90" height="60" src="' . $img_thumb . '">';
					?>
				</a>
			</div>

			<div class="nombre">
				<a href="<?php echo base_url() ?>evento/<?php echo $evento_relacionado->url; ?>">
					<?php echo $evento_relacionado->nombre ?>
				</a>
			</div>

			<div class="donde">
				<?php echo $evento_relacionado->lugar; ?>, ubicado en <?php echo $evento_relacionado->ubicacion; ?>, <?php echo $evento_relacionado->ciudad; ?> - <?php echo $evento_relacionado->pais; ?>
			</div>
			<?php if ($evento_relacionado->recent_valid != -1 && $evento_relacionado->nearest_date != $evento_relacionado->recent_valid) : ?>
			<div style="clear:both; text-align: center">
				<span class="label label-info">Este evento tambi&eacute;n esta programado para repetirse el</span>
				<a class="icon-external-link btn" href="<?php echo base_url() ?>evento/<?php echo $evento_relacionado->url; ?>">
					<strong>
						<?php echo format_date_to_show($evento_relacionado->recent_valid); ?>
					</strong>
				</a>
			</div>
		<?php endif; ?>
		</div><!-- evento -->

		<?php endforeach; ?>
	</div><!-- lista-eventos -->

	<!-- pagination -->
	<?php if ($this->mdl_evento->page_links) { ?>
        <div id="pagination" class="pagination pagination-centered">
            <ul>
                <?php echo $this->mdl_evento->page_links; ?>
            </ul>
        </div>
	<?php } ?>

<?php
else : ?>
<div class="alert alert-info fade in">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<div class="pagination-centered">
		<p><strong>Oops! no hay eventos para mostrar!</strong></p>
		<p><strong>puedes eliminar algunos criterios del filtro!</strong></p>
	</div>
</div>
<?php endif;?>