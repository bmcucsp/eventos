<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<div>
    <input id="searchTextField" type="text" size="50">
    <input type="radio" name="type" id="changetype-all" checked="checked">
    <label for="changetype-all">All</label>

    <input type="radio" name="type" id="changetype-establishment">
    <label for="changetype-establishment">Establishments</label>

    <input type="radio" name="type" id="changetype-geocode">
    <label for="changetype-geocode">Geocodes</label>
</div>
<div id="map_canvas"></div>
