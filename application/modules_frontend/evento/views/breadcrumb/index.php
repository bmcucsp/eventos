<ul class="breadcrumb" style="margin: 0px 0px 2px;">
	<li>Estas viendo los Eventos seg&uacute;n :</li>
	<?php
	/* buscamos mas segmentos si hubieran (x categorias, x fecha, x tags) */
	$filtro = array("p_", "i_", "c_", "f_", "t_", "q_");
	$filtro_descripcion = array("p_" => "Pais: ", "i_" => "Ciudad: ", "c_" => "Categoria: ", "f_" => "Fecha: ", "t_" => "Tags: ", "q_" => 'Busqueda: ');
	foreach($segment as $seg) :
		if ($seg == "eventos" || !in_array(substr($seg, 0, 2), $filtro)) continue;
		$print = "";
		$prefix = substr($seg, 0, 2);
		switch ($prefix) {
			case "c_":
				$print = ucfirst(substr($seg, 2));
				break;
			case "f_":
				$fecha = substr($seg, 2);
				if ($fecha == "estasemana" || $fecha == "estemes" || $fecha == "hoydia") {
						switch ($fecha) {
							case 'estasemana':
								$print = "Esta semana";
								break;
							case 'estemes':
								$print = "Este mes";
								break;
							case 'hoydia':
								$print = "Hoy";
								break;
						}
				}
				else
					$print = str_replace('-', '/', substr($seg, 2));
				break;

			case "p_":
				$this->load->model("evento/mdl_pais");
				$params = array();
				$params['select'] = "nombre";
				$params["where"]["pais_id"] = substr($seg, 2);
				$params['return_row'] = true;
				$pais = $this->mdl_pais->get($params);
				$print = $pais->nombre;
				break;

			case "i_":
				$this->load->model("evento/mdl_ciudad");
				$params = array();
				$params['select'] = "nombre";
				$params["where"]["ciudad_id"] = substr($seg, 2);
				$params['return_row'] = true;
				$ciudad = $this->mdl_ciudad->get($params);
				$print = $ciudad->nombre;
				break;
			case "q_":
				$print = str_replace("+", ' ', substr($seg, 2));
				$print = "\"" .  $print . "\"";
			default:
				break;
		}
		$path_to_filter = "";
		foreach ($segment as $value) {
			if ($value != $seg) $path_to_filter .= "/" . $value;
		}
		?>
		<span class="badge badge-info pointer_hover"><?php echo $filtro_descripcion[$prefix].$print." "; ?>
		<?php if ($prefix != 'p_') : ?>
			<i class="icon-remove" data-href="<?php echo site_url($path_to_filter); ?>"></i>
		<?php endif; ?>
		</span>&nbsp;

	<?php endforeach; ?>
</ul>