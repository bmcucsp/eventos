<div class="row-fluid">

	<div class="span6">

		<div id="imagen">
			<!--creo que es x el tamano estandard que no quepa x completo-->
			<?php $img = ($evento->img) ? base_url_foto_evento() . $evento->img . "_standard.jpg" : base_img() . "/categorias/" . $categoria->img; ?>
			<a id="boton-mostrar-imagen" type="button" data-toggle="modal" data-target="#modal-imagen">
				<!-- <img id="main-image" src="<?php echo $img ?>" width="640" height="360"> -->
				<img id="main-image" src="<?php echo $img ?>" />
			</a>

			<div id="modal-imagen" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3>
						<?php echo $evento->nombre ?>
					</h3>
				</div>
				<div class="modal-body">
					<?php $img = ($evento->img) ? base_url_foto_evento() . $evento->img . ".jpg" : base_img() . "/categorias/" . $categoria->img; ?>
					<img src="<?php echo $img ?>" width="640" height="360">
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
				</div>
			</div>
			<?php echo ($evento->detalle_costo == "gratuito") ? '<div id="evento-gratuito"><img alt="Entrada Libre, Evento Gratuito" src="' . base_img() . '/entrada-libre.png" /></div>' : "" ?>

			<div id="info-small">
				<div id="icons">
					<?php if ($evento->presencial != 1) : ?>
					<img title="Evento virtual, por Internet" src="<?php echo base_img() ?>/evento-icons/evento-virtual.png" />
					<?php endif; ?>

					<?php if ($evento->capacidad > 0) : ?>
					<div id="capacidad">
						<img title="Evento con capacidad para <?php echo $evento->capacidad ?> asistentes" src="<?php echo base_img() ?>/evento-icons/evento-asistentes.png" />
						<div id="cantidad">
							<?php echo $evento->capacidad ?>
						</div>
					</div>
					<?php endif; ?>

					<?php if ($evento->publico != 1) : ?>
					<img title="Evento privado, contactar al organizador para participar" src="<?php echo base_img() ?>/evento-icons/evento-privado.png" />
					<?php endif; ?>
				</div>
				<h1>
					<?php echo (strlen($evento->nombre) <= 50 ? $evento->nombre : substr($evento->nombre, 0, 50)) . " ..."; ?>
				</h1>
			</div>
		</div><!-- imagen -->

		<div id="info-medium">
			<div id="organizador" class="left">
				<?php echo $evento->organizador ?>
				<br />
				<span>Organizador</span>
			</div>
			<div id="fecha" class="right">
				<?php
					if($evento->es_repetido == 0){
						if(format_date_to_show($evento->fecha_inicio) != format_date_to_show($evento->fecha_fin)) {
							echo format_date_to_show($evento->fecha_inicio) . ' al '. format_date_to_show($evento->fecha_fin);
						}
						else {
							echo format_date_to_show($evento->fecha_inicio);
						}
					}
					else
						echo format_date_to_show($evento->nearest_date) ;
					?><br/>
				<span>Fecha</span>
			</div>

		</div>
		<?php
			if (time() > (int)$evento->nearest_date) {
		?>
		<h2 class="evento_finalizado">Evento Finalizado</h2>
		<?php
			}
		?>
		<h2>Descripci&oacute;n/Itinerario</h2>
		<div id="descripcion">
			<?php echo html_entity_decode($evento->descripcion, ENT_COMPAT, 'UTF-8'); ?>
		</div>
		<div>
			<script type="text/javascript"><!--
			google_ad_client = "ca-pub-6968540297762595";
			/* single-336x280-esceniko */
			google_ad_slot = "5235603538";
			google_ad_width = 336;
			google_ad_height = 280;
			//-->
			</script>
			<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>		
		</div>

	</div><!-- span6 -->

	<div class="span6">

		<h2>Detalles
			<span class="derecha_editar">
				<?php if ($logged_in && $usuario_id == $evento->usuario_id) : ?>
					<a href="<?php echo site_url('evento/agregar/evento_id/'. $evento->evento_id); ?>" title="<?php echo $this->lang->line('edit'); ?>">(editar)<?php echo icon('edit'); ?></a>
				<?php endif; ?>
			</span>
		</h2>
		<div class="block">
			<div class="right left-padded">
				<?php if (time() < (int)$evento->nearest_date) : ?>
				<div class="boton-participar-<?php echo $evento->evento_id; ?>">
					<?php if (!$logged_in) : ?>
					<button class="btn btn-large isesion participar" data-evento-id="<?php echo $evento->evento_id; ?>" rel="tooltip" data-original-title="Inicie sesi&oacute;n para participar en este evento">
						Participar
					</button>
					<?php else :
					if (isset($mis_eventos) && !in_array($evento->evento_id, $mis_eventos)): ?>
					<button class="btn btn-large btn-success participar" data-evento-id="<?php echo $evento->evento_id; ?>">
						Participar
					</button>
					<?php else : ?>
					<div class="btn-group">
						<?php if ($usuario_id == $evento->usuario_id) : ?>
						<button class="btn btn-large btn-warning" data-toggle="dropdown">Asistir&eacute;</button>
						<?php else : ?>
						<button class="btn btn-large btn-warning dropdown-toggle" data-toggle="dropdown">
							Asistir&eacute; <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li>
								<a style="cursor: pointer" data-evento-id="<?php echo $evento->evento_id; ?>" class="cancelar_asistencia">Cancelar mi asistencia</a>
							</li>
						</ul>
						<?php endif; ?>
					</div>
					<?php endif; endif;?>
				</div>
				<?php else: ?>
				<span class="label label-important">Evento Finalizado</span>
				<?php endif; ?>
			</div>
			<p>
				<b>Lugar:</b>
				<?php echo $evento->lugar; ?>
				<br /><b>Direcci&oacute;n:</b>
				<?php echo $evento->ubicacion; ?>
				<br />
				<?php echo $evento->ciudad; ?> - <?php echo $evento->pais; ?>
			</p>

			<?php if ($evento->latitud && $evento->longitud) : ?>
			<div class="mapa">
				<button id="boton-mostrar-mapa" class="btn" type="button" data-toggle="modal" data-target="#modal-mapa">
					<img alt="mostrar mapa" src="<?php echo base_img() ?>/evento-icons/evento-popup.png" />
					Ver mapa de ubicaci&oacute;n
				</button>
			</div>
			<div id="modal-mapa" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3>
						<b>Ubicaci&oacute;n del evento:</b> "<?php echo $evento->nombre ?>"
					</h3>
				</div>
				<div class="modal-body">
					<div id="map-canvas"></div>
				</div>
				<div class="modal-footer">
					<div class="left">
						Puedes utilizar las flechas de tu teclado para mover el mapa y tambien los signos m&aacute;s (+) y menos (-) para acercar y alejar
					</div>
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
				</div>
			</div>
			<?php endif; ?>
		</div><!-- block -->

		<div class="block">
			<?php echo ($evento->detalle_costo != "gratuito") ? "<strong>Costo:</strong> " . nl2br(strip_tags($evento->detalle_costo)) : "" ?>
			<p>
				<?php if ($evento->dirigido_a) : ?>
				<b>Dirigido a:</b>
				<?php echo $evento->dirigido_a; ?>
				<br />
				<?php endif; ?>

				<b>Categor&iacute;a:</b> <a href="<?php echo base_url() ?>eventos/c_<?php echo $categoria->url ?>"><?php echo $evento->categoria; ?>
				</a>
				<br />
				<b>Hora de Inicio:</b>
				<?php if ($evento->es_repetido == '0') : ?>
				<?php echo date("h:i:s A", $evento->fecha_inicio); ?>
				<?php else: ?>
				<?php echo date("h:i:s A", $evento->nearest_date); ?>
				<?php endif; ?>
				<br />
				<b>Hora de Fin:</b>
				<?php echo date("h:i:s A", $evento->fecha_fin); ?>
			</p>
		</div><!-- block -->
		<?php if ($evento->detalle_organizador) : ?>
		<div class="block">
			<b>Organizador: </b><?php echo $evento->organizador . "<br/>" . $evento->detalle_organizador; ?>
		</div>
		<?php endif; ?>

		<?php if ($evento->es_repetido) : ?>
		<h2>Fechas Programadas</h2>
		<div class="block">
			<?php if ($events_meta->repeat_type == "diariamente") : ?>

			<p>
				Este evento se repite cada
				<?php
					$times = ((int)$events_meta->repeat_interval / 24 / 60 / 60);
					echo $times, " dia" , ($times > 1) ? "s " : " ";
				?>
				desde el <?php echo format_date_to_show($evento->fecha_inicio); ?>
				hasta el <?php echo format_date_to_show($evento->fecha_fin); ?>
			</p>
			<?php elseif ($events_meta->repeat_type == "semanalmente") : ?>

			<p>
				Este evento se repite cada
				<?php
					$times = ((int)$events_meta->repeat_interval / 7 / 24 / 60 / 60);
					echo $times, " semana" , ($times > 1) ? "s " : " ";
				?>
				desde el <?php echo format_date_to_show($evento->fecha_inicio); ?>
				hasta el <?php echo format_date_to_show($evento->fecha_fin); ?>
			</p>

			<?php endif; ?>
			<?php if ($evento->latitud && $evento->longitud) : ?>
			<div class="fechas">
				<!-- <button type="button" data-toggle="modal" data-target="#modal-fechas" class="btn">
					<img alt="mostrar fechas" src="<?php echo base_img() ?>/evento-icons/evento-popup.png" />
					Ver fechas programadas para esta semana
				</button>  -->

				<div id="modal-fechas" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
						<h3>
							<b>Pr&oacute;ximas fechas del evento:</b>
							<?php echo $evento->nombre ?>
						</h3>
					</div>
					<div class="modal-body">
						<p>Este evento esta programado para repetirse los Martes y Jueves cada semana, desde el 05/06/12 al 02/06/13</p>
						<ul>
							<li>Fecha 01</li>
							<li>Fecha 02</li>
							<li>Fecha 03</li>
						</ul>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
					</div>
				</div>
			</div>
			<?php endif; ?>

		</div><!-- block -->

		<?php endif; ?>
		
		<div class="block">
			<script type="text/javascript"><!--
			google_ad_client = "ca-pub-6968540297762595";
			/* single-468x60-esceniko */
			google_ad_slot = "8189069936";
			google_ad_width = 468;
			google_ad_height = 60;
			//-->
			</script>
			<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>		
		</div>

		<h2>Compartir</h2>
		<?php $this->load->view("social/social"); ?>
		<h2>Comentarios</h2>
		<div id="comentarios">
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=350188195076932";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
			<div class="fb-comments"
				data-href="<?php echo base_url() . "evento/" . $evento->url ?>"
				data-num-posts="5"></div>
		</div><!-- comentarios -->

		<?php if (isset($eventos_relacionados)) : ?>
		<h2>
			M&aacute;s eventos de <?php echo $evento->categoria ?>
			<span style="font-size: .5em;float:right;">
				<?php
				$url_categoria = $categoria->url;
				echo anchor('eventos/c_' . $url_categoria, "ver m&aacute;s ...");
				?>
			</span>
		</h2>

		<div id="lista-eventos-relacionados">
			<?php foreach ($eventos_relacionados as $evento_relacionado) : ?>

			<div class="evento-relacionado">

				<div class="right left-padded">

					<div class="cuando">
						<?php echo format_date_to_show($evento_relacionado->nearest_date) ?>
					</div>
					<?php if (time() < $evento_relacionado->nearest_date) : ?>
					<div class="boton-participar-<?php echo $evento_relacionado->evento_id; ?>">
						<?php if (!$logged_in) : ?>
						<button class="btn isesion participar" data-evento-id="<?php echo $evento_relacionado->evento_id; ?>" rel="tooltip" data-original-title="Inicia sesi&oacute;n para participar en este evento">
							Participar
						</button>
						<?php else :
						if (isset($mis_eventos) && !in_array($evento_relacionado->evento_id, $mis_eventos)): ?>
						<button class="btn btn-success participar" data-evento-id="<?php echo $evento_relacionado->evento_id; ?>">
							Participar
						</button>
						<?php else : ?>
						<div class="btn-group">
							<?php if ($usuario_id == $evento_relacionado->usuario_id) : ?>
							<button class="btn btn-warning" data-toggle="dropdown">Asistir&eacute;</button>
							<?php else : ?>
							<button class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
								Asistir&eacute; <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a style="cursor: pointer" data-evento-id="<?php echo $evento_relacionado->evento_id; ?>" class="cancelar_asistencia">Cancelar mi asistencia</a>
								</li>
							</ul>
							<?php endif; ?>
						</div>
						<?php endif; endif;?>
					</div>
					<?php else : ?>
					<span class="label label-important">Evento Finalizado</span>
					<?php endif; ?>

				</div>

				<div class="thumb-image">
					<a href="<?php echo base_url() ?>evento/<?php echo $evento_relacionado->url; ?>">
						<?php
						$img_thumb = ($evento_relacionado->img) ? base_url_foto_evento() . $evento_relacionado->img . "_thumb.jpg" : base_img() . "/categorias/" . $categoria->img;
						echo '<img width="90" height="60" src="' . $img_thumb . '">';
						?>
					</a>
				</div>

				<div class="nombre">
					<a href="<?php echo base_url() ?>evento/<?php echo $evento_relacionado->url; ?>">
						<?php echo $evento_relacionado->nombre ?>
					</a>
				</div>

				<div class="donde">
					<?php echo $evento_relacionado->lugar; ?>, ubicado en <?php echo $evento_relacionado->ubicacion; ?>, <?php echo $evento_relacionado->ciudad; ?> - <?php echo $evento_relacionado->pais; ?>
				</div>

			</div><!-- evento -->

			<?php endforeach; ?>
		<!-- pagination -->
		<?php if ($this->mdl_evento->page_links) : ?>
		<div id="loading" style="position:relative;"></div>
		<div id="pagination" class="pagination pagination-centered">
			<ul>
				<?php echo $this->mdl_evento->page_links; ?>
			</ul>
		</div>
		<?php endif; ?>
		</div><!-- lista-eventos -->

		<?php
			else:
				echo 'no hay';
		endif;?>

	</div><!-- span6 -->

</div><!-- row-fluid -->