<div class="modal hide fade" id="modal_semanalmente">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>¿Cuando repite este evento?</h3>
    </div>
    <div class="modal-body">
        <?php
        if ($evento_repetido && $repeat_type == "semanalmente") {
            if ($actualizacion) {
                $interval_weeks = $repeticiones[0]->repeat_interval / (7 * 24 * 60 * 60);

                $hour_start_weekly = date("g", $this->mdl_evento->form_value('fecha_inicio'));
                $minute_start_weekly = date("i", $this->mdl_evento->form_value('fecha_inicio'));
                $meridian_start_weekly = date("A", $this->mdl_evento->form_value('fecha_inicio'));

                $hour_end_weekly = date("g", $this->mdl_evento->form_value('fecha_fin'));
                $minute_end_weekly = date("i", $this->mdl_evento->form_value('fecha_fin'));
                $meridian_end_weekly = date("A", $this->mdl_evento->form_value('fecha_fin'));
            } else {
                $interval_weeks = (isset($arreglo_repeticiones['num_semanas']) && $arreglo_repeticiones['num_semanas'] != '')  ? $arreglo_repeticiones['num_semanas'] : '';

                $hour_start_weekly = (isset($arreglo_repeticiones['hour_start_weekly']) && $arreglo_repeticiones['hour_start_weekly'] != '') ? $arreglo_repeticiones['hour_start_weekly'] : 0;
                $minute_start_weekly = (isset($arreglo_repeticiones['minute_start_weekly']) && $arreglo_repeticiones['minute_start_weekly'] != '') ? $arreglo_repeticiones['minute_start_weekly'] : 0;
                $meridian_start_weekly = (isset($arreglo_repeticiones['meridian_start_weekly']) && $arreglo_repeticiones['meridian_start_weekly'] != '') ? $arreglo_repeticiones['meridian_start_weekly'] : 'AM';

                $hour_end_weekly = (isset($arreglo_repeticiones['hour_end_weekly']) && $arreglo_repeticiones['hour_end_weekly'] != '') ? $arreglo_repeticiones['hour_end_weekly'] : 0;
                $minute_end_weekly = (isset($arreglo_repeticiones['minute_end_weekly']) && $arreglo_repeticiones['minute_end_weekly'] != '') ? $arreglo_repeticiones['minute_end_weekly'] : 0;
                $meridian_end_weekly = (isset($arreglo_repeticiones['meridian_end_weekly']) && $arreglo_repeticiones['meridian_end_weekly'] != '') ? $arreglo_repeticiones['meridian_end_weekly'] : 'AM';
            }
        }
        ?>
        <div class="form-horizontal">
            <div class="control-group">
                <label for="" class="control-label">Repetir cada:</label>
                <div class="controls">
                    <select name="weekly-repeat_interval" id="id_weekly-repeat_interval">
                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                            <option value="<?php echo $i ?>" <?php if (isset($interval_weeks) && $interval_weeks == $i) : ?> selected ="selected" <?php endif; ?> ><?php echo $i ?></option>
                            <?php
                        }
                        ?>
                    </select> semana(s)
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">Hora de inicio:</label>
                <div class="controls">
                    <select name="hour_start_weekly" id="hour_start_weekly">
                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                            <option value="<?php echo $i; ?>" <?php if (isset($hour_start_weekly) && $hour_start_weekly == $i) : ?> selected="selected" <?php endif; ?> ><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                    <span class="separator">:</span>
                    <select name="minute_start_weekly" id="minute_start_weekly">
                        <?php for ($i = 0; $i < 12; $i++) { ?>
                            <option value="<?php echo sprintf('%02u', $i * 5); ?>" <?php if (isset($minute_start_weekly) && $minute_start_weekly == sprintf('%02u', $i * 5)) : ?> selected="selected" <?php endif; ?> ><?php echo sprintf('%02u', $i * 5); ?></option>
                        <?php } ?>
                    </select>
                    <select name="meridian_start_weekly" id="meridian_start_weekly">
                        <option value="AM" <?php if (isset($meridian_start_weekly) && $meridian_start_weekly == "AM") : ?> selected="selected" <?php endif; ?> >AM</option>
                        <option value="PM" <?php if (isset($meridian_start_weekly) && $meridian_start_weekly == "PM") : ?> selected="selected" <?php endif; ?> >PM</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">Hora de Fin:</label>
                <div class="controls">
                     <select name="hour_end_weekly" id="hour_end_weekly">
                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                            <option value="<?php echo $i; ?>" <?php if (isset($hour_end_weekly) && $hour_end_weekly == $i) : ?> selected="selected" <?php endif; ?> ><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                    <span class="separator">:</span>
                    <select name="minute_end_weekly" id="minute_end_weekly">
                        <?php for ($i = 0; $i < 12; $i++) { ?>
                            <option value="<?php echo sprintf('%02u', $i * 5); ?>" <?php if (isset($minute_end_weekly) && $minute_end_weekly == sprintf('%02u', $i * 5)) : ?> selected="selected" <?php endif; ?> ><?php echo sprintf('%02u', $i * 5); ?></option>
                        <?php } ?>
                    </select>
                    <select name="meridian_end_weekly" id="meridian_end_weekly">
                        <option value="AM" <?php if (isset($meridian_end_weekly) && $meridian_end_weekly == "AM") : ?> selected="selected" <?php endif; ?> >AM</option>
                        <option value="PM" <?php if (isset($meridian_end_weekly) && $meridian_end_weekly == "PM") : ?> selected="selected" <?php endif; ?> >PM</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">Repite los dias:</label>
                <div class="controls">
                    <?php
                    $dias = array(
                            "Monday" => array("activo" => false, "alias" => "Lu"),
                            "Tuesday" => array("activo" => false, "alias" => "Ma"),
                            "Wednesday" => array("activo" => false, "alias" => "Mi"),
                            "Thursday" => array("activo" => false, "alias" => "Ju"),
                            "Friday" => array("activo" => false, "alias" => "Vi"),
                            "Saturday" => array("activo" => false, "alias" => "Sa"),
                            "Sunday" => array("activo" => false, "alias" => "Do")
                        );
                    if ($evento_repetido && $repeat_type == "semanalmente") {
                        if ($actualizacion) {
                            foreach ($repeticiones as $repeticion) {
                                $dias[date("l", $repeticion->repeat_start)]["activo"] = true;
                            }
                        } else {
                            if (isset($arreglo_repeticiones['dias'])) :
                                foreach ($arreglo_repeticiones['dias'] as $dia) {
                                    $dias[$dia]["activo"] = true;
                                }
                            endif;
                        }
                    }
                    foreach ($dias as $dia => $detalle_dia) :
                        ?>
                        <input type="checkbox" value="<?php echo $dia; ?>" name="days[]" <?php if ($detalle_dia["activo"]): ?> checked="checked" <?php endif; ?> style="margin: 1px 2px 2px 2px;"> <label for="id_weekly-repeat_on_monday" style="display: inline-block"><?php echo $detalle_dia["alias"]; ?></label>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">Repite desde:</label>
                <div class="controls">
                    <input type="text" name="weekly-repeat_start" class="datepicker" id="id_weekly-repeat_start" value="<?php echo ($evento_repetido && $repeat_type == "semanalmente") ? (!$actualizacion) ? $arreglo_repeticiones['repeat_start'] : format_date($this->mdl_evento->form_value('fecha_inicio'))  : format_date(standardize_date(time($this->mdl_mcb_data->setting('default_date_format')))); ?>">
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">Hasta:</label>
                <div class="controls">
                    <input type="text" name="weekly-repeat_end" class="datepicker" id="id_weekly-repeat_end" value="<?php echo ($evento_repetido && $repeat_type == "semanalmente") ? (!$actualizacion) ? $arreglo_repeticiones['repeat_end'] : format_date($repeticiones[0]->repeat_end)  : format_date(standardize_date(time($this->mdl_mcb_data->setting('default_date_format')))); ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="guardar_modal_semanalmente">Guardar</button>
    </div>
</div>