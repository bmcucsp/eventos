<div id="evento_repite_content">

	<div class="control-group">

		<label class="control-label">Este evento repite:</label>

		<div class="controls">
		    </select>
		    <div class="btn-group">
		    	<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		    		Frecuencia: <span class="caret"></span>
		    	</a>
		    	<ul class="dropdown-menu frec-repeat">
		    		<li>
		    			<a style="cursor: pointer" data-value="no-repite">No repite</a>
		    			<a style="cursor: pointer" data-value="diariamente">Diariamente</a>
		    			<a style="cursor: pointer" data-value="semanalmente">Semanalmente</a>
		    		</li>
		    	</ul>
		    </div>
		    <div id="repeticiones">
		        <?php if ($evento_repetido) : ?>
		            <?php
		            if ($actualizacion):
		                switch ($repeat_type) {
		                    case "diariamente":
		                        ?>
		                        <label>Repite cada <?php echo $repeticiones[0]->repeat_interval / (24 * 60 * 60); ?> dia(s) hasta el <?php echo format_date($repeticiones[0]->repeat_end); ?></label>
		                        <a class="btn btn-mini" style="cursor: pointer" onclick="$('#modal_diariamente').modal()">Editar</a>
		                        <input type="hidden" name="repeat_type" value="diariamente" />
		                        <input type="hidden" value="<?php echo $repeticiones[0]->repeat_interval / (24 * 60 * 60); ?>" name="arreglo_repeticiones[num_dias]" />
		                        <input type="hidden" value="<?php echo format_date($this->mdl_evento->form_value('fecha_inicio')); ?>" name="arreglo_repeticiones[repeat_start]" />
		                        <input type="hidden" value="<?php echo format_date($repeticiones[0]->repeat_end); ?>" name="arreglo_repeticiones[repeat_end]" />

		                        <input type="hidden" value="<?php echo date("g", $this->mdl_evento->form_value('fecha_inicio')); ?>" name="arreglo_repeticiones[hour_start_daily]" />
		                        <input type="hidden" value="<?php echo date("i", $this->mdl_evento->form_value('fecha_inicio')); ?>" name="arreglo_repeticiones[minute_start_daily]" />
		                        <input type="hidden" value="<?php echo date("A", $this->mdl_evento->form_value('fecha_inicio')); ?>" name="arreglo_repeticiones[meridian_start_daily]" />

		                        <input type="hidden" value="<?php echo date("g", $this->mdl_evento->form_value('fecha_fin')); ?>" name="arreglo_repeticiones[hour_end_daily]" />
		                        <input type="hidden" value="<?php echo date("i", $this->mdl_evento->form_value('fecha_fin')); ?>" name="arreglo_repeticiones[minute_end_daily]" />
		                        <input type="hidden" value="<?php echo date("A", $this->mdl_evento->form_value('fecha_fin')); ?>" name="arreglo_repeticiones[meridian_end_daily]" />

		                        <?php
		                        break;

		                    case "semanalmente":
		                        ?>
		                        <label>Repite cada <?php echo $repeticiones[0]->repeat_interval / (7 * 24 * 60 * 60); ?> semana(s) hasta el <?php echo format_date($repeticiones[0]->repeat_end); ?></label>
		                        <a class="btn btn-mini" style="cursor: pointer" onclick="$('#modal_semanalmente').modal()">Editar</a>
		                        <input type="hidden" name="repeat_type" value="semanalmente" />
		                        <!--para cada uno de los dias-->
		                        <?php foreach ($repeticiones as $repeticion) : ?>
		                            <input type="hidden" name="arreglo_repeticiones[dias][]" value="<?php echo date("l", $repeticion->repeat_start); ?>" />
		                        <?php endforeach; ?>
		                        <!--para las fechas-->
		                        <input type="hidden" value="<?php echo $repeticiones[0]->repeat_interval / (7 * 24 * 60 * 60); ?>" name="arreglo_repeticiones[num_semanas]" />
		                        <input type="hidden" value="<?php echo format_date($this->mdl_evento->form_value('fecha_inicio')); ?>" name="arreglo_repeticiones[repeat_start]" />
		                        <input type="hidden" value="<?php echo format_date($repeticiones[0]->repeat_end); ?>" name="arreglo_repeticiones[repeat_end]" />

		                        <input type="hidden" value="<?php echo date("g", $this->mdl_evento->form_value('fecha_inicio')); ?>" name="arreglo_repeticiones[hour_start_weekly]" />
		                        <input type="hidden" value="<?php echo date("i", $this->mdl_evento->form_value('fecha_inicio')); ?>" name="arreglo_repeticiones[minute_start_weekly]" />
		                        <input type="hidden" value="<?php echo date("A", $this->mdl_evento->form_value('fecha_inicio')); ?>" name="arreglo_repeticiones[meridian_start_weekly]" />

		                        <input type="hidden" value="<?php echo date("g", $this->mdl_evento->form_value('fecha_fin')); ?>" name="arreglo_repeticiones[hour_end_weekly]" />
		                        <input type="hidden" value="<?php echo date("i", $this->mdl_evento->form_value('fecha_fin')); ?>" name="arreglo_repeticiones[minute_end_weekly]" />
		                        <input type="hidden" value="<?php echo date("A", $this->mdl_evento->form_value('fecha_fin')); ?>" name="arreglo_repeticiones[meridian_end_weekly]" />

		                        <?php
		                        break;

		                    default:
		                        break;
		                }
		            else : /* viene de un formulario con errores entonces cargar los items que existan para que no se pierdan debido al error */
		                switch ($repeat_type) {
		                    case "diariamente":
		                        ?>
		                        <?php if (isset($arreglo_repeticiones['num_dias']) && $arreglo_repeticiones['num_dias'] != '') : ?>
		                            <label>Repite cada <?php echo $arreglo_repeticiones['num_dias']; ?> dia(s) hasta el <?php echo $arreglo_repeticiones['repeat_end']; ?></label>
		                        <?php endif; ?>
		                        <a class="btn btn-mini" style="cursor: pointer" onclick="$('#modal_diariamente').modal()">Editar</a>
		                        <input type="hidden" name="repeat_type" value="diariamente" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['num_dias']) && $arreglo_repeticiones['num_dias'] != '') ? $arreglo_repeticiones['num_dias'] : ''; ?>" name="arreglo_repeticiones[num_dias]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['repeat_start']) && $arreglo_repeticiones['repeat_start'] != '') ? $arreglo_repeticiones['repeat_start'] : ''; ?>" name="arreglo_repeticiones[repeat_start]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['repeat_end']) && $arreglo_repeticiones['repeat_end'] != '') ? $arreglo_repeticiones['repeat_end'] : ''; ?>" name="arreglo_repeticiones[repeat_end]" />

		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['hour_start_daily']) && $arreglo_repeticiones['hour_start_daily'] != '') ? $arreglo_repeticiones['hour_start_daily'] : ''; ?>" name="arreglo_repeticiones[hour_start_daily]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['minute_start_daily']) && $arreglo_repeticiones['minute_start_daily'] != '') ? $arreglo_repeticiones['minute_start_daily'] : ''; ?>" name="arreglo_repeticiones[minute_start_daily]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['meridian_start_daily']) && $arreglo_repeticiones['meridian_start_daily'] != '') ? $arreglo_repeticiones['meridian_start_daily'] : ''; ?>" name="arreglo_repeticiones[meridian_start_daily]" />

		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['minute_end_daily']) && $arreglo_repeticiones['minute_end_daily'] != '') ? $arreglo_repeticiones['minute_end_daily'] : ''; ?>" name="arreglo_repeticiones[minute_end_daily]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['meridian_end_daily']) && $arreglo_repeticiones['meridian_end_daily'] != '') ? $arreglo_repeticiones['meridian_end_daily'] : ''; ?>" name="arreglo_repeticiones[meridian_end_daily]" />

		                        <?php
		                        break;

		                    case "semanalmente":
		                        ?>
		                        <?php if (isset($arreglo_repeticiones['num_semanas']) && $arreglo_repeticiones['num_semanas'] != ''): ?>
		                            <label>Repite cada <?php echo $arreglo_repeticiones['num_semanas']; ?> semana(s) hasta el <?php echo $arreglo_repeticiones['repeat_end']; ?></label>
		                        <?php endif; ?>
		                        <a class="btn btn-mini" style="cursor: pointer" onclick="$('#modal_semanalmente').modal()">Editar</a>
		                        <input type="hidden" name="repeat_type" value="semanalmente" />
		                        <!--para cada uno de los dias-->
		                        <?php
		                        if (isset($arreglo_repeticiones['dias'])) :
		                            foreach ($arreglo_repeticiones['dias'] as $dia) :
		                                ?>
		                                <input type="hidden" name="arreglo_repeticiones[dias][]" value="<?php echo $dia; ?>" />
		                                <?php
		                            endforeach;
		                        endif;
		                        ?>
		                        <!--para las fechas-->
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['num_semanas']) && ($arreglo_repeticiones['num_semanas'] != '')) ? $arreglo_repeticiones['num_semanas'] : ''; ?>" name="arreglo_repeticiones[num_semanas]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['repeat_start']) && ($arreglo_repeticiones['repeat_start'] != '')) ? $arreglo_repeticiones['repeat_start'] : ''; ?>" name="arreglo_repeticiones[repeat_start]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['repeat_end']) && ($arreglo_repeticiones['repeat_end'] != '')) ? $arreglo_repeticiones['repeat_end'] : ''; ?>" name="arreglo_repeticiones[repeat_end]" />

		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['hour_start_weekly']) && ($arreglo_repeticiones['hour_start_weekly'] != '')) ? $arreglo_repeticiones['hour_start_weekly'] : ''; ?>" name="arreglo_repeticiones[hour_start_weekly]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['minute_start_weekly']) && ($arreglo_repeticiones['minute_start_weekly'] != '')) ? $arreglo_repeticiones['minute_start_weekly'] : ''; ?>" name="arreglo_repeticiones[minute_start_weekly]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['meridian_start_weekly']) && ($arreglo_repeticiones['meridian_start_weekly'] != '')) ? $arreglo_repeticiones['meridian_start_weekly'] : ''; ?>" name="arreglo_repeticiones[meridian_start_weekly]" />

		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['hour_end_weekly']) && ($arreglo_repeticiones['hour_end_weekly'] != '')) ? $arreglo_repeticiones['hour_end_weekly'] : ''; ?>" name="arreglo_repeticiones[hour_end_weekly]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['minute_end_weekly']) && ($arreglo_repeticiones['minute_end_weekly'] != '')) ? $arreglo_repeticiones['minute_end_weekly'] : ''; ?>" name="arreglo_repeticiones[minute_end_weekly]" />
		                        <input type="hidden" value="<?php echo (isset($arreglo_repeticiones['meridian_end_weekly']) && ($arreglo_repeticiones['meridian_end_weekly'] != '')) ? $arreglo_repeticiones['meridian_end_weekly'] : ''; ?>" name="arreglo_repeticiones[meridian_end_weekly]" />

		                        <?php
		                        break;

		                    default:
		                        break;
		                } endif; ?>
		            <?php endif; ?>
		    </div>

		</div>

	</div>

</div>