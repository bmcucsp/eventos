<div class="modal hide fade" id="modal_diariamente">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>¿Cuándo repite este evento?</h3>
    </div>
    <div class="modal-body">
        <?php
        if ($evento_repetido && $repeat_type == "diariamente") {
            if ($actualizacion) {
                $interval_days = $repeticiones[0]->repeat_interval / (24 * 60 * 60);
                $hour_start_daily = date("g", $this->mdl_evento->form_value('fecha_inicio'));
                $minute_start_daily = date("i", $this->mdl_evento->form_value('fecha_inicio'));
                $meridian_start_daily = date("A", $this->mdl_evento->form_value('fecha_inicio'));
                $hour_end_daily = date("g", $this->mdl_evento->form_value('fecha_fin'));
                $minute_end_daily = date("i", $this->mdl_evento->form_value('fecha_fin'));
                $meridian_end_daily = date("A", $this->mdl_evento->form_value('fecha_fin'));
            }
            else {
                $interval_days = (isset($arreglo_repeticiones['num_dias']) && $arreglo_repeticiones['num_dias'] != '')  ? $arreglo_repeticiones['num_dias'] : '';
                $hour_start_daily =  (isset($arreglo_repeticiones['hour_start_daily']) && $arreglo_repeticiones['hour_start_daily'] != '') ? $arreglo_repeticiones['hour_start_daily'] : 0;
                $minute_start_daily =  (isset($arreglo_repeticiones['minute_start_daily']) && $arreglo_repeticiones['minute_start_daily'] != '') ? $arreglo_repeticiones['minute_start_daily'] : 0;
                $meridian_start_daily =  (isset($arreglo_repeticiones['meridian_start_daily']) && $arreglo_repeticiones['meridian_start_daily'] != '') ? $arreglo_repeticiones['meridian_start_daily'] : 'AM';
                $hour_end_daily =  (isset($arreglo_repeticiones['hour_end_daily']) && $arreglo_repeticiones['hour_end_daily'] != '') ? $arreglo_repeticiones['hour_end_daily'] : 0;
                $minute_end_daily =  (isset($arreglo_repeticiones['minute_end_daily']) && $arreglo_repeticiones['minute_end_daily'] != '') ? $arreglo_repeticiones['minute_end_daily'] : 0;
                $meridian_end_daily =  (isset($arreglo_repeticiones['meridian_end_daily']) && $arreglo_repeticiones['meridian_end_daily'] != '') ? $arreglo_repeticiones['meridian_end_daily'] : 'AM';
            }
        }
        ?>
        <div class="left">Repetir cada:
            <select id="id_daily-repeat_interval" name="daily-repeat_interval">
                <?php for ($i = 1; $i <= 31; $i++) { ?>
                    <option value="<?php echo $i ?>" <?php if (isset($interval_days) && $interval_days == $i) : ?> selected ="selected" <?php endif; ?> ><?php echo $i ?></option>
                    <?php
                }
                ?>
            </select> dia(s)
        </div>
        <div style="clear: both"></div>
        <div class="left">
            Hora de inicio:
            <select name="hour_start_daily" id="hour_start_daily">
                <?php for ($i = 1; $i <= 12; $i++) { ?>
                    <option value="<?php echo $i; ?>" <?php if (isset($hour_start_daily) && $hour_start_daily == $i) : ?> selected="selected" <?php endif; ?> ><?php echo $i; ?></option>
                <?php } ?>
            </select>
            <span class="separator">:</span>
            <select name="minute_start_daily" id="minute_start_daily">
                <?php for ($i = 0; $i < 12; $i++) { ?>
                    <option value="<?php echo sprintf('%02u', $i * 5); ?>" <?php if (isset($minute_start_daily) && $minute_start_daily == sprintf('%02u', $i * 5)) : ?> selected="selected" <?php endif; ?> ><?php echo sprintf('%02u', $i * 5); ?></option>
                <?php } ?>
            </select>
            <select name="meridian_start_daily" id="meridian_start_daily">
                <option value="AM" <?php if (isset($meridian_start_daily) && $meridian_start_daily == "AM") : ?> selected="selected" <?php endif; ?> >AM</option>
                <option value="PM" <?php if (isset($meridian_start_daily) && $meridian_start_daily == "PM") : ?> selected="selected" <?php endif; ?> >PM</option>
            </select>
        </div>
        <div class="left" style="clear: both">
            Hora de Fin:
            <select name="hour_end_daily" id="hour_end_daily">
                <?php for ($i = 1; $i <= 12; $i++) { ?>
                    <option value="<?php echo $i; ?>" <?php if (isset($hour_end_daily) && $hour_end_daily == $i) : ?> selected="selected" <?php endif; ?> ><?php echo $i; ?></option>
                <?php } ?>
            </select>
            <span class="separator">:</span>
            <select name="minute_end_daily" id="minute_end_daily">
                <?php for ($i = 0; $i < 12; $i++) { ?>
                    <option value="<?php echo sprintf('%02u', $i * 5); ?>" <?php if (isset($minute_end_daily) && $minute_end_daily == sprintf('%02u', $i * 5)) : ?> selected="selected" <?php endif; ?> ><?php echo sprintf('%02u', $i * 5); ?></option>
                <?php } ?>
            </select>
            <select name="meridian_end_daily" id="meridian_end_daily">
                <option value="AM" <?php if (isset($meridian_end_daily) && $meridian_end_daily == "AM") : ?> selected="selected" <?php endif; ?> >AM</option>
                <option value="PM" <?php if (isset($meridian_end_daily) && $meridian_end_daily == "PM") : ?> selected="selected" <?php endif; ?> >PM</option>
            </select>
        </div>
        <div style="clear: both"></div>
        <div class="left">Repite desde:
            <input name="daily-repeat_start" id="id_daily-repeat_start" type="text" class="datepicker" value="<?php echo (($evento_repetido) && ($repeat_type == "diariamente")) ? (!$actualizacion) ? $arreglo_repeticiones['repeat_start'] : format_date($this->mdl_evento->form_value('fecha_inicio')) : format_date(standardize_date(time($this->mdl_mcb_data->setting('default_date_format')))); ?>">
        </div>
        <div class="left">Hasta:
            <input name="daily-repeat_end" id="id_daily-repeat_end" type="text" class="datepicker" value="<?php echo (($evento_repetido) && ($repeat_type == "diariamente")) ? (!$actualizacion) ? $arreglo_repeticiones['repeat_end'] : format_date($repeticiones[0]->repeat_end) : format_date(standardize_date(time($this->mdl_mcb_data->setting('default_date_format')))); ?>">
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="guardar_modal_diariamente">Guardar</button>
    </div>
</div>