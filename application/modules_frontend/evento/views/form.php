<div class="row-fluid">

	<div class="span3">

		<ul class="nav nav-list bs-docs-sidenav" data-spy="affix" data-offset-top="200">
			<li><a href="#general"><i class="icon-chevron-right"></i>Datos generales</a></li>
			<li><a href="#programacion"><i class="icon-chevron-right"></i>Programaci&oacute;n de fechas</a></li>
			<li><a href="#ubicacion"><i class="icon-chevron-right"></i>Datos de ubicaci&oacute;n</a></li>
			<li><a href="#adicional"><i class="icon-chevron-right"></i>Datos adicionales</a></li>

			<div id="action_buttons_sidebar">
				<?php
					if ($evento_id != null) $message_button = "update_event";
					else $message_button = "create_event";
				?>
				<input type="submit" class="btn_submit btn btn-success" name="btn_submit" value="<?php echo $this->lang->line($message_button); ?>" />
			</div>

			<?php if (validation_errors()) : ?>
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<span>Complete los campos obligatorios marcados con (*)</span>

				<?php if (form_error('fecha_fin') != '') : ?>
					<ul>
						<li><?php echo form_error('fecha_fin'); ?></li>
					</ul>
				<?php endif; ?>
				<?php if ($evento_repetido && validation_errors() != '') : ?>
					<ul><?php echo validation_errors('<li>', '</li>'); ?></ul>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</ul>

	</div><!-- span3 -->

	<div class="span9 evento-add-form">
		<h3>Consideraciones para publicar un nuevo evento:</h3>
		<ul>
			<li>Los campos marcados con el signo (*) son <b>obligatorios</b>.</li>
			<li>Sea extensivo en sus descripciones, eso ayudar&aacute; a los asistentes de su evento.</li>
		</ul>

		<form class="form-horizontal" method="post" action="<?php echo site_url($this->uri->uri_string()); ?>" id="evento">

			<section id="general">
			<div class="well">

				<div class="row-fluid">

					<div class="span12">
						<legend>Datos Generales</legend>
					</div>
				</div>

				<div class="row-fluid">

					<div class="span12">

						<?php
						$err_nombre = form_error('nombre');
						$err_categoria_id = form_error('categoria_id');
						$err_organizador = form_error('organizador');
						$err_descripcion = form_error('descripcion');
						$err_fecha_inicio = form_error('fecha_inicio');
						$err_fecha_fin = form_error('fecha_fin');
						$err_pais_id = form_error('pais_id');
						$err_ciudad_id = form_error('ciudad_id');
						$err_lugar = form_error('lugar');
						$err_ubicacion = form_error('ubicacion');
						$err_publico = form_error('publico');
						$err_presencial = form_error('presencial');
						?>

						<div class="control-group <?php echo $err_nombre != ''? 'error' : '' ?>">
							<label class="control-label">* T&iacute;tulo</label>
							<div class="controls">
								<input type="text" class="input-block-level" name="nombre" value="<?php echo $this->mdl_evento->form_value('nombre'); ?>" />
								<?php echo form_error('nombre'); ?>
							</div>
						</div>

					</div>

				</div>

				<div class="row-fluid">

					<div class="span7">

						<div class="control-group <?php echo $err_categoria_id != '' ? 'error' : '' ?>">
							<label class="control-label">* Categor&iacute;a</label>
							<div class="controls">
								<select name="categoria_id">
									<?php
									$categoria_id = $this->mdl_evento->form_value('categoria_id');
									?>
									<option value="" <?php echo ($categoria_id == '') ? 'selected="selected"' : '';?> >
										--
									</option>
									<?php foreach ($categorias as $categoria) : ?>
									<option value="<?php echo $categoria->categoria_id; ?>"
									<?php if ($categoria->categoria_id == $categoria_id) : ?>
										selected="selected" <?php endif; ?>>
										<?php echo $categoria->nombre; ?>
									</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Ingreso</label>
							<div class="controls">
								<?php
								$detalle = $this->mdl_evento->form_value('detalle_costo');
								if ($detalle == 'gratuito')
									$detalle = "";
								?>
								<select name="condicion" id="combo_condicion">
									<option value="gratuito" <?php if ($detalle == "") : ?> selected="selected" <?php endif; ?>>Gratuito</option>
									<option value="previo_pago" <?php if ($detalle != "") : ?> selected="selected" <?php endif; ?>>Previo pago de entrada</option>
								</select>
								<a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="Especif&iacute;que si los asistentes deben de pagar para asistir al evento" data-original-title="Evento gratuito o de pago?">?</a>

								<div id="costo">
									<textarea name="detalle_costo" id="detalle_costo" placeholder="Ingrese aqu&iacute; el costo de las entradas"><?php echo $detalle; ?></textarea>
								</div>
							</div>

						</div>

					</div>

					<div class="span5">
						<label>
							Imagen<br/>(formatos JPG, JPEG, PNG o GIF)
							<a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="bottom" data-content="Agrega una imagen referencial (puedes arrastrar y soltar) para identificar publicamente el evento, puedes incluir un logo o afiche" data-original-title="Imagen referencial del evento">?</a>
						</label>
						<div>
							<?php
							if ($this->mdl_evento->form_value('img') && file_exists(base_path_foto_evento() . $this->mdl_evento->form_value('img').'_standard.jpg')) {
		                    	$src = base_url_foto_evento() . $this->mdl_evento->form_value('img').'_standard.jpg';
		                	} else {
		                		if (file_exists(base_path_foto_temp(). $this->mdl_evento->form_value('img').'_standard.jpg')) {
		                    		$src = base_url_foto_temp() . $this->mdl_evento->form_value('img').'_standard.jpg';
		                		}
		                    	else {
		                    		$src = base_url_foto_evento() . 'anonimo.jpg';
		                    	}
		                	}
		                	?>
							<input type="hidden" name="img" id="img" value="<?php echo $this->mdl_evento->form_value('img');?>" />
							<img src="<?= $src ?>" id="foto-evento" />
							<!--<input type="file" id="filename" name="filename" />-->
							<div id="file-uploader"></div>
							<div class="file-upload-message"></div>
						</div>

					</div>

				</div>

				<div class="row-fluid">
					<div class="span12">
						<div class="control-group <?php echo $err_descripcion != '' ? 'error' : '' ?>">
							<label>* Descripci&oacute;n del evento <a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="Describe el programa del evento o agrega un itinerario si lo tuviera" data-original-title="Descripci&oacute;n del evento">?</a></label>
							<div>
								<textarea name="descripcion" id="descripcion">
									<?php echo $this->mdl_evento->form_value('descripcion'); ?>
								</textarea>
							</div>
						</div>
					</div>
				</div>

				<div class="control-group <?php echo $err_organizador != '' ? 'error' : '' ?>">

					<label class="control-label">* Organizador<br/>(persona o empresa)</label>
					<div class="controls">
						<input class="input-block-level" type="text" name="organizador" value="<?php echo $this->mdl_evento->form_value('organizador'); ?>" /> <?php echo form_error('organizador'); ?>

						<label>
							<?php $detalle = $this->mdl_evento->form_value('detalle_organizador'); ?>
							Agregar informaci&oacute;n adicional del organizador?
							<input type="checkbox" name="has_detail_organizer" id="has_detail_organizer" <?php if ($detalle != "") : ?> checked="checked" <?php endif; ?> />
						</label>

						<div id="organizer_detail">
							<div class="control-group">
								<div>
									<textarea name="detalle_organizador" id="detalle_organizador">
										<?php echo $this->mdl_evento->form_value('detalle_organizador'); ?>
									</textarea>
								</div>
							</div>
						</div>

					</div>

				</div>

			</div>
			</section><!-- general -->

			<section id="programacion">
			<div class="well">

				<legend>Programaci&oacute;n de fechas</legend>

				<div class="control-group">
					<label class="control-label">Frecuencia del evento</label>
					<div class="controls">
						<select name="select_repite" id="select_repite">
							<?php
								$select_repite = $this->mdl_evento->form_value('select_repite');
								if ($select_repite == '')
								if (isset($repeat_type)) $select_repite = "evento_repite";
								else $select_repite = "evento_unico";
							?>
							<option value="evento_unico" <?php if ($select_repite == "evento_unico") : ?> selected="selected" <?php endif; ?>>s&oacute;lo una vez</option>
							<option value="evento_repite" <?php if ($select_repite == "evento_repite") : ?> selected="selected" <?php endif; ?>>varias veces</option>
						</select>
						<a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="Selecciona si el evento se repite por varios d&iacute;as o semanas" data-original-title="Frecuencia del evento">?</a>
						<?php echo form_error('repeat_type'); ?>
					</div>
				</div>

				<?php $this->load->view("modals/hidden_repetidos"); ?>

				<div id="evento_unico_content">
					<div class="control-group <?php echo $err_fecha_inicio != '' ? 'error' : '' ?>">
						<label class="control-label">* Fecha de inicio</label>
						<div class="controls">
							<input type="text" name="fecha_inicio" class="datepicker" id="fecha_inicio" value="<?php echo format_date($this->mdl_evento->form_value('fecha_inicio')); ?>" />
							<?php
								$time = $this->mdl_evento->form_value('fecha_inicio');

								if (empty($time))
									$time = 0;

								$fecha = date("H:i", $time);

								list($hours, $minutes) = explode(':', $fecha);

								$meridiano = "AM";

								if ($hours >= 12) {
			                        $meridiano = "PM";
			                        $hours -= 12;
			                    }
			                    $select_hour = "";
			                    for ($i = 1; $i <= 12; $i++) {
			                        $selected = ($hours == $i % 12) ? ' selected = "selected"' : "";
			                        $select_hour .= "<option value=" . ($i % 12) . $selected . ">" . $i . "</option>\n";
			                    }
			                    $select_minute = "";
			                    for ($i = 0; $i < 12; $i++) {
			                        $selected = ($minutes == ($i * 5)) ? ' selected = "selected"' : "";
			                        $select_minute .= "<option value=" . sprintf('%02u', $i * 5) . $selected . ">" . sprintf('%02u', $i * 5) . "</option>\n";
			                    }
			                    ?>
							<select name="hour_start" id="hour_start">
								<?php echo $select_hour; ?>
							</select>
							<span class="separator">:</span>
							<select name="minute_start" id="minute_start">
								<?php echo $select_minute; ?>
							</select>
							<select name="meridian_start" id="meridian_start">
								<option value="AM" <?php if ($meridiano == "AM") : ?> selected="selected" <?php endif; ?>>AM</option>
								<option value="PM" <?php if ($meridiano == "PM") : ?> selected="selected" <?php endif; ?>>PM</option>
							</select>
							<a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="¿Cu&aacute;ndo y a qu&eacute; hora empieza tu evento?" data-original-title="Fecha de inicio del evento">?</a>
						</div>

					</div>

					<div id="fecha_final" class="control-group <?php echo $err_fecha_fin != '' ? 'error' : ''; ?>">
						<label class="control-label">* Fecha final </label>
						<div class="controls">
							<input type="text" name="fecha_fin" class="datepicker" id="fecha_fin" value="<?php echo format_date($this->mdl_evento->form_value('fecha_fin')); ?>" />
							<?php
								$time = $this->mdl_evento->form_value('fecha_fin');

								if (empty($time))
									$time = 0;

								$fecha = date("H:i", $time);

								list($hours, $minutes) = explode(':', $fecha);

								$meridiano = "AM";

								if ($hours >= 12) {
		                        $meridiano = "PM";
		                        $hours -= 12;
		                    }
		                    $select_hour = "";
		                    for ($i = 1; $i <= 12; $i++) {
		                        $selected = ($hours == $i % 12) ? ' selected = "selected"' : "";
		                        $select_hour .= "<option value=" . ($i % 12) . $selected . ">" . $i . "</option>\n";
		                    }
		                    $select_minute = "";
		                    for ($i = 0; $i < 12; $i++) {
		                        $selected = ($minutes == ($i * 5)) ? ' selected = "selected"' : "";
		                        $select_minute .= "<option value=" . sprintf('%02u', $i * 5) . $selected . ">" . sprintf('%02u', $i * 5) . "</option>\n";
		                    }
		                    ?>
							<select name="hour_end" id="hour_end">
								<?php echo $select_hour; ?>
							</select>
							<span class="separator">:</span>
							<select name="minute_end" id="minute_end">
								<?php echo $select_minute; ?>
							</select>
							<select name="meridian_end" id="meridian_end">
								<option value="AM" <?php if ($meridiano == "AM") : ?> selected="selected" <?php endif; ?>>AM</option>
								<option value="PM" <?php if ($meridiano == "PM") : ?> selected="selected" <?php endif; ?>>PM</option>
							</select>
							<a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="¿Cu&aacute;ndo y a qu&eacute; hora termina tu evento?" data-original-title="Fecha de fin del evento">?</a>
						</div>

					</div>

				</div><!-- evento-unico -->

				<div id="evento-repetitivo">
					<?php $this->load->view("modals/modal_diariamente"); ?>
					<?php $this->load->view("modals/modal_semanalmente"); ?>
				</div>

			</div>
			</section><!-- programacion -->

			<section id="ubicacion">
			<div class="well">

				<legend>Datos de Ubicaci&oacute;n</legend>

				<div class="control-group <?php echo $err_pais_id != '' ? 'error' : ''; ?>">
					<label class="control-label">* Pa&iacute;s:</label>
					<div class="controls">
						<select name="pais_id" name="pais_id" id="pais_id">
							<option value="">Seleccione Pa&iacute;s:</option>
							<?php foreach ($paises as $pais) : ?>
							<option value="<?php echo $pais->pais_id; ?>"
							<?php if ($pais_id == $pais->pais_id) : ?> selected="selected"
							<?php endif; ?>>
								<?php echo $pais->nombre; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>

				</div>

				<div class="control-group <?php echo $err_ciudad_id != '' ? 'error' : ''; ?>">
					<label class="control-label">* Ciudad o Estado:</label>
					<div class="controls">
						<select name="ciudad_id" id="ciudad_id">
							<option value="">Seleccione un pais primero</option>
							<?php
							if (isset($ciudades))
								foreach ($ciudades as $ciudad) :
								?>
							<option value="<?php echo $ciudad->ciudad_id; ?>"
							<?php if ($ciudad_id == $ciudad->ciudad_id) : ?>
								selected="selected" <?php endif; ?>>
								<?php echo $ciudad->nombre; ?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="control-group <?php echo $err_lugar != '' ? 'error' : ''; ?>">
					<label class="control-label">* Recinto</label>
					<div class="controls">
						<input type="text" name="lugar" value="<?php echo $this->mdl_evento->form_value('lugar'); ?>" />
						<?php echo form_error('lugar'); ?>
						<a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="Nombre del recinto, local o establecimiento donde se llevar&aacute; a cabo el evento" data-original-title="Nombre del recinto">?</a>
					</div>
				</div>

				<div class="control-group <?php echo $err_ubicacion != '' ? 'error' : ''; ?>">
					<label class="control-label">* Direcci&oacute;n</label>
					<div class="controls">
						<input type="text" name="ubicacion" value="<?php echo $this->mdl_evento->form_value('ubicacion'); ?>" />
						<a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="Ingresa la direcci&oacute;n del local. Agrega referencias que faciliten su ubicaci&oacute;n " data-original-title="Ubicaci&oacute;n / Direcci&oacute;n">?</a>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label">Agregar un mapa?</label>
					<div class="controls">
						<?php
							  $referencia = $this->mdl_evento->form_value('referencia');
							  $set_cords = $this->mdl_evento->form_value('latitud') != '' && $this->mdl_evento->form_value('longitud') != '';
							  if (!$set_cords) $referencia = "";
						?>
						<input type="checkbox" name="agregar_ubicacion_referencial" id="agregar_ubicacion_referencial" <?php if ($set_cords) : ?> checked="checked" <?php endif; ?> />
					</div>
				</div>

				<div class="control-group ubicacion_referencial">
					<label class="control-label">Ubicaci&oacute;n referencial</label>
					<div class="controls">
						<input id="searchTextField" class="referencia" name="referencia" type="text" size="50" value="<?php echo $referencia; ?>">
						<input type="hidden" name="latitud" id="latitud" value="<?php echo $this->mdl_evento->form_value('latitud'); ?>" />
						<input type="hidden" name="longitud" id="longitud" value="<?php echo $this->mdl_evento->form_value('longitud'); ?>" />
						<a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="Ingresa una ubicación referencial (conocida) y a partir de ella selecciona el lugar exacto del evento (clickeando en el mapa)" data-original-title="Ubicaci&oacute;n referencial">?</a>
						<div id="map_canvas"></div>
					</div>
				</div>

			</div>
			</section><!-- ubicacion -->

			<section id="adicional">
			<div class="well">

				<legend>Datos adicionales</legend>

				<div class="control-group <?php echo $err_publico != '' ? 'error' : ''; ?>">
					<label class="control-label">Restricci&oacute;n</label>
					<div class="controls">
						<?php
							$val = $this->mdl_evento->form_value('publico');
							if ($val == '') $val = 1;
						?>

						<label class="radio">
							<input type="radio" name="publico" value="1" <?php if ($val == 1) : ?> checked <?php endif; ?> />
							Evento p&uacute;blico
						</label>

						<label class="radio">
							<input type="radio" name="publico" value="0" <?php if ($val == 0) : ?> checked <?php endif; ?> />
							Evento privado <a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="Los asistentes a los eventos <strong>privados</strong> deben previamente ser aprobados por el organizador" data-original-title="Eventos privados">?</a>
						</label>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label">Dirigido a: </label>
					<div class="controls">
						<input type="text" name="dirigido_a" value="<?php echo $this->mdl_evento->form_value('dirigido_a'); ?>" placeholder="P&uacute;blico en general" />
					</div>
				</div>

				<div class="control-group">
					<label class="control-label">Capacidad para</label>
					<div class="controls">
						<input type="text" name="capacidad" value="<?php echo $this->mdl_evento->form_value('capacidad'); ?>" />
						asistentes <a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="Escribe la <strong>cantidad</strong> aproximada de asistentes al evento. (ejm: 5000)" data-original-title="Capacidad del recinto">?</a>
					</div>
				</div>

				<div class="control-group <?php echo $err_presencial != '' ? 'error' : ''; ?>">
					<label class="control-label">Asistencia</label>
					<div class="controls">
						<?php
							$val = $this->mdl_evento->form_value('presencial');
							if ($val == '') $val = 1;
						?>

						<label class="radio">
							<input type="radio" name="presencial" value="1" <?php if ($val == 1) : ?> checked <?php endif; ?> />
							Presencial
						</label>

						<label class="radio">
							<input type="radio" name="presencial" value="0" <?php if ($val == 0) : ?> checked <?php endif; ?> />
							Por Internet <a href="#" class="btn btn-info btn-mini" rel="popover" data-placement="right" data-content="Indica que el evento se transmitir&aacute; v&iacute;a Internet" data-original-title="Evento por Internet">?</a>
						</label>

					</div>
				</div>

			</div>
			</section><!-- adicional -->
		</form>
		<div id="action_buttons_form">
			<button type="submit" class="btn_submit btn btn-success" name="btn_submit"><?php echo $this->lang->line($message_button); ?></button>
		</div>


	</div><!-- span9 -->

</div><!-- row-fluid -->