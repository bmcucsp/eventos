<style>
    .search-option-header-wrap {
        /* line-height: 53px; */
        height: 53px;
        border-right: 1px solid #DEDEDE;
        position: relative;
    }
    .search-option-header-wrap:hover {
        background: #eee;
    }
    .search-option-header-wrap::after {
        background-image: url("http://assets.behance.net/img/site/arrows.png?cb=864532520");
        background-position: -11px -511px;
        width: 5px;
        height: 9px;
        display: block;
        content: "";
        position: absolute;
        right: 15px;
        top: 23px;
    }

    .search-option {
        position: relative;
        cursor: pointer;
    }
    #search-option-field .search-option-header-wrap {
        min-width: 180px;
    }

    #bloque-calendar, #bloque-location, #bloque-categorias{
        display: none; position: absolute; top : 50px; left:0px;
    }
    .bloque{
        padding:15px;
        background:#eee;
        -moz-box-shadow:    1px 7px 10px rgba(0, 0, 0, 0.2);
        -webkit-box-shadow: 1px 7px 10px rgba(0, 0, 0, 0.2);
        box-shadow:         1px 7px 10px rgba(0, 0, 0, 0.2);
        z-index:100;
        text-align:center;
    }
    .search-option-widget-item{text-align:left;}
    #id-categorias{
        text-align: center;
        vertical-align: middle;
        padding: 16px;
        font-size: 15px;
        font-weight: bold;
    }

    #search-option-sort .search-option-header-wrap,
    #search-option-time .search-option-header-wrap {
        /* width: 112px;
        max-width: 172px; */
        /*border: 1px solid;*/
    }
    #search-option-sort .search-option-header-wrap .search-option-header,
    #search-option-time .search-option-header-wrap .search-option-header
    {
        min-width: 160px;
    }

    #search-option-header-icon-time {
        width: 21px;
        height: 23px;
        background-position: -77px -150px;
        top: 16px;
        left: 35px;
    }

    #search-option-location .search-option-header-wrap {
        /* width: 96px;
        max-width: 96px; */
    }

    #search-option-location .search-option-header-wrap .search-option-header {
        max-width: 160px;
    }

    #search-option-header-icon-location {
        width: 45px;
        height: 30px;
        background-position: -100px -150px;
        top: 12px;
        left: 18px;
    }

    #search-option-user_tags .search-option-header-wrap {
        width: 76px;
        max-width: 76px;
    }
    #search-option-user_tags .search-option-header-wrap .search-option-header {
        max-width: 160px;
    }
    #search-option-header-icon-user_tags {
        width: 20px;
        height: 19px;
        background-position: -147px -150px;
        top: 19px;
        left: 21px;
    }
    /* #search-option-search .search-option-header-wrap {
        width: 358px;
    } */

    #search-option-search .search-option-header-wrap .search-option-header {
        max-width: 160px;
    }

    .search-option .form-item-text {
        /* width: 310px; */
        text-align: center;
    }


    .search-option-header-icon,
    #search-option-content .search-option-link-icon,
    #search-option-sort .search-option-link-icon,
    .search-option-header-icon-item_list {
        background-image: url("http://assets.behance.net/img/network/site/sprite.png?cb=864532520");
        background-repeat: no-repeat;
        text-indent: -9999px;
        position: absolute;
    }
    .search-option-body-wrap, .search-option-body-wrap:hover {
        /* border-radius: 0 0 5px 5px !important;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(40%, white), color-stop(100%, #f3f3f3));
        background: -webkit-linear-gradient(top, white 40%, #f3f3f3 100%);
        background: -moz-linear-gradient(top, white 40%, #f3f3f3 100%);
        background: linear-gradient(top, #ffffff 40%, #f3f3f3 100%);
        border-top: 0 !important;
        z-index: 30 !important;
        cursor: default !important; */
        padding-top:10px;
    }
    #search-option-location .search-option-body-wrap, #search-option-user_tags .search-option-body-wrap {
        width: 290px !important;
        padding: 0 !important;
    }
    #search-option-location .search-option-body-wrap .search-option-body, #search-option-user_tags .search-option-body-wrap .search-option-body {
        margin: 8px 20px !important;
    }
    .search-option-widget-confirmation {
        /* background: #EAEAEA !important;
        background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#F6F6F6), to(#DFDFDF)) !important;
        background: -webkit-linear-gradient(0% 0%, 0% 100%, from(#F6F6F6), to(#DFDFDF)) !important;
        background: -moz-linear-gradient(center top, #F6F6F6, #DFDFDF) !important;
        background: linear-gradient(#F6F6F6, #DFDFDF) !important;
        -webkit-box-shadow: 0 1px 0 0 white inset !important;
        -moz-box-shadow: 0 1px 0 0 white inset !important;
        box-shadow: 0 1px 0 0 white inset !important;
        border-top: 1px solid #D1D1D1 !important;
        padding: 10px 0 !important;
        margin-top: 10px !important; */
    }
    .search-option-widget-confirmation .searchmenui-clear,
    .search-option-widget-confirmation .searchmenui-cancel {
        position: relative;
        height: 25px;
        margin-right: 20px;
        line-height: 25px;
        font-size: 11px;
        color: #4C4C4C;
    }
    .search-option-widget-confirmation .left {
        margin-left: 20px;
    }
    .search-option-widget-item .widget-header {
        padding: 10px 0 4px !important;
        margin-bottom: 18px !important;
        border-bottom: 1px solid #E3E3E3 !important;
    }
</style>
<div class="navbar">
    <div class="navbar-inner">
      <div class="container">
        <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <div class="nav-collapse">
            <ul class="nav">

                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#" onclick="return false;">
                    <i class="icon-group icon-large"></i>
                    <span class="Open-Sans-normal-300"> Categorias </span>
                    <b class="caret"></b>
                  </a>
                  <ul class="dropdown-menu">
                    <?php foreach ($this->milib->categorias_con_eventos(false) as $categoria) : ?>
                    <li><?php echo anchor_filter('c_' . $categoria->url, $categoria->nombre); ?></li>
                    <?php endforeach; ?>
                  </ul>
                </li>

                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" onclick="return false;">
                        <i class="icon-calendar icon-large"></i>
                        <span class="Open-Sans-normal-300">Calendario</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li style="padding: 15px;">
                            <div id="calendario_mes">
                                <div><?php echo Modules::run("evento/calendariomes", true); ?></div>
                            </div>
                            <div>
                                <?php echo anchor("eventos/calendario", "ver detallado", array('style' => 'color: white; width: 50%; margin: 0px auto;', 'class' => 'btn btn-info')); ?>
                            </div>
                            <!-- .location-widget -->
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" onclick="return false;">
                        <span>&nbsp;&nbsp;</span>
                        <b></b>
                    </a>
                </li>
                <li style="position: relative;" class="dropdown">
                    <div class="icon-location dropdown-toggle" style="width: 45px;position: absolute;top: 5px;left: -32px;"></div>
                    <a data-toggle="dropdown" class="dropdown-toggle" style="width: 102px;" href="#" onclick="return false;">
                    <span>Ubicaci&oacute;n</span>
                    <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                       <li style="padding: 5px;">
                        <style>
                        .form-horizontal .control-label {
                            width: 60px;
                        }
                        .form-horizontal .controls {
                            margin-left: 80px;
                        }
                        .combo-min-form {
                            max-width: 180px;
                        }
                        </style>
                         <form class="form-horizontal" style="padding: 5px 15px 0 0; margin-bottom: 0px;">
                            <div class="control-group" style="text-align: center;">
                                Busqueda de Eventos por ubicaci&oacute;n
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="pais_id">Pais</label>
                                <div class="controls">
                                    <select name="pais_id" id="pais_id" class="combo-min-form">
                                        <option value="">-- Seleccione un pais</option>
                                        <?php foreach ($paises as $pais) { ?>
                                            <option value="<?php echo $pais->pais_id; ?>" <?php if ($pais_id == $pais->pais_id) : ?> selected="selected" <?php endif; ?> ><?php echo $pais->nombre; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="ciudad_id">Ciudad</label>
                                <div class="controls">
                                <select name="ciudad_id" id="ciudad_id" class="combo-min-form">
                                    <option value="">-- Seleccione una ciudad</option>
                                    <?php foreach ($ciudades as $ciudad) { ?>
                                        <option value="<?php echo $ciudad->ciudad_id; ?>" <?php if ($ciudad_id == $ciudad->ciudad_id): ?> selected="selected" <?php endif; ?> ><?php echo $ciudad->nombre; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>
                            <div class="control-group" style="margin-bottom: 0px;">
                                <div class="controls">
                                    <div class="btn btn-info" id="buscar_pais">Buscar</div>
                                </div>
                            </div>
                        </form> <!-- .location-widget -->
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav pull-right">
                <li style="padding: 5px 0px;">
                    <form method="post" class="form-search" action="<?php echo build_segment() ?>"  accept-charset="UTF-8" style="margin: 0px !important;">
                        <input type="text" name="q" placeholder="Buscar por palabra clave...">
                        <input type="submit" id="btn_submit" class="btn" style="margin: 0px;" value="Buscar">
                    </form>
                </li>
            </ul>
        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
</div><!-- /navbar  -->



<div>
    <div class="span3">
        <div id="search-option-field" class="search-option cfix" option="field">
            <div class="search-option-wrap" option="field">

                <div class="search-option-header-wrap">
                    <span class="search-option-header-icon" id="search-option-header-icon-field"></span>
                    <div class="search-option-header" id="id-categorias">Categorias</div>
                </div>

                <div id="bloque-categorias" class="bloque">
                    <div class="search-option-body cfix">
                        <div class="form-item form-item-select search-option-widget-item" id="search-option-field-container">
                            <ul>
                                <?php foreach ($this->milib->categorias_con_eventos(false) as $categoria) : ?>
                                    <li><?php echo anchor_filter('c_' . $categoria->url, $categoria->nombre); ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="span2">
        <div id="search-option-time" class="search-option cfix">
            <div class="search-option-wrap" option="time">
                <div class="search-option-header-wrap">
                    <span class="search-option-header-icon" id="search-option-header-icon-time"></span>

                    <div class="search-option-header"></div>
                </div>

                <div id="bloque-calendar" class="bloque">
                    <div class="search-option-body cfix">
                        <div class="location-widget search-option-widget-item" id="calendario_mes">
                            <div><?php echo Modules::run("evento/calendariomes", true); ?></div>
                        </div> <!-- .location-widget -->
                    </div>

                    <div class="search-option-widget-confirmation cfix">
                        <?php echo anchor("eventos/calendario", "ver detallado", array('class' => 'btn btn-info')); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="span2">
        <div id="search-option-location" class="search-option cfix" option="location">
            <div class="search-option-wrap" option="location">
                <div class="search-option-header-wrap">
                    <span class="search-option-header-icon" id="search-option-header-icon-location"></span>
                    <div class="search-option-header"></div>
                </div>

                <div id="bloque-location" class="bloque">
                    <div class="search-option-body cfix">
                        <div class="location-widget search-option-widget-item">
                            <div class="widget-header widget-header-text">Elige un lugar</div>

                            <label class="form-label" for="searchmenui_country-button">Pais</label>
                            <select name="pais_id" id="pais_id">
                                <option value="">-- Seleccione un pais</option>
                                <?php foreach ($paises as $pais) { ?>
                                    <option value="<?php echo $pais->pais_id; ?>" <?php if ($pais_id == $pais->pais_id) : ?> selected="selected" <?php endif; ?> ><?php echo $pais->nombre; ?></option>
                                <?php } ?>
                            </select>

                            <label class="form-label">Ciudad</label>
                            <select name="ciudad_id" id="ciudad_id">
                                <option value="">-- Seleccione una ciudad</option>
                                <?php foreach ($ciudades as $ciudad) { ?>
                                    <option value="<?php echo $ciudad->ciudad_id; ?>" <?php if ($ciudad_id == $ciudad->ciudad_id): ?> selected="selected" <?php endif; ?> ><?php echo $ciudad->nombre; ?></option>
                                <?php } ?>
                            </select>
                        </div> <!-- .location-widget -->
                    </div>
                    <div class="search-option-widget-confirmation cfix">
                        <div class="btn btn-info" id="buscar_pais">Buscar</div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="span5">
        <div id="search-option-search" class="search-option cfix" option="search">
            <div class="search-option-wrap" option="search">
                <div class="search-option-header-wrap">
                    <div class="search-option-body-wrap cfix">
                        <div class="search-option-body cfix">
                            <div class="form-item form-item-text" id="searchmenui_search-container">
                                <form method="post" class="form-search" action="<?php echo build_segment() ?>"  accept-charset="UTF-8">
                                    <input name="q" type="text" class="form-text form-searchi arrow validate[optional,Generic]" id="searchmenui_search" value="" placeholder="Buscar...">
                                    <input type="submit" id="btn_submit" class="btn" value="Buscar">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>