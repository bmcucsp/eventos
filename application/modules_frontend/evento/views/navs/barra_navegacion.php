<div class="navbar">
    <div class="navbar-inner">
        <ul class="nav">
            <li class="dropdown">
              <a data-toggle="dropdown" class="dropdown-toggle" href="#" onclick="return false;">
                <i class="icon-group icon-large"></i>
                <span class="Open-Sans-normal-300">Categorias</span>
                <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <?php foreach ($this->milib->categorias_con_eventos(false) as $categoria) : ?>
                <li><?php echo anchor_filter('c_' . $categoria->url, $categoria->nombre); ?></li>
                <?php endforeach; ?>
              </ul>
            </li>

            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#" onclick="return false;">
                    <i class="icon-calendar icon-large"></i>
                    <span class="Open-Sans-normal-300">Calendario</span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li style="padding: 15px;">
                        <div id="calendario_mes">
                            <div><?php echo Modules::run("evento/calendariomes", true); ?></div>
                        </div>
                        <div>
                            <?php echo anchor("eventos/calendario", "ver detallado", array('style' => 'color: white; width: 50%; margin: 0px auto;', 'class' => 'btn btn-info')); ?>
                        </div>
                        <!-- .location-widget -->
                    </li>
                </ul>
            </li>
            <li style="position: relative;margin-left: 45px;" class="dropdown">
                <div class="icon-location dropdown-toggle" style="width: 45px;position: absolute;top: 5px;left: -32px;"></div>
                <a data-toggle="dropdown" class="dropdown-toggle" style="width: 102px;" href="#" onclick="return false;">
                <span>Ubicaci&oacute;n</span>
                <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                   <li style="padding: 5px;">
                    <style>
                    .form-horizontal .control-label {
                        width: 60px;
                    }
                    .form-horizontal .controls {
                        margin-left: 80px;
                    }
                    .combo-min-form {
                        max-width: 180px;
                    }
                    </style>
                     <form class="form-horizontal" style="padding: 5px 15px 0 0; margin-bottom: 0px;">
                        <div class="control-group" style="text-align: center;">
                            Busqueda de Eventos por ubicaci&oacute;n
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="pais_id">Pais</label>
                            <div class="controls">
                                <select name="pais_id" id="pais_id" class="combo-min-form">
                                    <option value="">-- Seleccione un pais</option>
                                    <?php foreach ($paises as $pais) { ?>
                                        <option value="<?php echo $pais->pais_id; ?>" <?php if ($pais_id == $pais->pais_id) : ?> selected="selected" <?php endif; ?> ><?php echo $pais->nombre; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="ciudad_id">Ciudad</label>
                            <div class="controls">
                            <select name="ciudad_id" id="ciudad_id" class="combo-min-form">
                                <option value="">-- Seleccione una ciudad</option>
                                <?php foreach ($ciudades as $ciudad) { ?>
                                    <option value="<?php echo $ciudad->ciudad_id; ?>" <?php if ($ciudad_id == $ciudad->ciudad_id): ?> selected="selected" <?php endif; ?> ><?php echo $ciudad->nombre; ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: 0px;">
                            <div class="controls">
                                <div class="btn btn-info" id="buscar_pais">Buscar</div>
                            </div>
                        </div>
                    </form> <!-- .location-widget -->
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="nav pull-right">
            <li style="padding: 5px 0px;">
                <form method="post" class="form-search" action="<?php echo build_segment() ?>"  accept-charset="UTF-8" style="margin: 0px !important;">
                    <input type="text" name="q" placeholder="Buscar por palabra clave...">
                    <input type="submit" id="btn_submit" class="btn" style="margin: 0px;" value="Buscar">
                </form>
            </li>
        </ul>
    </div><!-- /navbar-inner -->
</div><!-- /navbar  -->