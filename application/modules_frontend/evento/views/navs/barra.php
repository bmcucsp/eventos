<div class="container navbar-wrapper">
    <div class="navbar">
        <div class="navbar-inner">
            <a href="#" class="brand">Busquedas</a>
            <ul class="nav">
                <li class="dropdown active">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>CATEGORIAS</b><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="icon-random"></i>content</a>
                        </li>
                        <?php foreach ($this->milib->categorias_con_eventos(false) as $categoria) : ?>
                        <li>
                            <?php echo anchor_filter('c_' . $categoria->url, '<i class="icon-random"></i>' . $categoria->nombre); ?>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </li>
                <li><a href="#">sd</a></li>
            </ul>
        </div>
    </div>
</div>
<div>
    <div class="form-item form-item-select" id="search-option-field-container">
        <ul style="list-style: none; margin: 0px 0px 10px 0px">
            <?php foreach ($this->milib->categorias_con_eventos(false) as $categoria) : ?>
                <li><?php echo anchor_filter('c_' . $categoria->url, $categoria->nombre); ?></li>
                    <?php endforeach; ?>
        </ul>
    </div>
    <div id="bloque-calendar" class="search-option-body-wrap cfix" style="display: none; position: absolute; top : 53px; left:292px; border:1px solid black !important">
        <div class="search-option-body cfix" style="margin: 8px 20px;">
            <div><?php echo anchor("eventos/calendario", "ver detallado"); ?></div>
            <div class="location-widget search-option-widget-item" id="calendario_mes">
                <div><?php echo Modules::run("evento/calendariomes", true); ?></div>
            </div> <!-- .location-widget -->
        </div>
    </div>
    <div class="widget-header widget-header-text">Elige un lugar</div>
    <div>
        <div>
            <label class="form-label" for="searchmenui_country-button">Pais</label>
            <select name="pais_id" id="pais_id">
                <option value="">-- Seleccione un pais</option>
                <?php foreach ($paises as $pais) { ?>
                    <option value="<?php echo $pais->pais_id; ?>" <?php if ($pais_id == $pais->pais_id) : ?> selected="selected" <?php endif; ?> ><?php echo $pais->nombre; ?></option>
                <?php } ?>
            </select>
        </div>
        <div>
            <label class="form-label">Ciudad</label>
            <select name="ciudad_id" id="ciudad_id">
                <option value="">-- Seleccione una ciudad</option>
                <?php foreach ($ciudades as $ciudad) { ?>
                    <option value="<?php echo $ciudad->ciudad_id; ?>" <?php if ($ciudad_id == $ciudad->ciudad_id): ?> selected="selected" <?php endif; ?> ><?php echo $ciudad->nombre; ?></option>
                <?php } ?>
            </select>
        </div>
        <input name="hidden_state" type="hidden" class="form-hidden hidden-state" id="hidden_state" value="">
    </div>
    <div class="search-option-widget-confirmation cfix">
        <div class="btn btn-small btn-info" style="margin-left: 20px" id="buscar_pais">Buscar</div>
        <div class="right searchmenui-cancel fake-link" id="cancelar_pais">Cancel</div>
    </div>
    <div class="form-item form-item-text" id="searchmenui_search-container">
        <form method="post" class="form-search" action="<?php echo base_url() ?>buscar/evento"  accept-charset="UTF-8">
            <input name="q" type="text" class="form-text form-searchi arrow validate[optional,Generic]" id="searchmenui_search" value="" placeholder="Buscar...">
        </form>
    </div>
</div>