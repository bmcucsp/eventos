
<!--
 Copyright 2008 Google Inc. 
 Licensed under the Apache License, Version 2.0: 
 http://www.apache.org/licenses/LICENSE-2.0 
-->
<html>
    <head>
        <title>Google Maps JavaScript API v3 Example: Common Loader</title>
        <script type="text/javascript" src="http://www.google.com/jsapi"></script>
        <script src="<?php echo base_jquery();?>jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="http://api.easyjquery.com/easyjquery.js"></script>

        <script type="text/javascript">
            var place;
            function my_callback(json) {
                console.log(json);
                place = json;
                console.log("IP :" + json.IP + " nCOUNTRY: " + json.COUNTRY);
            }

            function my_callback2(json) {
                // more information at http://api.easyjquery.com/test/demo-ip.php
                console.log(json);
                console.log("IP :" + json.IP + " nCOUNTRY: " + json.COUNTRY + " City: " + json.cityName + " regionName: " + json.regionName);
            }

            // 2. Setup Callback Function
            EasyjQuery_Get_IP("my_callback"); // fastest version
            google.load("maps", "3",  {callback: initialize, other_params:"sensor=false"});

            function initialize() {
                // Initialize default values
                var zoom = 17;
                var latitud = (place.cityLatitude != undefined) ? place.cityLatitude : 37.4419;
                var longitud = (place.cityLongitude != undefined) ? place.cityLongitude : -100.1419;
                var latlng = new google.maps.LatLng(latitud, longitud);
                console.log(google);
                var location = "Showing default location for map.";

                // If ClientLocation was filled in by the loader, use that info instead
                if (google.loader.ClientLocation) {
                    zoom = 13;
                    latlng = new google.maps.LatLng(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
                    location = "Showing IP-based location: <b>" + getFormattedLocation() + "</b>";
                }

                var myOptions = {
                    zoom: zoom,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }

                var map = new google.maps.Map(document.getElementById("map"), myOptions);
                document.getElementById("location").innerHTML = location;
            }

            function getFormattedLocation() {
                if (google.loader.ClientLocation.address.country_code == "US" &&
                    google.loader.ClientLocation.address.region) {
                    return google.loader.ClientLocation.address.city + ", " 
                        + google.loader.ClientLocation.address.region.toUpperCase();
                } else {
                    return  google.loader.ClientLocation.address.city + ", "
                        + google.loader.ClientLocation.address.country_code;
                }
            }

        </script>
    </head>
    <body>
        <div style="width:600;height:600" id="map"></div>
        <div id="location"></div>
    </body>
</html>
