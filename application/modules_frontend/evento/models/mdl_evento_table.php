<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_evento_table extends CI_Model {

    public function get_table_headers() {

        $order = (uri_assoc('order')) == 'asc' ? 'desc' : 'asc';

        $headers = array(
        'evento_id' => anchor('evento/index/order_by/evento_id/order/'.$order, 'evento_id'),
        'local_id' => anchor('evento/index/order_by/local_id/order/'.$order, 'local_id'),
        'capacidad' => anchor('evento/index/order_by/capacidad/order/'.$order, 'capacidad'),
        'nombre' => anchor('evento/index/order_by/nombre/order/'.$order, 'nombre'),
        'descripcion' => anchor('evento/index/order_by/descripcion/order/'.$order, 'descripcion'),
        'fecha_evento' => anchor('evento/index/order_by/fecha_evento/order/'.$order, 'fecha_evento'),
        'duracion' => anchor('evento/index/order_by/duracion/order/'.$order, 'duracion'),
        'url' => anchor('evento/index/order_by/url/order/'.$order, 'url'),
        'img' => anchor('evento/index/order_by/img/order/'.$order, 'img'),
        'status' => anchor('evento/index/order_by/status/order/'.$order, 'status'),
        'publico' => anchor('evento/index/order_by/publico/order/'.$order, 'publico'),
        'costo_estudiante' => anchor('evento/index/order_by/costo_estudiante/order/'.$order, 'costo_estudiante'),
        'costo_profesional' => anchor('evento/index/order_by/costo_profesional/order/'.$order, 'costo_profesional'),
        'gratuito' => anchor('evento/index/order_by/gratuito/order/'.$order, 'gratuito'),
        'created_at' => anchor('evento/index/order_by/created_at/order/'.$order, 'created_at'),
        'presencial' => anchor('evento/index/order_by/presencial/order/'.$order, 'presencial'),
        'organizador' => anchor('evento/index/order_by/organizador/order/'.$order, 'organizador'),
        );


        return $headers;
    }

}

?>
