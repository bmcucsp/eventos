<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_evento extends MY_Model {

	public function __construct() {

		parent::__construct();

		$this->table_name = 'evento';

		$this->primary_key = 'evento.evento_id';

		$this->select_fields = "
		SQL_CALC_FOUND_ROWS
		evento.*, evento.fecha_fin as nearest_date";

        $this->order_by = 'evento_id';

    }

    function db_array() {
        $fields = $this->db->list_fields($this->table_name);
        foreach ($fields as $field) {
            if (isset($_POST[$field])) {
                $db_array[$field] = $this->input->post($field, TRUE);
            }
        }
        return $db_array;
    }

    /*
     * busqueda match
     */

    function search($table, $campos, $where, $texto_busqueda, $perpage = 0, $start = 0) {
        $this->db->select($campos);
        $this->db->where("MATCH(nombre) AGAINST('$texto_busqueda' IN BOOLEAN MODE)", NULL, TRUE);
        $this->db->where($where);
        if ($perpage != 0)
            $this->db->limit($perpage, $start);
        $query = $this->db->get($table);
        return $query->result();
    }

    /*
     * busqueda like
     */

    function search_like($table, $campos, $where, $like, $texto_busqueda, $perpage = 0, $start = 0) {
        $this->db->select($campos);
        $this->db->like($like);
        $this->db->or_like($texto_busqueda);
        $this->db->where($where);
        if ($perpage != 0)
            $this->db->limit($perpage, $start);
        $query = $this->db->get($table);
        return $query->result();
    }

    function retrieve_repeated_events($start_date, $where = "") {
        var_dump($start_date);
        exit;
        $start_date = str_replace('-', '/', $start_date);
        $start_date = strtotime($start_date);
        if (!$start_date)
            return array();

        /* recuperar eventos repetidos */
        $statement = "select event_meta.today, ev.* from (select FROM_UNIXTIME($start_date, '%d/%m/%Y') as today, evento_id from `events_meta` em
            where (em.repeat_start <= $start_date) and ($start_date <= em.repeat_end) and
            (($start_date - UNIX_TIMESTAMP(FROM_UNIXTIME(em.repeat_start, '%Y%m%d'))) % em.repeat_interval = 0))
            as event_meta
            inner join evento ev on event_meta.evento_id = ev.evento_id" . $where;
        $query = $this->db->query($statement);
        $res_repeated = ($query->num_rows > 0) ? $query->result() : array();
        return $res_repeated;
    }

    function retrieve_repeated_events_since($start_date) {

    }

    public function evento_mas_popular_x_categoria()
    {
        $statement = "select max(cant_participantes) max_participantes, categoria_id, evento_id, es_repetido from
        (select cant_participantes, p.evento_id, nombre, categoria_id, es_repetido from
            (select count(evento_id) cant_participantes, evento_id from inscrito
            group by evento_id) p
        inner join
        evento on p.evento_id = evento.evento_id and
        ((evento.es_repetido = 1 and
            (select count(id) from events_meta
                where evento_id = evento.evento_id and UNIX_TIMESTAMP(now()) <= repeat_end) >= 1)
        or (evento.es_repetido = 0 and UNIX_TIMESTAMP(now()) <= evento.fecha_inicio))
        ) part_x_cat
        group by categoria_id";
        $query = $this->db->query($statement);
        $res_repeated = ($query->num_rows > 0) ? $query->result() : array();
        return $res_repeated;
    }

    public function proximos_eventos($params = array())
    {
    	//var_dump($params); //exit;
        $where = array();
        if (isset($params['where'])) {
            $where = $params['where'];
            unset($params['where']);
        }
        if (isset($params['paginate']) && $params['paginate'] === true) {
            $limit = 'limit ' . $params['page'] . ', ' .  $params['limit'];
            $this->offset = (isset($params['page'])) ? $params['page'] : 0;
            $this->limit = (isset($params['limit'])) ? $params['limit'] : $this->limit;
        }
        else $limit = "";
        if (is_array($where) && !empty($where))
        {
        	//var_dump($where);
        	$dest = array();
        	foreach ($where as $key => $val)
        	{
        		$prefix = (count($dest) == 0) ? '' : ' AND ';
        		if ($val !== '')
        		{
        			if ( ! $this->db->_has_operator($key))
        			{
        				$key .= ' =';
        			}
        			$val = ' '.$this->db->escape($val);
        		}
        		$dest[] = $prefix.$key.$val;
        	}
        	$where = 'where ' . implode("", $dest);
        	//var_dump($where);
        }
        else
            $where = "";
        if(isset($params['to_calendar']) && $params['to_calendar'])
        {
            $statement = "
               select SQL_CALC_FOUND_ROWS recent_events.* from
                    (select * from(
                            select *, fecha_fin as nearest_date, -1 as recent_valid from evento
                            where es_repetido = 0
                        ) evento
                        $where
                    union
                    select evento.*, rep.nearest_date, rep.recent_valid from
                        (select a.evento_id, a.nearest_date, a.recent_valid from(
                                select evento_id,
                                FROM_UNIXTIME(repeat_start) rs,
                                if ({$params['fecha_ini']} >  repeat_start,
                                if (repeat_start + (floor(({$params['fecha_ini']} -  UNIX_TIMESTAMP(FROM_UNIXTIME(repeat_start, '%Y%m%d'))) / repeat_interval) + 1) * repeat_interval > repeat_end,
									repeat_start + floor((repeat_end -  repeat_start) / repeat_interval) * repeat_interval,
									repeat_start + (floor(({$params['fecha_ini']} -  repeat_start) / repeat_interval) + 1) * repeat_interval
								),repeat_start) as nearest_date,
								if (UNIX_TIMESTAMP(now()) >  repeat_start and UNIX_TIMESTAMP(now()) < repeat_end,
										if (repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval > repeat_end,
												repeat_start + floor((repeat_end -  repeat_start) / repeat_interval) * repeat_interval,
												repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval
										)
									,-1
								) as recent_valid
                                from events_meta
                            ) a
                        ) rep
                        inner join
                        evento
                        on rep.evento_id = evento.evento_id
                    ) recent_events
                    $where
                    order by nearest_date
                    $limit
            ";
        }
        else
        {
            $statement = "
               select SQL_CALC_FOUND_ROWS recent_events.* from
                    (select * from(
                            select *, fecha_fin as nearest_date, -1 as recent_valid from evento
                            where es_repetido = 0
                            and fecha_fin >= UNIX_TIMESTAMP(now())
                        ) evento
                        $where
                    union
                    select evento.*, rep.nearest_date, rep.recent_valid from
                        (select a.evento_id, a.nearest_date, a.recent_valid from(
                                select evento_id,
                                FROM_UNIXTIME(repeat_start) rs,
                                if (UNIX_TIMESTAMP(now()) >  repeat_start,
									if (repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval > repeat_end,
											repeat_start + floor((repeat_end -  repeat_start) / repeat_interval) * repeat_interval,
											repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval
									),repeat_start
								) as nearest_date,
								if (UNIX_TIMESTAMP(now()) >  repeat_start and UNIX_TIMESTAMP(now()) < repeat_end,
									if (repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval > repeat_end,
											repeat_start + floor((repeat_end -  repeat_start) / repeat_interval) * repeat_interval,
											repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval
									),-1
								) as recent_valid
                                from events_meta
                                where repeat_end >= UNIX_TIMESTAMP(now())
                            ) a where a.nearest_date >= UNIX_TIMESTAMP(now())
                        ) rep
                        inner join
                        evento
                        on rep.evento_id = evento.evento_id
                    ) recent_events
                    $where
                    order by nearest_date
                    $limit
            ";
        }
        $query = $this->db->query($statement);
        /*$myFile = "query.txt";
        $fh = fopen($myFile, 'w') or die("can't open file");
        $var = $this->db->last_query();
        fwrite($fh, $var);
        fclose($fh);*/
        // var_dump($this->db->last_query());exit;

        if (isset($params['paginate']) && $params['paginate'] === true) {
            $this->_prep_pagination($params);
        }
        $res_repeated = ($query->num_rows > 0) ? $query->result() : array();
        return $res_repeated;
    }

    private function _prep_pagination($params)
    {
        if (isset($params['paginate']) AND $params['paginate'] == TRUE)
        {
            $query = $this->db->query('SELECT FOUND_ROWS() AS total_rows');
            $this->total_rows = $query->row()->total_rows;
            $this->load->library('pagination');

            if (!isset($this->page_config)) {
                $config = array(
                    'base_url' => $this->_base_url(),
                    'total_rows' => $this->total_rows,
                    'per_page' => $this->limit,
                    'cur_page' => $this->offset,
                    'next_link' => $this->lang->line('next') . ' >',
                    'next_tag_open' => '<li>',
                    'next_tag_close' => '</li>',
                    'prev_link' => '< ' . $this->lang->line('prev'),
                    'prev_tag_open' => '<li>',
                    'prev_tag_close' => '</li>',
                    'cur_tag_open' => '<li class="active"><a>',
                    'cur_tag_close' => '</a></li>',
                    'num_links' => 1,
                    'num_tag_open' => '<li>',
                    'num_tag_close' => '</li>',
                    'first_link' => 'Primero',
                    'first_tag_open' => '<li>',
                    'first_tag_close' => '</li>',
                    'last_link' => '&Uacute;ltimo',
                    'last_tag_open' => '<li>',
                    'last_tag_close' => '</li>'
                );
            } else {
                $config = $this->page_config;
            }
            $this->pagination->initialize($config);
            $this->page_links = $this->pagination->create_links();
            $this->current_page = ($this->offset / $this->limit) + 1;
            $this->num_pages = ceil($this->total_rows / $this->limit);
        }
    }
    private function _base_url() {

        // strips the page segment and re-adds it to the end
        // for use in CI pagination library for base_url

        $uri_segments = $this->uri->uri_string();

        $uri_segments = explode('/', $uri_segments);

        //var_dump($uri_segments);exit;
        foreach ($uri_segments as $key => $value) {

            if ($value == 'page') {

                unset($uri_segments[$key], $uri_segments[$key + 1]);
            }
        }

        $uri_segments[] = 'page';

        return site_url(implode('/', $uri_segments));
    }
    public function get_evento($id_evento, $es_repetido)
    {
        /* esto para recuperar la ultima fecha valida para mostrar (nearest_date) */
        if ($es_repetido) {
            return $this->db->query("
				select ev.*, rep.nearest_date from (
					select if (UNIX_TIMESTAMP(now()) >  repeat_start,
					if (repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval > repeat_end,
						repeat_start + floor((repeat_end -  repeat_start) / repeat_interval) * repeat_interval,
						repeat_start + (floor((UNIX_TIMESTAMP(now()) -  repeat_start) / repeat_interval) + 1) * repeat_interval
					   ),repeat_start
					) as nearest_date, evento_id
					from events_meta where evento_id = '$id_evento'
					) rep
				inner join
				evento ev on ev.evento_id = rep.evento_id and ev.status = 1
				limit 1")->row();
        }
        return parent::get(array('where' => array('evento_id' => $id_evento), 'return_row' => true));
    }
}
?>