<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_evento_repeticion extends MY_Model {

    public function __construct() {

        parent::__construct();

        $this->table_name = 'events_meta';

        $this->primary_key = 'events_meta.id';

        $this->select_fields = "
		SQL_CALC_FOUND_ROWS
		events_meta.*";

        $this->order_by = 'id';
    }
}

?>