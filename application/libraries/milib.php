<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package CodeIgniter
 * @author  ExpressionEngine Dev Team
 * @copyright  Copyright (c) 2006, EllisLab, Inc.
 * @license http://codeigniter.com/user_guide/license.html
 * @link http://codeigniter.com
 * @since   Version 1.0
 * @filesource
 */
// --------------------------------------------------------------------

/**
 * CodeIgniter Template Class
 *
 * This class is and interface to CI's View class. It aims to improve the
 * interaction between controllers and views. Follow @link for more info
 *
 * @package		CodeIgniter
 * @author		Colin Williams
 * @subpackage	Libraries
 * @category	Libraries
 * @link		http://www.williamsconcepts.com/ci/libraries/template/index.html
 * @copyright  Copyright (c) 2008, Colin Williams.
 * @version 1.4.1
 *
 */
class CI_Milib {

    var $CI;

    /**
     * Constructor
     *
     * Loads template configuration, template regions, and validates existence of
     * default template
     *
     * @access	public
     */
    function CI_Milib() {
        // Copy an instance of CI so we can use the entire framework.
        $this->CI = & get_instance();
    }

    // --------------------------------------------------------------------

    /**
     * Recupera todas las categorias existentes
     *
     * @access	public
     * @return	an array of category's objects
     */
    function categorias_con_eventos($solo_ids = true) {
        //primero recuperamos las categorias con eventos creados,
        //para mostrar a demanda
        $this->CI->load->model("evento/mdl_evento");
        $eventos = $this->CI->mdl_evento->proximos_eventos();
        $cat_with_events = array();
        foreach ($eventos as $evento) {
            $cat_with_events[$evento->categoria_id] = true;
        }
        if ($solo_ids) {
            return $data['cat_with_events'] = $cat_with_events;
        }
        else {
            $params = array();
            $this->CI->load->model("categoria/mdl_categoria");
            $keys = array();
            foreach ($cat_with_events as $key => $value) {
                $keys[] = $key;
            }
            if (!empty($keys)) {
                $params['where_in'] = array('categoria_id' => $keys);
            }
            return $this->CI->mdl_categoria->get($params);
        }
    }

    function categorias() {
        $this->CI->load->model('categoria/mdl_categoria');
        $categorias = $this->CI->mdl_categoria->get(array('select' => 'categoria_id, nombre'));

        $cat_mapeadas = array();
        foreach ($categorias as $categoria) {
            $cat_mapeadas[$categoria->categoria_id] = $categoria->nombre;
        }
        return $cat_mapeadas;
    }

    function categorias_ordenadas($select) {
        $this->CI->load->model('categoria/mdl_categoria');
        $params = array();
        $params['select'] = $select;
        $params['order_by'] = 'nombre';
        $categorias = $this->CI->mdl_categoria->get($params);
        return $categorias;
    }

    function bienvenido_usuario() {
        if (trim($this->CI->session->userdata('username')) != '')
            return "Bienvenido ".$this->CI->session->userdata('username');
        return "Bienvenido usuario";
    }

    //verifica si usuario esta o no logueado
    function logueado() {
    	return ($this->CI->session->userdata('logged_in') && $this->CI->session->userdata('sesion_id_user'));
    }

    public function fotoUser()
    {
        if ($this->CI->session->userdata('img')) {
            $this->CI->load->helper('html');
            return img(array(
                    'src' => base_path_foto_user() . $this->CI->session->userdata('img') . '_thumb.jpg',
                    'width' => 20,
                    'height' => 20,
                    'alt' => $this->CI->session->userdata('username'),
                    'title' => $this->CI->session->userdata('username')
                    )
                );
        }
        /* no tiene imagen todavia, entonces retornar uno por defecto */
        return '<i class="icon-user icon-white"></i> ';
    }

    public function totalEventsCreated()
    {
        if ($this->logueado()) {
            $this->CI->load->model('mdl_evento');
            $params['select'] = 'count(evento_id) as totalEvents';
            $params['where']['usuario_id'] = $this->CI->session->userdata('sesion_id_user');
            $params['return_row'] = true;
            return $this->CI->mdl_evento->get($params)->totalEvents;
        }
        return array();
    }

    //verifica si una url esta repetida
	function verificar_url_repetida($url) {
		$CI =& get_instance();
		$CI->load->model('mdl_evento');

		$params = array();
        $params['select'] = 'url';
        $params['where']['url'] = $url;
        $params['return_row'] = true;
        $evento = $CI->mdl_evento->get($params);
		if(count($evento) > 0){
			$nro_item = 1;
			do {
				$nueva_url = $url . "-" . $nro_item;
		        $params['select'] = 'url';
		        $params['where']['url'] = $nueva_url;
		        $params['return_row'] = true;
		        $evento = $CI->mdl_evento->get($params);
				$nro_item++;
			} while (count($evento) > 0);
			return $nueva_url;
		}
		return $url;
	}

    function user_campos_incompletos($id_user) {
        $this->CI->load->model('user/mdl_usuario');
		$params = array();
        $params['select'] = 'ciudad_id, nombres, apellidos, ocupacion, direccion, fecha_nac, img';
        $params['where']['usuario_id'] = $id_user;
        $params['return_row'] = true;
        $usuarios = $this->CI->mdl_usuario->get($params);
        $campos = array();
        $selected_fields = explode(",", $params['select']);
        $campos["total"] = count($selected_fields);
        $campos["campo"] = array();
        if($usuarios->ciudad_id == 0){
        	$var = 'Le falta completar su pa&iacute;s y ciudad';
        	array_push($campos["campo"], $var);
        }
        if ($usuarios->nombres == '') {
        	$var = 'Le falta completar sus nombres';
        	array_push($campos["campo"], $var);
        }
        if ($usuarios->apellidos == '') {
        	$var = 'Le falta completar sus apellidos';
        	array_push($campos["campo"], $var);
        }
        if ($usuarios->fecha_nac == '') {
        	$var = 'Le falta completar su fecha de nacimiento';
        	array_push($campos["campo"], $var);
        }
        if ($usuarios->img == '') {
        	$var = 'Le falta subir su imagen';
        	array_push($campos["campo"], $var);
        }
        if (isset($campos["campo"]))
        {
        	$campos["faltan"] = count($campos["campo"]);
        }
        return $campos;
    }

    function nombres_url_segment($segment) {
		$filtro = array("p_", "i_", "c_", "f_", "t_", "q_");
		$datos = array();
		foreach($segment as $seg) {
			if ($seg == "eventos" || !in_array(substr($seg, 0, 2), $filtro)) continue;
			$print = "";
			$prefix = substr($seg, 0, 2);
			switch ($prefix) {
				case "c_":
					$print = 'Categoria - '. ucfirst(substr($seg, 2));
					break;
				case "f_":
					$fecha = substr($seg, 2);
					if ($fecha == "estasemana" || $fecha == "estemes" || $fecha == "hoydia") {
							switch ($fecha) {
								case 'estasemana':
									$print = "Esta semana";
									break;
								case 'estemes':
									$print = "Este mes";
									break;
								case 'hoydia':
									$print = "Hoy";
									break;
							}
					}
					else{
						$print = str_replace('-', '/', substr($seg, 2));
						$print = 'Fecha - '. substr($print, 8, 2) . '/'. substr($print, 5, 2).'/'. substr($print, 0, 4);
					}
					break;

				case "p_":
					$this->CI->load->model("evento/mdl_pais");
					$params = array();
					$params['select'] = "nombre";
					$params["where"]["pais_id"] = substr($seg, 2);
					$params['return_row'] = true;
					$pais = $this->CI->mdl_pais->get($params);
					$print = 'Pais - '.$pais->nombre;
					break;

				case "i_":
					$this->CI->load->model("evento/mdl_ciudad");
					$params = array();
					$params['select'] = "nombre";
					$params["where"]["ciudad_id"] = substr($seg, 2);
					$params['return_row'] = true;
					$ciudad = $this->CI->mdl_ciudad->get($params);
					$print = 'Ciudad - '.$ciudad->nombre;
					break;
				case "q_":
					$print = str_replace("+", ' ', substr($seg, 2));
					$print = "\"" .  $print . "\"";
				default:
					break;
			}
			$datos[] = $print;
    	}
    	return $datos;
    }

}

// END Template Class

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */