<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require("twitteroauth.php");

class CI_Twitter
{
	private $_oauth = NULL;
	var $CI;
	
	function __construct()
	{
		session_start();
		$CI = & get_instance();
		$this->_oauth = new TwitterOAuth($CI->config->item('consumer_key'), $CI->config->item('consumer_secret'));
	}
			
	function sesion_twitter() {
		// solicitamos los tokens de autorizacion
		//$request_token = $this->_oauth->getRequestToken(base_js().'getTwitterData.php');
		$urlt = base_url().'user/twitterdata';
		$request_token = $this->_oauth->getRequestToken();
		
		// guardamos los tokens es sesiones
		$_SESSION['oauth_token'] = $request_token['oauth_token'];
		$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
		
		// hemos sido autorizados
		if ($this->_oauth->http_code == 200) {
		    // genera la url del redirect ./user/registro/twitter
		    $url = $this->_oauth->getAuthorizeURL($request_token['oauth_token']);
		    redirect($url);
		} else {
		    // error al obtener la autorizacion
		    die('Lo sentimos, no se pudo conectar.');
		}
	}
	
	function get_data_twitter() {
		// creamos una instancia con las sesiones
		$CI = & get_instance();
		$this->_oauth = new TwitterOAuth($CI->config->item('consumer_key'), $CI->config->item('consumer_secret'), $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
		// solicitamos el access token y obtenemos la info de user
		$access_token = $this->_oauth->getAccessToken($_GET['oauth_verifier']);
		//$_SESSION['access_token'] = $_GET['oauth_verifier'];
		//$_SESSION['access_token'] = $access_token;
		$user_info = $this->_oauth->get('account/verify_credentials');
		//print_r($user_info);
		return $user_info;
	}
	
}