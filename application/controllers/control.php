<?php
class Control extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->library('grocery_CRUD');
		$this->load->model('evento/mdl_evento');
		$this->load->model('user/mdl_usuario');
	}
	
	function index(){
		return;
	}
	
	function _example_output($output = null)
	{
		$this->load->view('crud/grocery_view', $output);	
	}
		
	function eventosadmin() {
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		//$crud->set_theme('flexigrid');
		//$crud->where('status', 1);
		$crud->order_by('created_at', 'desc');
		$crud->set_table('evento');
		$crud->columns('nombre', 'descripcion', 'created_at', 'status', 'organizador', 'usuario_id');
		//$crud->fields('nombre', 'descripcion', 'created_at', 'fecha_inicio', 'fecha_fin', 'organizador', 'categoria_id', 'usuario_id');
		$crud->set_subject('Evento');
		$crud->unset_add();
		
		$crud->callback_before_update(array($this, '_limpiarc'));
		$crud->add_action('Bloquear', '', 'control/bloquear', 'ui-icon-check');
		//$crud->add_action('Facebook', 'http://www.froglinks.com/s/441/images/editor/icons/icon_facebook.gif', 'control/facebook');
		
		$output = $crud->render();

		$this->_example_output($output);
	}
	
	function usuariosadmin() {
		$crud = new grocery_CRUD();

		$crud->unset_add();
		$crud->order_by('created_at', 'desc');
		$crud->set_theme('datatables');
		$crud->set_table('usuario');
		
		$crud->callback_column('created_at', array($this, '_formato_fecha'));
		//standardize_date	
		$crud->columns('usuario_id', 'username', 'nombres', 'apellidos', 'email', 'status', 'created_at');
		//$crud->fields('name', 'descripcion', 'tag', 'url', 'descarga', 'voto', 'status', 'comment_status', 'total_ratings');
		$crud->set_subject('Usuario');
		$crud->add_action('Deshabilitar', '', 'control/deshabilitar', 'ui-icon-check');
		
		$output = $crud->render();

		$this->_example_output($output);
	}
	
	function _formato_fecha($value, $row) {
		$this->load->helper('mcb_date');
		return format_date_to_show($value);
	}
	
	//deshabilitar eventos
    function bloquear(){
		if($this->uri->segment(3) != '' ){
			if(!is_numeric($this->uri->segment(3))){
				redirect('/');
			}
			
			$params = array();
			$params['select'] = 'evento_id, status';
			$params['where']['evento_id'] = $this->uri->segment(3);
			$evento = $this->mdl_evento->get($params);
			if(count($evento) > 0){
				//si esta publicado lo deshabilitamos sino nada
				if($evento[0]->status == '1'){
					$params = array();
					$params['status'] = 0;
					if($this->mdl_evento->save($params, $evento[0]->evento_id)){
						$this->eventosadmin();
					}					
				}
				else
					$this->eventosadmin();
			}
			else
				$this->eventosadmin();	
		}
	}

	//deshabilitar usuario
    function deshabilitar(){
    	if($this->uri->segment(3) != '' ){
			if(!is_numeric($this->uri->segment(3))){
				redirect('/');
			}
			
			$params = array();
			$params['select'] = 'usuario_id, status';
			$params['where']['usuario_id'] = $this->uri->segment(3);
			$evento = $this->mdl_evento->get($params);
			if(count($evento) > 0){
				//si esta publicado lo deshabilitamos sino nada
				if($evento[0]->status == '1'){
					$params = array();
					$params['status'] = 0;
					if($this->mdl_evento->save($params, $evento[0]->usuario_id)){
						$this->usuariosadmin();
					}					
				}
				else
					$this->usuariosadmin();
			}
			else
				$this->usuariosadmin();	
		}
	}	
	
}