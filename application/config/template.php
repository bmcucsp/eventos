<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template
| group to make active.  By default there is only one group (the
| "default" group).
|
*/
$template['active_template'] = 'template-sidebar';

/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land.
|   You may also include default markup, wrappers and attributes here
|   (though not recommended). Region keys must be translatable into variables
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| Default Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
Este template es limpio, no tiene areas de contenido con estilos.
Lo utilizan: pagina de inicio, ..
*/

$template['default']['template'] = 'template';
$template['default']['regions'] = array(
		'header_title',
		'error404',
		'title',
		'content',
);
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;

/*
|--------------------------------------------------------------------------
| Template: Pagina Intera
|--------------------------------------------------------------------------
*/

$template['template-sidebar']['template'] = 'template-sidebar';
$template['template-sidebar']['regions'] = array(
		'header_title',
		'title',
		'system_messages',
		'content',
		'sidebar',
);
$template['template-sidebar']['parser'] = 'parser';
$template['template-sidebar']['parser_method'] = 'parse';
$template['template-sidebar']['parse_template'] = FALSE;


/*
|--------------------------------------------------------------------------
| Template: Pagina de login / register
|--------------------------------------------------------------------------
*/
$template['login']['template'] = 'template-login';
$template['login']['regions'] = array(
		'header_title',
		'title',
		'system_messages',
		'content',
);
$template['login']['parser'] = 'parser';
$template['login']['parser_method'] = 'parse';
$template['login']['parse_template'] = FALSE;
/* End of file template.php */
/* Location: ./system/application/config/template.php */

/*
|--------------------------------------------------------------------------
| Template: Pagina de full width
|--------------------------------------------------------------------------
Este template tiene un div full-padded que contiene todo el contenido,
para utilizarlo agregar row-fluid
*/
$template['full-width']['template'] = 'template-full-width';
$template['full-width']['regions'] = array(
		'header_title',
		'title',
		'system_messages',
		'barra_navegacion',
		'content',
);
$template['full-width']['parser'] = 'parser';
$template['full-width']['parser_method'] = 'parse';
$template['full-width']['parse_template'] = FALSE;
/* End of file template.php */
/* Location: ./system/application/config/template.php */

/*
|--------------------------------------------------------------------------
| Template: vista opciones y categorias , busqueda en barra horizontal
|--------------------------------------------------------------------------
*/
$template['template-barra-navegacion']['template'] = 'template-filtro/template-filtro';
$template['template-barra-navegacion']['regions'] = array(
		'header_title',
		'title',
		'system_messages',
		'barra_navegacion',
		'breadcrumbs',
		'content',
		'calendario',
		'publicidad'
);
$template['template-barra-navegacion']['parser'] = 'parser';
$template['template-barra-navegacion']['parser_method'] = 'parse';
$template['template-barra-navegacion']['parse_template'] = FALSE;

/*
|--------------------------------------------------------------------------
| Template: vista de mensajes de bienvenida para enviar en correos
|--------------------------------------------------------------------------
*/
$template['template-email-mensajes']['template'] = 'template-email-mensajes';
$template['template-email-mensajes']['regions'] = array(
		'title',
		'mensaje',
		'email',
		'otros'
	);
$template['template-email-mensajes']['parser'] = 'parser';
$template['template-email-mensajes']['parser_method'] = 'parse';
$template['template-email-mensajes']['parse_template'] = FALSE;


/* End of file template.php */
/* Location: ./system/application/config/template.php */