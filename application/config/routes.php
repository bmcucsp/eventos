<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "evento/pagina_inicio";
$route['404_override'] = 'error';
$route['eventos'] = 'evento/index';
$route['evento/agregar'] = 'evento/agregar';
$route['evento/agregar/(:any)'] = 'evento/agregar/$1';
$route['evento/delete/(:any)'] = 'evento/delete/$1';

$route['evento/get_ciudades/(:num)'] = 'evento/get_ciudades/$1';

$route['eventos/page/(:num)'] = 'evento/index/page/$1';
$route['eventos/page'] = 'evento/index';


$route['evento/evento_async/(:any)/evento_id/(:num)'] = 'evento/evento_async/$1/evento_id/$2';
$route['evento/evento_async/(:any)'] = 'evento/evento_async/$1';

$route['eventos/fecha/(:any)'] = 'evento/fecha/$1';

$route['eventos/categoria'] = 'categoria/ver';

$route['evento/activar_evento/evento_id/(:num)'] = 'evento/activar_evento/evento_id/$1';
$route['evento/desactivar_evento/evento_id/(:num)'] = 'evento/desactivar_evento/evento_id/$1';

$route['evento/test'] = 'evento/test';
$route['evento/mapa'] = 'evento/mapa';
$route['evento/build_filter_link'] = 'evento/build_filter_link';
$route['evento/barra_horizontal_filtro'] = 'evento/barra_horizontal_filtro';
$route['evento/barra'] = 'evento/barra';
//para mostrar el detalle de un evento
$route['evento/(:any)'] = 'evento/detalle';
//calendario
$route['eventos/calendario'] = 'evento/calendario';
$route['eventos/calendario-mes'] = 'evento/calendariomes';
$route['eventos/calendario-mes/(:num)/(:num)'] = 'evento/calendariomes';
$route['eventos/calendario-mes/(:any)'] = 'evento/calendariomes/$1';

$route['categoria/page/(:num)'] = 'categoria/index/page/$1';
$route['categoria/page'] = 'categoria/index';

//$route['eventos/(:any)'] = 'evento/categoria/$1';

//usuario
$route['user/link'] = 'user/link_activacion';
$route['user/log/twitter'] = 'user/twitter';
$route['user/registro/twitter'] = 'user/registro_twitter';
$route['user/join/twitter'] = 'user/join';
$route['servicio/condiciones'] = 'user/condiciones';
$route['servicio/politicas'] = 'user/politicas';


//$route['user/logface'] = 'user/login';
//$route['eventos/(:any)'] = 'evento/categoria/$1';
$route['eventos/(:any)'] = 'evento/filtro/$1';
/*
 *
 * Routes
 *
 */

/* www.example.com/events/
www.example.com/events/sample-event
www.example.com/users/
www.example.com/users/sample-user */

/* End of file routes.php */
/* Location: ./application/config/routes.php */