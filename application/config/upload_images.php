<?php
//config para subir imagen de eventos
$config['config_evento']['upload_path'] = "./assets/uploads/temp/";
$config['config_evento']['upload_path_temp'] = "./assets/uploads/";
$config['config_evento']['allowed_types'] = 'gif|jpg|png';
$config['config_evento']['max_size'] = '2048';
$config['config_evento']['max_width'] = '1600';
$config['config_evento']['max_height'] = '1600';
$config['config_evento']['remove_spaces'] = true;

//config para redimensionar
$config['config_evento']['width_standard'] = 640;
$config['config_evento']['height_standard'] = 36000;
$config['config_evento']['width_thumb'] = 90;
$config['config_evento']['height_thumb'] = 60;

//config para subir imagen de config_user
$config['config_user']['upload_path'] = "./assets/uploads/users/";
$config['config_user']['allowed_types'] = 'gif|jpg|png';
$config['config_user']['max_size'] = '1024';
$config['config_user']['max_width'] = '1024';
$config['config_user']['max_height'] = '1024';
$config['config_user']['remove_spaces'] = true;

//config para redimensionar
$config['config_user']['width_standard'] = 180;
$config['config_user']['height_standard'] = 200;
$config['config_user']['width_thumb'] = 48;
$config['config_user']['height_thumb'] = 48;
