<?php

class Acceso {

    private $config = array(
        'acceso' => array(
            'evento' => array(
                'title' => 'evento',
                'href' => 'evento',
                'user_type' => 3,
                'submenu' => array(
                    'evento/index' => array(
                        'title' => 'Titulo',
                        'href' => 'evento/index',
                        'user_type' => 3
                    ),
                    'evento/form' => array(
                        'title' => 'Titulo',
                        'href' => 'evento/agregar',
                        'user_type' => 2
                    )
                )
            ),
            'user' => array(
                'title' => 'user',
                'href' => 'user',
                'user_type' => 2,
                'submenu' => array(
                    'user/index' => array(
                        'title' => 'user',
                        'href' => 'user/index',
                        'user_type' => 3
                    ),
                    'user/login' => array(
                        'title' => 'user_login',
                        'href' => 'user/login',
                        'user_type' => 3
                    ),
                    'user/register' => array(
                        'title' => 'user_register',
                        'href' => 'user/register',
                        'user_type' => 3
                    ),
                    'user/zona_privada' => array(
                        'title' => 'user_register',
                        'href' => 'user/zona_privada',
                        'user_type' => 2
                    ),
                    'user/cuenta' => array(
                        'title' => 'cuenta',
                        'href' => 'user/cuenta',
                        'user_type' => 2
                    ),
                    'user/suscripcion' => array(
                        'title' => 'suscripcion',
                        'href' => 'user/suscripcion',
                        'user_type' => 2
                    ),
                    'user/index' => array(
                        'title' => 'escritorio',
                        'href' => 'user/index',
                        'user_type' => 2
                    ),
                    'user/eventos' => array(
                        'title' => 'user_eventos',
                        'href' => 'user/eventos',
                        'user_type' => 2
                    ),
                    'user/participar' => array(
                        'title' => 'user_participar',
                        'href' => 'user/participar',
                        'user_type' => 2
                    )
                )
            ),
            'categoria' => array(
                'title' => 'categoria',
                'href' => 'categoria',
                'user_type' => 2,
                'submenu' => array(
                    'index' => array(
                        'title' => 'Titulo',
                        'href' => 'categoria/index',
                        'user_type' => 2
                    ),
                    'form' => array(
                        'title' => 'Titulo',
                        'href' => 'categoria/form',
                        'user_type' => 2
                    )
                )
            )
        )
    );

    function check_permission($uri_string, $user_type) {

        foreach ($this->config['acceso'] as $menu_item) {
            if (isset($menu_item['href']) and $uri_string == $menu_item['href']) {
                if (isset($menu_item['user_type']) && $user_type > $menu_item['user_type']) {
                    $this->CI->session->set_flashdata('custom_error', 'Inicie sesi&oacute;n para acceder a su cuenta');
                    $this->CI->session->set_userdata('last_index', $uri_string);
                    //asumiendo que user sera la zona publica
                    redirect('user/login');
                }
            }

            if (isset($menu_item['submenu'])) {

                foreach ($menu_item['submenu'] as $sub_item) {
                    if (isset($sub_item['href']) and $uri_string == $sub_item['href']) {
                        if (isset($sub_item['user_type']) and $user_type > $sub_item['user_type']) {
                            $this->CI->session->set_flashdata('custom_error', 'Inicie sesi&oacute;n para acceder a su cuenta');
                            $this->CI->session->set_userdata('last_index', $uri_string);
                            //asumiendo que user sera la zona publica
                            redirect('user/login');
                        }
                    }
                }
            }
        }
    }

    function identificado() {
        $this->CI = & get_instance();
        $uri_string = $this->CI->uri->uri_string();
        /* si no viene de logout y no viene de una llamada asincrona entonces poner en un last nav */
        if ($uri_string != 'user/logout'
            && !preg_match('/^(evento\/evento_async)/', $uri_string)
            && !preg_match('/^(user\/user_async)/', $uri_string)
        )
            $this->CI->session->set_userdata('last_nav', $uri_string);
        // var_dump($this-CI->session-userdata);exit;
        if (empty($uri_string))
            return;
        $user_type = $this->CI->milib->logueado();
        if (empty($user_type)) {
            $user_type = 3;
            /* no esta logueado entonces tenemos que agregar este script en la vista */
            $this->CI->template->add_js("user/no_logueado.js");
        }
        $this->check_permission($uri_string, $user_type);
    }

}

?>