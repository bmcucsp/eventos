<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Configurar variables de Upload
 */

function subir_imagen($tipoConfig = "config_user", $inputname = "userfile", $seo_name = "", $redimensionar = FALSE, $carpeta = "") {
    $CI = & get_instance();
    $CI->load->config("upload_images");
    $config = $CI->config->item($tipoConfig);
    if ($carpeta != "")
        $carpeta .= $carpeta . '/';
    $config['upload_path'] .= $carpeta;
    //verificando carpeta
    if (!is_dir($config['upload_path'])) {
        mkdir($config['upload_path'], 0777);
    }
    $CI->load->library('upload', $config);
    //subiendo imagen
//    var_dump("llegamos aca");
    if (!$CI->upload->do_upload($inputname)) {
        return array('upload_picture_error' => $CI->upload->display_errors());
    } else {
        /* logro subir, entonces creamos sus repetidos en diferentes tamaños */
        if ($redimensionar)
            return redimensionar($CI->upload->data(), $tipoConfig, $config, $seo_name);
        else {
            return array('isuploaded' => "exito");
        }
    }
}

function redimensionar($results, $tipoConfig, $config, $seo_name = "") {
    if (strtoupper($results['file_ext']) == strtoupper('.jpeg')) {
        $results['file_ext'] = '.jpg';
    }
    $results['raw_name'] = a_url_amigable($results['raw_name']);
    if ($seo_name != "")
        $filename = $results['file_path'] . $seo_name . $results['file_ext'];
    else {
        if ($tipoConfig == "config_user") {
            $nombre_entidad = generar_nombre_foto();
            $filename = $results['file_path'] . $nombre_entidad . $results['file_ext'];
        } elseif ($tipoConfig == "config_evento") {
            $nombre_entidad = generar_nombre_evento();
            $filename = $results['file_path'] . $nombre_entidad . $results['file_ext'];
        }
    }
    rename($results['full_path'], $filename);
    $results['full_path'] = $filename;
    if (isset($nombre_entidad))
        $results['raw_name'] = $nombre_entidad;
    $CI = & get_instance();
    $CI->load->library('image_moo');
    //redimensionando
    switch ($tipoConfig) {
        case "config_evento":
        case "config_user":
            /* creando el thumb */
            $CI->image_moo->load($results['full_path']);
            $CI->image_moo->resize_crop($config['width_thumb'], $config['height_thumb']);
            $CI->image_moo->save($config['upload_path'] . $results['raw_name'] .'_thumb'. '.jpg');
            /* creando el standard */
            $CI->image_moo->load($results['full_path']);
            $CI->image_moo->resize($config['width_standard'], $config['height_standard']);
            $CI->image_moo->save($config['upload_path'] . $results['raw_name'] .'_standard'. '.jpg');
            break;
        default:
            break;
    }
    /* eliminamos la imagen original ya que solo necesitamos, los transformados */
    /* por ahora mantendremos la copia original tmb */
    // @unlink($results['full_path']);
    $results['isuploaded'] = "exito";
    return $results;
}

function resizeImageToDimensions($results, $target_width, $target_height) {
    $CI = & get_instance();
    $CI->load->library('image_lib');

    $current_ratio = $results['image_width'] / $results['image_height'];
    $target_ratio = $target_width / $target_height;
    $config['source_image'] = $results['file_path'] . $results['raw_name'] . $results['file_ext'];


    if ($current_ratio > $target_ratio) {
        //resize first to height, maintain ratio
        $config['height'] = $target_height;
        $config['width'] = $target_height * $current_ratio;
        $CI->image_lib->initialize($config);

        if (!$CI->image_lib->resize())
            return array('success' => false, 'error' => "Hubo un error al redimensionar la imagen");

        //then crop off width
        $config['width'] = $target_width;
        $config['maintain_ratio'] = false;
        $CI->image_lib->initialize($config);

        if ($CI->image_lib->crop())
            return array('success' => true);
        else
            return array('success' => false, 'error' => "Hubo un error al recortar la imagen");
    }
    else if ($current_ratio < $target_ratio) {
        //resize first to width, maintain ratio
        $config['width'] = $target_width;
        $config['height'] = $target_width / $current_ratio;
        $CI->image_lib->initialize($config);

        if (!$CI->image_lib->resize())
            return array('success' => false, 'error' => "Hubo un error al redimensionar la imagen");

        //then crop off height
        $config['height'] = $target_height;
        $config['maintain_ratio'] = false;
        $CI->image_lib->initialize($config);

        if ($CI->image_lib->crop())
            return array('success' => true);
        else
            return array('success' => false, 'error' => "Hubo un error al recortar la imagen");
    }
    else {

        $config['width'] = $target_width;
        $config['height'] = $target_height;
        $CI->image_lib->initialize($config);

        if ($CI->image_lib->resize())
            return array('success' => true);
        else
            return array('success' => false, 'error' => "Hubo un error al redimensionar la imagen");
    }
}

function copytothumb($results) {

    $file = $results['file_path'] . $results['raw_name'] . $results['file_ext'];
    $newfile = $results['file_path'] . $results['raw_name'] . '_thumb' . $results['file_ext'];
    ;

    if (!copy($file, $newfile)) {
        echo "failed to copy $file...\n";
    } else {
        return true;
    }
}

/*
 * Borrar imagen de server
 */

function borrar_imagen($path, $imagen_value) {
    $file = $path . $imagen_value;
    if (file_exists($file)) {
        return unlink($file);
    } else {
        return false;
    }
}

//borrar directorio y archivos
function borrar_directorio($dir, $borrarme) {
    if (!$dh = @opendir($dir))
        return;
    while (false !== ($obj = readdir($dh))) {
        if ($obj == '.' || $obj == '..')
            continue;
        if (!@unlink($dir . '/' . $obj))
            borrar_directorio($dir . '/' . $obj, true);
    }
    closedir($dh);
    if ($borrarme) {
        @rmdir($dir);
        return true;
    }
    return false;
}